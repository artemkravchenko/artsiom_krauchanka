<%-- 
    Document   : footer
    Created on : 08.06.2015, 12:53:55
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
<fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>

<div id="footer">
    <p> <fmt:message bundle="${loc}" key="jsp.footer"/> </p>
</div>