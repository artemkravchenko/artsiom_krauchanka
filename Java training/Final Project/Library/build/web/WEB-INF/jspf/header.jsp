<%-- 
    Document   : header
    Created on : 08.06.2015, 12:08:59
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
<fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>

<div id = "header">
    <div id = "header-logo">
        <a href="WebController?command=SWITCH_TO_PAGE&forwardPage=jsp/Main.jsp">
            <img src="images/logo.png">
        </a>
    </div>

    <div id = "header-info">
        <h3>
            <fmt:message bundle="${loc}" key="jsp.welcome"/>, ${user.login}!

            <a href="WebController?command=EXIT">
                <fmt:message bundle="${loc}" key="jsp.exit"/>
            </a><br>

            <fmt:message bundle='${loc}' key='jsp.main.personaloffice'/>

            <a href="WebController?command=PERSONAL_OFFICE">
                <fmt:message bundle='${loc}' key='jsp.main.link.personaloffice'/>
            </a>
        </h3>
    </div>
</div>
