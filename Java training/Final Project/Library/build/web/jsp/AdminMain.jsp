<%-- 
    Document   : AdminMain
    Created on : 18.05.2015, 21:11:07
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/taglib.tld" prefix="tableTag" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>

<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <div id="content-wrapper">
            <div id = "wrapper">
                <div id = "header">
                    <div id = "header-logo">
                        <a href="WebController?command=SWITCH_TO_PAGE&forwardPage=jsp/AdminMain.jsp">
                            <img src="images/logo.png">
                        </a>
                    </div>

                    <div id = "header-info">
                        <h3>
                            <fmt:message bundle="${loc}" key="jsp.adminMain.info"/>
                            <a href="WebController?command=EXIT">
                                <fmt:message bundle="${loc}" key="jsp.exit"/>
                            </a>
                        </h3>
                    </div>
                </div>

                <div id = "content">
                    <h2 style="color: darkblue; text-indent: 130px;">
                        <fmt:message bundle="${loc}" key="jsp.adminMain.cardfile"/>
                    </h2>
                    <div id="table">
                        <tableTag:getTable tableName="ADMIN" recordsPerPage="7" numberOfRecords="${numberOfAdminRecords}">
                            <jsp:body>
                                <table class="customTable">
                                    <tr> 
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.id"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.user"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.name"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.isDelivery"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.deliveryDate"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.destination"/> </td>
                                        <td>  <fmt:message bundle="${loc}" key="jsp.adminMain.table.isReturn"/> </td>
                                    </tr>
                                    <c:forEach var="record" items="${adminList}">
                                        <tr>
                                            <td>
                                                <c:out value="${record.id}"/>
                                            </td>
                                            <td>
                                                <c:out value="${record.user.login}"/>
                                            </td>
                                            <td>
                                                <c:out value="${record.book.name}"/>
                                            </td>

                                            <td>
                                                <c:choose>
                                                    <c:when test="${record.isDelivery}">
                                                        <fmt:message bundle="${loc}" key="jsp.adminMain.isDelivery"/>

                                                    </c:when>

                                                    <c:when test="${not record.isDelivery}">
                                                        <fmt:message bundle="${loc}" key="jsp.adminMain.notDelivery"/>
                                                    </c:when>
                                                </c:choose>
                                            </td>

                                            <td>
                                                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${record.dateOfDelivery}"/>
                                            </td>

                                            <td>

                                                <c:if test="${record.isDelivery==false}">
                                                    <select name="destination" form="order_form">
                                                        <option value="читальный зал">
                                                            <fmt:message bundle="${loc}" key="jsp.adminMain.destination_read"/>
                                                        </option>

                                                        <option value="абонемент">
                                                            <fmt:message bundle="${loc}" key="jsp.adminMain.destination_subs"/>
                                                        </option>
                                                    </select>
                                                </c:if>
                                                <c:out value="${record.destination}"/>

                                            </td>

                                            <td>
                                                <c:if test="${record.isDelivery}">

                                                    <c:choose>
                                                        <c:when test="${record.isReturned}">
                                                            <fmt:message bundle="${loc}" key="jsp.adminMain.isReturned"/>
                                                        </c:when>  

                                                        <c:when test="${not record.isReturned}">
                                                            <fmt:message bundle="${loc}" key="jsp.adminMain.notReturned"/>
                                                        </c:when>
                                                    </c:choose>

                                                </c:if>
                                            </td>

                                            <c:if test="${record.isDelivery==false}">
                                                <td>
                                                    <form id="order_form" action="WebController" method="POST">
                                                        <input type="hidden" name="recordId" value="${record.id}">
                                                        <input type="hidden" name="command" value="PROCCESS_ORDER">
                                                        <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.adminMain.delivery_button'/>">
                                                    </form>
                                                </td>
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </jsp:body>
                        </tableTag:getTable>   
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../WEB-INF/jspf/footer.jsp"/>
    </body>
</html>
