<%-- 
    Document   : Authorization
    Created on : 11.05.2015, 12:04:44
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/AuthorizationStyle.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <section class = "container">
            <div class="lang">
                <a href="WebController?command=SET_LOCALE&locale=en">
                    <img src="images/USA.jpg">
                </a>
                <a href="WebController?command=SET_LOCALE&locale=ru">
                    <img src="images/RUSSIA.jpg">
                </a> 
            </div>

            <div class = "login">
                <h1> 
                    <fmt:message bundle="${loc}" key="jsp.authorization.header"/> 
                </h1>
                <form action="WebController" method="POST" >
                    <p>
                        <input type="text" name="login" value="" placeholder="<fmt:message bundle='${loc}' key='jsp.authorization.login_placeholder'/>">
                    </p>
                    <p>
                        <input type="password" name="password" value="" placeholder="<fmt:message bundle='${loc}' key='jsp.authorization.password_placeholder'/>">
                    </p>
                    <div id = "reg_link">
                        <a href="WebController?command=SWITCH_TO_PAGE&forwardPage=<fmt:message bundle='${loc}' key='forward.registration'/>">
                            <fmt:message bundle="${loc}" key="jsp.authorization.registration_link"/>
                        </a>
                    </div>

                    <input type="hidden" name="command" value="AUTHORIZATION"/>
                    <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.authorization.button'/>">
                </form>
            </div>
            <p id="error_string">${message}</p>
        </section>
    </body>
</html>
