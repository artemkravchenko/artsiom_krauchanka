<%-- 
    Document   : BookPage
    Created on : 17.05.2015, 20:14:18
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <div id="content-wrapper">
            <div id = "wrapper">
                <jsp:include page="../WEB-INF/jspf/header.jsp"/>

                <div id = "content">
                    <h2 style="color: darkblue; text-align: center">${book.name}</h2>
                    <div id="bookPage-left">
                        <div id="bookPage-image">
                            <img src="images/${book.name}.jpg">
                        </div>

                        <div id="book-order" style="float:left; margin-left: 10px;">
                            <fmt:message bundle='${loc}' key='jsp.book_page.queue'/>
                            ${waiting_users} 
                            <fmt:message bundle='${loc}' key='jsp.book_page.queue_people'/>

                            <form action="WebController" method="POST">
                                <input type="hidden" name="bookId" value="${book.id}">
                                <input type="hidden" name="command" value="BOOK_ORDER">
                                <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.book_page.button'/>"/>
                            </form>
                            <c:out value="${message}"/>
                        </div>
                    </div>

                    <div id="bookPage-info">
                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.name'/> </strong> 
                        <c:out value="${book.name}"/><br>

                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.writer'/> </strong> 
                        <c:out value="${book.writer}"/><br>

                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.year'/> </strong> 
                        <c:out value="${book.year}"/><br>

                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.page_count'/> </strong>
                        <c:out value="${book.pageCount}"/><br>

                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.publish'/> </strong>
                        <c:out value="${book.publishingHouse}"/><br>

                        <strong> <fmt:message bundle='${loc}' key='jsp.book_page.copies'/> </strong>
                        <c:out value="${book.countOfCopies}"/><br>
                    </div>

                    <div id="bookPage-description">
                        <h2 style="color: darkblue;">
                            <fmt:message bundle='${loc}' key='jsp.book_page.description'/>
                        </h2>
                        <blockquote>
                            <c:out value="${book.bookDescription}"/>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../WEB-INF/jspf/footer.jsp"/>
    </body>
</html>
