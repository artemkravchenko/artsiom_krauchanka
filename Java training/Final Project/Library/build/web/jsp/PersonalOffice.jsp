<%-- 
    Document   : PersonalOffice
    Created on : 18.05.2015, 17:29:52
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/taglib.tld" prefix="tableTag" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>

<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <div id="content-wrapper">
            <div id = "wrapper">
                <jsp:include page="../WEB-INF/jspf/header.jsp"/>

                <div id = "content">
                    <h2 style="color: darkblue; text-indent: 130px;">
                        <fmt:message bundle='${loc}' key='jsp.personaloffice.cardfile'/>
                    </h2>

                    <div id="table">
                        <tableTag:getTable tableName="CARDFILE" recordsPerPage="7" numberOfRecords="${numberOfRecords}">
                            <jsp:body>
                                <table class="customTable">
                                    <tr> 
                                        <td><fmt:message bundle='${loc}' key='jsp.personaloffice.table.name'/></td>
                                        <td><fmt:message bundle='${loc}' key='jsp.personaloffice.table.isDelivery'/></td>
                                        <td><fmt:message bundle='${loc}' key='jsp.personaloffice.table.deliveryDate'/> </td>
                                        <td><fmt:message bundle='${loc}' key='jsp.personaloffice.table.destination'/></td>
                                        <td><fmt:message bundle='${loc}' key='jsp.personaloffice.table.isReturned'/></td>
                                    </tr>
                                    <c:forEach var="record" items="${recordList}">
                                        <tr>
                                            <td>
                                                <a href="WebController?command=BOOK_VIEW&bookID=${record.book.id}">
                                                    ${record.book.name}
                                                </a>
                                            </td>

                                            <td>
                                                <c:choose>
                                                    <c:when test="${record.isDelivery}">
                                                        <fmt:message bundle='${loc}' key='jsp.personaloffice.isDelivery'/>

                                                    </c:when>
                                                    <c:when test="${not record.isDelivery}">
                                                        <fmt:message bundle='${loc}' key='jsp.personaloffice.notDelivery'/>
                                                    </c:when>
                                                </c:choose>

                                            </td>

                                            <td> 
                                                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${record.dateOfDelivery}"/>
                                            </td>
                                            <td> ${record.destination} </td>
                                            <td>
                                                <c:if test="${record.isDelivery}">
                                                    <c:choose>
                                                        <c:when test="${record.isReturned}">
                                                            <fmt:message bundle='${loc}' key='jsp.personaloffice.isReturned'/>
                                                        </c:when>

                                                        <c:when test="${not record.isReturned}">
                                                            <fmt:message bundle='${loc}' key='jsp.personaloffice.notReturned'/>
                                                        </c:when>
                                                    </c:choose>
                                                </c:if>
                                            </td>

                                            <c:if test="${record.isDelivery==false}">
                                                <td>
                                                    <form action="WebController" method="POST">
                                                        <input type="hidden" name="recordId" value="${record.id}">
                                                        <input type="hidden" name="command" value="CANCEL_ORDER">
                                                        <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.personaloffice.cancel_order.button'/>">
                                                    </form>
                                                </td>
                                            </c:if>

                                            <c:if test="${record.isReturned==false}">
                                                <c:if test="${record.isDelivery==true}">
                                                    <td>
                                                        <form action="WebController" method="POST">
                                                            <input type="hidden" name="recordId" value="${record.id}">
                                                            <input type="hidden" name="command" value="RETURN_BOOK">
                                                            <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.personaloffice.button'/> ">
                                                        </form>
                                                    </td>
                                                </c:if>
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </jsp:body>
                        </tableTag:getTable>   
                    </div>
                </div>
            </div> 
        </div>
        <jsp:include page="../WEB-INF/jspf/footer.jsp"/>
    </body>
</html>
