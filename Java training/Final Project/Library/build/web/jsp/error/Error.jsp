<%-- 
    Document   : error
    Created on : 12.05.2015, 12:38:40
    Author     : Artem Kravchenko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>

<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.error.title"/> 
        </title>
    </head>
    <body>
        <section class="container">
            <div class="error-page">
                <h1> <fmt:message bundle="${loc}" key="jsp.error.header"/> </h1>
                <div id="error-text">
                    <c:out value="${exception}"/>
                </div>
            </div>
        </section>
    </body>
</html>
