/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.UserDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.CardRecord;
import com.epam.training.entity.User;
import com.epam.training.resource.Resource;
import java.util.ArrayList;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * The <code>AuthorizationCommand</code> class extends <code>Command</code>
 * class. Its method <code>processRequest</code> executes when user try to log
 * in.
 *
 * @author Artem Kravchenko
 */
public class AuthorizationCommand extends Command {

    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "message";
    private static final String PARAM_BOOKLIST = "bookList";
    private static final String PARAM_RECORDLIST = "adminList";
    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_ERROR_MESSAGE = "error.authorization";
    private static final String PARAM_ADMIN_RECORD = "numberOfAdminRecords";
    private static final String PARAM_BOOK_RECORD = "numberOfBooks";
    private static final String PARAM_TABLESIZE = "tableSize";

    private static final String DAO_ERROR = "error.dao";

    private static final String FORWARD_MAIN = "forward.main";
    private static final String FORWARD_AUTHORIZATION = "forward.authorization";
    private static final String FORWARD_ADMINMAIN = "forward.admin_main";
    private static final String FORWARD_ERROR = "forward.error";

    /**
     * <code>processRequest</code> method executes when user with login and
     * password try to log in. If such user exist in database, information about
     * him adds to the session.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        Locale locale = (Locale) request.getSession().getAttribute(PARAM_LOCALE);
        String userLogin = request.getParameter(PARAM_LOGIN);
        String userPassword = DigestUtils.md5Hex(request.getParameter(PARAM_PASSWORD));
        UserDao userDao = UserDao.getInstance();
        BookDao bookDao = BookDao.getInstance();

        try {
            User user = userDao.getUser(userLogin, userPassword);
            if (user != null) {
                int recordNumber;
                request.getSession().setAttribute(PARAM_USER, user);
                int tableSize = Integer.parseInt(request.getSession().
                        getServletContext().getInitParameter(PARAM_TABLESIZE));
                if (user.isAdmin()) {
                    CardfileDao cardfileDao = CardfileDao.getInstance();
                    ArrayList<CardRecord> records = cardfileDao.getRecords(0, tableSize);
                    recordNumber = cardfileDao.getTableSize();
                    request.getSession().setAttribute(PARAM_ADMIN_RECORD, recordNumber);

                    request.getSession().setAttribute(PARAM_RECORDLIST, records);
                    setForward(Resource.getResource(FORWARD_ADMINMAIN));
                } else {

                    recordNumber = bookDao.getTableSize();
                    ArrayList<Book> bookList = bookDao.getRecords(0, tableSize);
                    request.getSession().setAttribute(PARAM_BOOKLIST, bookList);
                    request.getSession().setAttribute(PARAM_BOOK_RECORD, recordNumber);
                    setForward(Resource.getResource(FORWARD_MAIN));
                }
            } else {
                request.setAttribute(PARAM_ERROR, Resource.getResource(PARAM_ERROR_MESSAGE, locale));
                setForward(Resource.getResource(FORWARD_AUTHORIZATION));
            }

        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }

    }

}
