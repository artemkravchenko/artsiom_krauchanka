/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.CardRecord;

import com.epam.training.resource.Resource;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>BookDeliveryCommand</code> class extends <code>Command</code>
 * class. Its method <code>processRequest</code> executes when administrator
 * confirm user book apllication.
 *
 * @author Artem Kravchenko
 */
public class BookDeliveryCommand extends Command {

    private static final String PARAM_DESTINATION = "destination";
    private static final String PARAM_RECORD = "recordId";
    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_RECORD_LIST = "adminList";
    private static final String FORWARD_ERROR = "forward.error";
    private static final String FORWARD_ADMIN_MAIN = "forward.admin_main";
    private final static String DAO_ERROR = "error.dao";
    private final static String ENCODING_CHARSET = "ISO-8859-1";
    private final static String DECODING_CHARSET = "UTF-8";
    private static final String PARAM_TABLESIZE = "tableSize";

    /**
     * <code>processRequest</code> method executes when administrator confirm
     * user book apllication.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        CardfileDao cardfileDao = CardfileDao.getInstance();
        BookDao bookDao = BookDao.getInstance();
        int recordId = Integer.parseInt(request.getParameter(PARAM_RECORD));
        try {
            String destination = new String(request.getParameter(PARAM_DESTINATION).getBytes(ENCODING_CHARSET), DECODING_CHARSET);
            int tableSize = Integer.parseInt(request.getSession().
                    getServletContext().getInitParameter(PARAM_TABLESIZE));
            cardfileDao.confirmRecord(recordId, destination);
            Book book = cardfileDao.getBookByRecord(recordId);
            bookDao.setCountOfCopies(book.getId(), book.getCountOfCopies() - 1);

            ArrayList<CardRecord> records = cardfileDao.getRecords(0, tableSize);

            request.getSession().setAttribute(PARAM_RECORD_LIST, records);
            setForward(Resource.getResource(FORWARD_ADMIN_MAIN));
        } catch (DaoException | UnsupportedEncodingException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
