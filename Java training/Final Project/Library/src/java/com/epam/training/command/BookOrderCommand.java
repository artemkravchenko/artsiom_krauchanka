/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.User;
import com.epam.training.resource.Resource;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>BookOrderCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes when user try to order book
 * on main page.
 *
 * @author Artem Kravchenko
 */
public class BookOrderCommand extends Command {

    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_USER = "user";
    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_MESSAGE = "message";
    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_BOOK = "book";
    private final static String PARAM_WAITING_USERS = "waiting_users";
    private static final String PARAM_ERROR_MESSAGE = "error.book_order";

    private final static String DAO_ERROR = "error.dao";

    private static final String ERROR_MESSAGE = "error.book_order.book_exist";
    private static final String ORDER_MESSAGE = "message.book_order";
    private static final String FORWARD_ERROR = "forward.error";
    private static final String FORWARD_BOOKPAGE = "forward.book_page";

    /**
     * <code>processRequest</code> method executes when user try to order book.
     *
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        Locale locale = (Locale) request.getSession().getAttribute(PARAM_LOCALE);
        int id = Integer.parseInt(request.getParameter(PARAM_BOOK_ID));
        User user = (User) request.getSession().getAttribute(PARAM_USER);
        BookDao bookDao = BookDao.getInstance();
        CardfileDao cardfileDao = CardfileDao.getInstance();

        try {
            Book book = bookDao.getBookById(id);
            request.setAttribute(PARAM_BOOK, book);
            request.setAttribute(PARAM_WAITING_USERS, cardfileDao.waitingUsersCount(book.getId()));
            if (book.getCountOfCopies() > 0) {
                if (!cardfileDao.checkUserRecord(user.getId(), book.getId())) {
                    cardfileDao.addRecord(user.getId(), book.getId());
                    request.setAttribute(PARAM_MESSAGE, Resource.getResource(ORDER_MESSAGE, locale));
                } else {
                    request.setAttribute(PARAM_MESSAGE, Resource.getResource(PARAM_ERROR_MESSAGE, locale));
                }
            } else {
                request.setAttribute(PARAM_MESSAGE, Resource.getResource(ERROR_MESSAGE, locale));
            }
            setForward(Resource.getResource(FORWARD_BOOKPAGE));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }

    }

}
