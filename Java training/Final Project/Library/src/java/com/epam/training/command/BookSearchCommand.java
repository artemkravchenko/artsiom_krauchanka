/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import static com.epam.training.command.Command.logger;
import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.resource.Resource;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Artem Kravchenko
 */
public class BookSearchCommand extends Command {

    private final static String PARAM_BOOKNAME = "bookName";
    private final static String PARAM_EXCEPTION = "exception";
    private static final String PARAM_BOOK_RECORD = "numberOfBooks";
    private static final String PARAM_BOOKLIST = "bookList";

    private final static String DAO_ERROR = "error.dao";
    private final static String FORWARD_MAIN = "forward.main";
    private final static String FORWARD_ERROR = "forward.error";

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        BookDao bookDao = BookDao.getInstance();
        String bookName = request.getParameter(PARAM_BOOKNAME);
        try {
            ArrayList<Book> bookList = bookDao.getBookByName(bookName);
            request.setAttribute(PARAM_BOOKLIST, bookList);
            request.setAttribute(PARAM_BOOK_RECORD, bookList.size());
            setForward(Resource.getResource(FORWARD_MAIN));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
