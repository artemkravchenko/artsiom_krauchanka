/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;

import com.epam.training.resource.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>BookViewCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes before the book page
 * formation.
 *
 * @author Artem Kravchenko
 */
public class BookViewCommand extends Command {

    private final static String PARAM_BOOKID = "bookID";
    private final static String PARAM_EXCEPTION = "exception";
    private final static String PARAM_BOOK = "book";
    private final static String PARAM_WAITING_USERS = "waiting_users";

    private final static String DAO_ERROR = "error.dao";

    private final static String FORWARD_MAIN = "forward.main";
    private final static String FORWARD_BOOKPAGE = "forward.book_page";
    private final static String FORWARD_ERROR = "forward.error";

    /**
     * <code>processRequest</code> method executes before the book page
     * formation. The book information adds to the session.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        BookDao bookDao = BookDao.getInstance();
        CardfileDao cardfileDao = CardfileDao.getInstance();
        int bookId = Integer.parseInt(request.getParameter(PARAM_BOOKID));
        try {
            Book book = bookDao.getBookById(bookId);
            if (book != null) {
                int waitingUsersCount = cardfileDao.waitingUsersCount(book.getId());
                request.setAttribute(PARAM_BOOK, book);
                request.setAttribute(PARAM_WAITING_USERS, waitingUsersCount);
                setForward(Resource.getResource(FORWARD_BOOKPAGE));
            } else {
                setForward(Resource.getResource(FORWARD_MAIN));
            }
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }

    }

}
