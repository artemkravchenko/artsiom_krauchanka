/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.CardRecord;
import com.epam.training.entity.User;
import com.epam.training.resource.Resource;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>CancelOrderCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes when user try to cancel book
 * order.
 *
 * @author Artem Kravchenko
 */
public class CancelOrderCommand extends Command {

    private static final String PARAM_RECORD = "recordId";
    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_USER = "user";
    private static final String PARAM_RECORDS = "recordList";

    private static final String FORWARD_ERROR = "forward.error";
    private static final String FORWARD_PERSONAL_OFFICE = "forward.personal_office";
    private static final String PARAM_TABLESIZE = "tableSize";
    private static final String DAO_ERROR = "error.dao";

    /**
     * <code>processRequest</code> method executes when user try to cancel book
     * order.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        CardfileDao cardfileDao = CardfileDao.getInstance();
        int recordId = Integer.parseInt(request.getParameter(PARAM_RECORD));
        try {
            int tableSize = Integer.parseInt(request.getSession().
                    getServletContext().getInitParameter(PARAM_TABLESIZE));
            cardfileDao.cancelOrder(recordId);
            User user = (User) request.getSession().getAttribute(PARAM_USER);
            ArrayList<CardRecord> recordList = cardfileDao.getUserRecords(user.getId(), 0, tableSize);
            request.getSession().setAttribute(PARAM_RECORDS, recordList);
            setForward(Resource.getResource(FORWARD_PERSONAL_OFFICE));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
