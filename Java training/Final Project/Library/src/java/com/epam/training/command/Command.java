/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * The abstract class <code>Command</code> implements a Commnad pattern.
 *
 * @author Artem Kravchenko
 */
public abstract class Command {

    private String forward;
    protected static Logger logger;

    public Command() {
        BasicConfigurator.configure();
        logger = Logger.getLogger(Command.class);
    }

    /**
     *
     * @return forward.
     */
    public String getForward() {
        return forward;
    }

    /**
     *
     * @param forward
     */
    public void setForward(String forward) {
        this.forward = forward;
    }

    /**
     *
     * @param request
     * @param response
     *
     */
    public abstract void processRequest(HttpServletRequest request, HttpServletResponse response);

}
