/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

/**
 *
 * @author Artem Kravchenko
 */
public enum CommandAccessEnum {

    ADMIN, USER, ALL, UNREPORT;
}
