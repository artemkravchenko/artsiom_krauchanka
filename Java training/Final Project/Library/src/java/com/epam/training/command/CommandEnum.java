/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import static com.epam.training.command.CommandAccessEnum.*;

/**
 *
 * @author Artem Kravchenko
 */
public enum CommandEnum {

    AUTHORIZATION(UNREPORT), REGISTRATION(UNREPORT), EXIT(UNREPORT), SWITCH_TO_PAGE(UNREPORT), SET_LOCALE(UNREPORT),
    BOOK_VIEW(USER), PERSONAL_OFFICE(USER), BOOK_ORDER(USER), SEARCH_BOOK(USER), CANCEL_ORDER(USER), RETURN_BOOK(USER),
    PROCCESS_ORDER(ADMIN), GET_BOOK_TABLE(USER), GET_CARDFILE_TABLE(USER), GET_ADMIN_TABLE(ADMIN), BOOK_SEARCH(USER);

    private final CommandAccessEnum commandAccess;

    private CommandEnum(CommandAccessEnum commandAccess) {
        this.commandAccess = commandAccess;
    }

    /**
     *
     * @return command privilege.
     */
    public CommandAccessEnum getCommandPrivilege() {
        return commandAccess;
    }

}
