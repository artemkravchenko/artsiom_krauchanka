/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import static com.epam.training.command.CommandEnum.*;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Artem Kravchenko
 */
public class CommandFactory {

    private static final Map<CommandEnum, Command> commands = new HashMap<>();
    
    private static final String PARAM_COMMAND = "command";

    static {
        commands.put(REGISTRATION, new RegistrationCommand());
        commands.put(AUTHORIZATION, new AuthorizationCommand());
        commands.put(EXIT, new SessionExitCommand());
        commands.put(SWITCH_TO_PAGE, new SwitchToPageCommand());
        commands.put(BOOK_VIEW, new BookViewCommand());
        commands.put(PERSONAL_OFFICE, new PersonalOfficeCommand());
        commands.put(BOOK_ORDER, new BookOrderCommand());
        commands.put(PROCCESS_ORDER, new BookDeliveryCommand());
        commands.put(RETURN_BOOK, new ReturnBookCommand());
        commands.put(SET_LOCALE, new SetLocaleCommand());
        commands.put(CANCEL_ORDER, new CancelOrderCommand());
        commands.put(GET_BOOK_TABLE, new GetBookTableCommand());
        commands.put(GET_CARDFILE_TABLE, new GetCardfileTableCommand());
        commands.put(GET_ADMIN_TABLE, new GetAdminTableCommand());
        commands.put(BOOK_SEARCH, new BookSearchCommand());
    }

    /**
     *
     * @param request
     * @return
     */
    public static Command getCommand(HttpServletRequest request) {
        CommandEnum commandType = CommandEnum.valueOf(request.getParameter(PARAM_COMMAND));
        Command command = commands.get(commandType);
        return command;
    }

    public static Command getCommand(CommandEnum commandEnum) {
        Command command = null;
        if (commandEnum != null) {
            command = commands.get(commandEnum);
        }
        return command;
    }

}
