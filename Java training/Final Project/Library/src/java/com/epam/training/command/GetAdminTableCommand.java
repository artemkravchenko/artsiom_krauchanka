/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.CardRecord;
import com.epam.training.resource.Resource;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Artem Kravchenko
 */
public class GetAdminTableCommand extends Command {

    private static final String PARAM_PAGE = "page";
    private static final String PARAM_RECORD_NUMBER = "recordsPerPage";
    private final static String PARAM_RECORD_LIST = "adminList";
    private final static String PARAM_EXCEPTION = "exception";
    private static final String FORWARD_ERROR = "forward.error";
    private final static String DAO_ERROR = "error.dao";
    private static final String FORWARD_ADMINMAIN = "forward.admin_main";

    /**
     *
     * @param request
     * @param response
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            CardfileDao cardfileDao = CardfileDao.getInstance();
            int page = Integer.parseInt(request.getParameter(PARAM_PAGE));
            int numberOfRecords = Integer.parseInt(request.getParameter(PARAM_RECORD_NUMBER));

            ArrayList<CardRecord> recordList = cardfileDao.getRecords((page - 1) * numberOfRecords, numberOfRecords);
            request.getSession().setAttribute(PARAM_RECORD_LIST, recordList);
            setForward(Resource.getResource(FORWARD_ADMINMAIN));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
