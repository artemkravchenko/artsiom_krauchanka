/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.CardRecord;
import com.epam.training.entity.User;

import com.epam.training.resource.Resource;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>PersonalOfficeCommand</code> class extends <code>Command</code>
 * class. Its method <code>processRequest</code> executes when user try enter to
 * the own personal office.
 *
 * @author Artem Kravchenko
 */
public class PersonalOfficeCommand extends Command {

    private static final String FORWARD_ERROR = "forward.error";
    private static final String FORWARD_PERSONAL_OFFICE = "forward.personal_office";

    private static final String PARAM_USER = "user";
    private static final String PARAM_RECORD_LIST = "recordList";
    private static final String PARAM_RECORDS = "numberOfRecords";
    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_TABLESIZE = "tableSize";
    private final static String DAO_ERROR = "error.dao";

    /**
     * <code>processRequest</code> method executes when user try enter to the
     * own personal office. Information about user records from cardfile addedd
     * to the request.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {

        try {
            CardfileDao cardfileDao = CardfileDao.getInstance();
            User user = (User) request.getSession().getAttribute(PARAM_USER);
            int tableSize = Integer.parseInt(request.getSession().
                    getServletContext().getInitParameter(PARAM_TABLESIZE));
            ArrayList<CardRecord> recordList = cardfileDao.getUserRecords(user.getId(), 0, tableSize);
            request.getSession().setAttribute(PARAM_RECORD_LIST, recordList);
            int recordNumber = cardfileDao.getTableSize(user.getId());
            request.getSession().setAttribute(PARAM_RECORDS, recordNumber);
            setForward(Resource.getResource(FORWARD_PERSONAL_OFFICE));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }

    }

}
