/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.UserDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.User;
import com.epam.training.utils.RegistrationUtil;
import com.epam.training.resource.Resource;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * The <code>RegistrationCommand</code> class extends <code>Command</code>
 * class. Its method <code>processRequest</code> executes when user try to sign
 * up in the system. If registration is successful, information about user added
 * to the database.
 *
 * @author Artem Kravchenko
 */
public class RegistrationCommand extends Command {

    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_MESSAGE = "message";
    private static final String PARAM_CONFIRM_PASSWORD = "confirm_password";

    private static final String FORWARD_REGISTRATION = "forward.registration";
    private static final String FORWARD_AUTHORIZATION = "forward.authorization";
    private static final String FORWARD_ERROR = "forward.error";

    private static final String ERROR_MESSAGE = "error.registration";
    private static final String MESSAGE = "message.registration";
    private static final String FILLING_ERROR_MESSAGE = "error.registration.filling";

    private static final String PARAM_LOCALE = "locale";

    private final static String DAO_ERROR = "error.dao";

    /**
     * <code>processRequest</code> method executes when user try to sign up in
     * the system. If registration is successful, information about user added
     * to the database.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        Locale locale = (Locale) request.getSession().getAttribute(PARAM_LOCALE);
        UserDao userDao = UserDao.getInstance();
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        String email = request.getParameter(PARAM_EMAIL);
        String confirmPassword = request.getParameter(PARAM_CONFIRM_PASSWORD);

        RegistrationUtil utils = new RegistrationUtil();
        try {
            if (utils.checkLogin(login) && utils.checkPassword(password)
                    && utils.checkConfirmPassword(password, confirmPassword) && utils.checkEmail(email)) {
                password = DigestUtils.md5Hex(password);
                if (!userDao.isUserExist(login)) {
                    User user = new User();
                    user.setLogin(login);
                    user.setPassword(password);
                    user.setEmail(email);
                    userDao.addRecord(user);
                    request.setAttribute(PARAM_MESSAGE, Resource.getResource(MESSAGE, locale));
                    setForward(Resource.getResource(FORWARD_AUTHORIZATION));
                } else {
                    request.setAttribute(PARAM_MESSAGE, Resource.getResource(ERROR_MESSAGE, locale));
                    setForward(Resource.getResource(FORWARD_REGISTRATION));
                }
            } else {
                request.setAttribute(PARAM_MESSAGE, Resource.getResource(FILLING_ERROR_MESSAGE, locale));
                setForward(Resource.getResource(FORWARD_REGISTRATION));
            }
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
