/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.database.dao.BookDao;
import com.epam.training.database.dao.CardfileDao;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.CardRecord;
import com.epam.training.entity.User;
import com.epam.training.resource.Resource;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>ReturnBookCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes when user try to return book
 * to the library.
 *
 * @author Artem Kravchenko
 */
public class ReturnBookCommand extends Command {

    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_RECORDS = "recordList";
    private static final String PARAM_RECORD_ID = "recordId";
    private static final String PARAM_USER = "user";

    private static final String FORWARD_OFFICE = "forward.personal_office";
    private static final String FORWARD_ERROR = "forward.error";
    private static final String PARAM_TABLESIZE = "tableSize";
    private static final String DAO_ERROR = "error.dao";

    /**
     * <code>processRequest</code> method executes when user try to return book
     * to the library.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute(PARAM_USER);
        CardfileDao cardfileDao = CardfileDao.getInstance();
        BookDao bookDao = BookDao.getInstance();
        int recordId = Integer.parseInt(request.getParameter(PARAM_RECORD_ID));
        int tableSize = Integer.parseInt(request.getSession().
                getServletContext().getInitParameter(PARAM_TABLESIZE));
        try {
            cardfileDao.returnBook(recordId);
            Book book = cardfileDao.getBookByRecord(recordId);
            bookDao.setCountOfCopies(book.getId(), book.getCountOfCopies() + 1);

            ArrayList<CardRecord> recordList = cardfileDao.getUserRecords(user.getId(), 0, tableSize);
            request.getSession().setAttribute(PARAM_RECORDS, recordList);

            setForward(Resource.getResource(FORWARD_OFFICE));
        } catch (DaoException ex) {
            logger.error(ex);
            request.setAttribute(PARAM_EXCEPTION, Resource.getResource(DAO_ERROR));
            setForward(Resource.getResource(FORWARD_ERROR));
        }
    }

}
