/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.resource.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>SessionExitCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes when user try to logout.
 *
 * @author Artem Kravchenko
 */
public class SessionExitCommand extends Command {

    private static final String FORWARD_AUTHORIZATION = "forward.authorization";

    /**
     * <code>processRequest</code> method executes when user try to logout.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().invalidate();
        setForward(Resource.getResource(FORWARD_AUTHORIZATION));
    }

}
