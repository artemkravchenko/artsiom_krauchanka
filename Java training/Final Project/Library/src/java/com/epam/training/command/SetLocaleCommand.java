/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import com.epam.training.resource.Resource;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>SetLocaleCommand</code> class extends <code>Command</code> class.
 * Its method <code>processRequest</code> executes when user try to change
 * language in the system.
 *
 * @author Artem Kravchenko
 */
public class SetLocaleCommand extends Command {

    private static final String PARAM_LOCALE = "locale";
    private static final String FORWARD_AUTHORIZATION = "forward.authorization";

    /**
     * <code>processRequest</code> method executes when user try to change
     * language in the system.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String localeName = request.getParameter(PARAM_LOCALE);
        Locale locale = new Locale(localeName);
        request.getSession().setAttribute(PARAM_LOCALE, locale);
        setForward(Resource.getResource(FORWARD_AUTHORIZATION));
    }

}
