/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The <code>SwitchToPageCommand</code> class extends <code>Command</code>
 * class. Its method <code>processRequest</code> executes in the transition to
 * another page.
 *
 * @author Artem Kravchenko
 */
public class SwitchToPageCommand extends Command {

    private static final String PARAM_FORWARD_PAGE = "forwardPage";

    /**
     * <code>processRequest</code> method executes in the transition to another
     * page.
     *
     * @param request
     * @param response
     *
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String forward = request.getParameter(PARAM_FORWARD_PAGE);
        if (forward != null) {
            setForward(forward);
        }
    }

}
