/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.database;

import com.epam.training.database.dao.exceptions.ConnectionPoolException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * The <code>ConnectionPool</code> class provides access to the database. It
 * contains list of <code>Connection</code> objects.
 *
 * @author Artem Kravchenko
 * @see Connection
 */
public class ConnectionPool {

    private final static String DB_CON_COUNT = "database.connectionCount";
    private final static String DB_DRIVER_CLASS = "database.driver";
    private final static String DB_PATH = "database.path";
    private final static String DB_LOGIN = "database.login";
    private final static String DB_PASSWORD = "database.password";
    private final static String DB_PROPERTY_PATH = "../Database.properties";

    private BlockingQueue<Connection> connectionList;
    private static volatile ConnectionPool instance;
    private final String DBUrl;
    private final String DBlogin;
    private final String DBpassword;

    /**
     * Creates a ConnectionPool object that contains connection list.
     * Configuration file located in WEB-INF folder.
     *
     * @throws ConnectionPoolException
     * @see java.sql.Connection
     *
     */
    private ConnectionPool() throws ConnectionPoolException {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(DB_PROPERTY_PATH);
            Properties property = new Properties();
            property.load(is);

            DBUrl = property.getProperty(DB_PATH);
            DBlogin = property.getProperty(DB_LOGIN);
            DBpassword = property.getProperty(DB_PASSWORD);
            int connectionsCount = Integer.parseInt(property.getProperty(DB_CON_COUNT));
            connectionList = new ArrayBlockingQueue<>(connectionsCount);
            Class.forName(property.getProperty(DB_DRIVER_CLASS));

            for (int i = 0; i < connectionsCount; i++) {
                connectionList.add(DriverManager.getConnection(DBUrl, DBlogin, DBpassword));
            }
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            throw new ConnectionPoolException(ex);
        }
    }

    /**
     * Creates ConnectionPool instance if it's not exist.
     *
     * @return ConnectionPool instance.
     * @throws ConnectionPoolException
     */
    public static synchronized ConnectionPool getInstance() throws ConnectionPoolException {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    }

    /**
     * Take connection from connection queue if is not empty.
     *
     * @return Connection object.
     * @throws ConnectionPoolException
     */
    public Connection getConnection() throws ConnectionPoolException {
        Connection connection;
        try {
            connection = connectionList.poll(15, TimeUnit.SECONDS);
            if (connection == null) {
                connection = DriverManager.getConnection(DBUrl, DBlogin, DBpassword);
            }
            return connection;
        } catch (InterruptedException | SQLException ex) {
            throw new ConnectionPoolException(ex);
        }
    }

    /**
     * Put connection to the connection queue.
     *
     * @param connection
     * @throws com.epam.training.database.dao.exceptions.ConnectionPoolException
     *
     *
     */
    public void freeConnection(Connection connection) throws ConnectionPoolException {
        try {
            if (connection != null) {
                if (!connectionList.offer(connection, 15, TimeUnit.SECONDS)) {
                    connection.close();
                }
            }
        } catch (InterruptedException | SQLException ex) {
            throw new ConnectionPoolException(ex);
        }
    }

    /**
     *
     * @throws ConnectionPoolException
     */
    public void dispose() throws ConnectionPoolException {
        Connection connection;
        try {
            while ((connection = connectionList.poll()) != null) {
                if (!connection.getAutoCommit()) {
                    connection.commit();
                }
                connection.close();
            }
        } catch (SQLException ex) {
            throw new ConnectionPoolException(ex);
        }

    }

}
