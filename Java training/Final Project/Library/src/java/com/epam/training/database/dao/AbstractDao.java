/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.database.dao;

import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Entity;
import java.util.List;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * <code>AbstractDao</code> class provides abstract interface to database.
 *
 * @author Artem Kravchenko
 */
public abstract class AbstractDao {

    protected Logger logger;
    protected static String CHARSET = "UTF-8";

    public AbstractDao() {
        BasicConfigurator.configure();
        logger = Logger.getLogger(AbstractDao.class);
    }

    /**
     *
     * @return DB table size.
     * @throws DaoException
     */
    abstract public int getTableSize() throws DaoException;

    /**
     *
     * @param offset
     * @param recordNumber
     * @return Entity object list according DB table.
     * @throws DaoException
     */
    abstract public List<? extends Entity> getRecords(int offset, int recordNumber) throws DaoException;

    /**
     * Delete Entity object from DB.
     *
     * @param record
     * @return true if object is removed successful, otherwise - false;
     * @throws DaoException
     */
    abstract public Boolean deleteRecord(Entity record) throws DaoException;

    /**
     *
     * @param record
     * @return
     * @throws DaoException
     */
    abstract public Boolean addRecord(Entity record) throws DaoException;
}
