/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.database.dao;

import com.epam.training.database.ConnectionPool;
import com.epam.training.database.dao.exceptions.ConnectionPoolException;
import com.epam.training.database.dao.exceptions.BookDaoException;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.Entity;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The <code>BookDao</code> class extends <code>AbstractDao</code> class and
 * contains methods that bind the <code>Book</code> entity with Database.
 *
 * @author Artem Kravchenko
 * @see AbstractDao
 *
 */
public class BookDao extends AbstractDao {

    private static volatile BookDao instance;

    private final static String SQL_ADD_BOOK = "INSERT INTO book (bk_name, bk_year, bk_pageCount, bk_publishingHouse, bk_description, bk_writer, bk_copyNumber) values (?,?,?,?,?,?,?)";
    private final static String SQL_GET_BOOK_LIST_OFFSET = "SELECT * from book limit ?,?";
    private final static String SQL_GET_BOOK_LIST = "SELECT * FROM book";
    private final static String SQL_GET_BOOK_LIST_SIZE = "SELECT COUNT(*) FROM book";
    private final static String SQL_GET_BOOK_BY_NAME = "SELECT * FROM book WHERE bk_name like ?";
    private final static String SQL_GET_BOOK_BY_ID = "SELECT * FROM book WHERE bk_id=?";
    private final static String SQL_SET_COPIES = "UPDATE book SET bk_copyNumber=? WHERE bk_id=?";
    private final static String SQL_DELETE_BOOK = "DELETE FROM book WHERE bk_id=?";

    private static final String COLUMN_BOOK_ID = "bk_id";
    private static final String COLUMN_BOOK_NAME = "bk_name";
    private static final String COLUMN_BOOK_YEAR = "bk_year";
    private static final String COLUMN_BOOK_PAGECOUNT = "bk_pageCount";
    private static final String COLUMN_BOOK_PUBLISH = "bk_publishingHouse";
    private static final String COLUMN_BOOK_DESCRIPTION = "bk_description";
    private static final String COLUMN_BOOK_WRITER = "bk_writer";
    private static final String COLUMN_BOOK_COPYNUMBER = "bk_copyNumber";

    private BookDao() {
    }

    /**
     * Creates <code>BookDao</code> instance if it's not exist.
     *
     * @return <code>BookDao</code> instance
     */
    public static synchronized BookDao getInstance() {
        if (instance == null) {
            instance = new BookDao();
        }
        return instance;
    }

    /**
     *
     * @return <code>Book</code> table size.
     * @throws BookDaoException
     */
    @Override
    public int getTableSize() throws BookDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int resultNumber = 0;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOK_LIST_SIZE);
                    ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    resultNumber = resultSet.getInt(1);
                }
            }
            return resultNumber;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }
    }

    /**
     *
     * @param offset
     * @param recordNumber
     * @return list of book records.
     * @throws BookDaoException
     */
    @Override
    public ArrayList<Book> getRecords(int offset, int recordNumber) throws BookDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            ArrayList<Book> resultList;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOK_LIST_OFFSET)) {
                preparedStatement.setInt(1, offset);
                preparedStatement.setInt(2, recordNumber);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    resultList = new ArrayList<>();

                    while (resultSet.next()) {
                        Book book = new Book();

                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));
                        book.setYear(resultSet.getInt(COLUMN_BOOK_YEAR));
                        book.setPageCount(resultSet.getInt(COLUMN_BOOK_PAGECOUNT));
                        book.setPublishingHouse(resultSet.getString(COLUMN_BOOK_PUBLISH));
                        book.setBookDescription(resultSet.getString(COLUMN_BOOK_DESCRIPTION));
                        book.setWriter(resultSet.getString(COLUMN_BOOK_WRITER));
                        book.setCountOfCopies(resultSet.getInt(COLUMN_BOOK_COPYNUMBER));

                        resultList.add(book);
                    }
                }

            }
            return resultList;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }

    }

    /**
     *
     * @param record
     * @return true if record removed successful, otherwise - false.
     * @throws DaoException
     */
    @Override
    public Boolean deleteRecord(Entity record) throws DaoException {
        Book book = (Book) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BOOK)) {
                preparedStatement.setInt(1, book.getId());

                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }
    }

    /**
     * Add new book record to the database.
     *
     * @param record
     * @return true if adding book to the database was successful, else return
     * false.
     * @throws BookDaoException
     */
    @Override
    public Boolean addRecord(Entity record) throws BookDaoException {
        Book book = (Book) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_BOOK)) {
                preparedStatement.setString(1, book.getName());
                preparedStatement.setInt(2, book.getYear());
                preparedStatement.setInt(3, book.getPageCount());
                preparedStatement.setString(4, book.getPublishingHouse());
                preparedStatement.setString(5, book.getBookDescription());
                preparedStatement.setString(6, book.getWriter());
                preparedStatement.setInt(7, book.getCountOfCopies());
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }
    }

    /**
     * Find in the database book with the specified name.
     *
     * @param name
     * @return list of books according the name.
     * @throws BookDaoException
     */
    public ArrayList<Book> getBookByName(String name) throws BookDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            ArrayList<Book> bookList = new ArrayList<>();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOK_BY_NAME)) {
                String resultName = "%" + name + "%";
                preparedStatement.setBytes(1, resultName.getBytes(CHARSET));
                try (ResultSet resultSet = preparedStatement.executeQuery()) {

                    while (resultSet.next()) {
                        Book book = new Book();

                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));
                        book.setYear(resultSet.getInt(COLUMN_BOOK_YEAR));
                        book.setPageCount(resultSet.getInt(COLUMN_BOOK_PAGECOUNT));
                        book.setPublishingHouse(resultSet.getString(COLUMN_BOOK_PUBLISH));
                        book.setBookDescription(resultSet.getString(COLUMN_BOOK_DESCRIPTION));
                        book.setWriter(resultSet.getString(COLUMN_BOOK_WRITER));
                        book.setCountOfCopies(resultSet.getInt(COLUMN_BOOK_COPYNUMBER));

                        bookList.add(book);

                    }
                }
            }
            return bookList;

        } catch (SQLException | ConnectionPoolException | UnsupportedEncodingException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }
    }

    /**
     * Find in the database book with the specified id.
     *
     * @param id
     * @return book according its ID
     * @throws BookDaoException
     */
    public Book getBookById(int id) throws BookDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            Book book = null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOK_BY_ID)) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {

                    if (resultSet.next()) {
                        book = new Book();

                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));
                        book.setYear(resultSet.getInt(COLUMN_BOOK_YEAR));
                        book.setPageCount(resultSet.getInt(COLUMN_BOOK_PAGECOUNT));
                        book.setPublishingHouse(resultSet.getString(COLUMN_BOOK_PUBLISH));
                        book.setBookDescription(resultSet.getString(COLUMN_BOOK_DESCRIPTION));
                        book.setWriter(resultSet.getString(COLUMN_BOOK_WRITER));
                        book.setCountOfCopies(resultSet.getInt(COLUMN_BOOK_COPYNUMBER));
                    }
                }
            }
            return book;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }

    }

    /**
     * Set a new value of book copies.
     *
     * @param bookId
     * @param countOfcopies - new copies value.
     * @return true if copy setting is sucsessful.
     * @throws BookDaoException
     */
    public boolean setCountOfCopies(int bookId, int countOfcopies) throws BookDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_COPIES)) {
                preparedStatement.setInt(1, countOfcopies);
                preparedStatement.setInt(2, bookId);
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new BookDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new BookDaoException(ex);
            }
        }
    }

}
