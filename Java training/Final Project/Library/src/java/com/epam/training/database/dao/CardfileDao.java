/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.database.dao;

import com.epam.training.database.ConnectionPool;
import com.epam.training.database.dao.exceptions.CardfileDaoException;
import com.epam.training.database.dao.exceptions.ConnectionPoolException;
import com.epam.training.database.dao.exceptions.DaoException;
import com.epam.training.entity.Book;
import com.epam.training.entity.CardRecord;
import com.epam.training.entity.Entity;
import com.epam.training.entity.User;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

/**
 * The <code>CardfileDao</code> class extends <code>AbstractDao</code> class and
 * contains methods that bind the <code>CardRecord</code> entity with Database.
 *
 * @author Artem Kravchenko
 * @see AbstractDao
 * @see BookDao
 * @see UserDao
 */
public class CardfileDao extends AbstractDao {

    private static volatile CardfileDao instance;

    private static final String SQL_ADD_RECORD = "INSERT INTO cardfile (user_id, book_bk_id) values (?,?)";
    private static final String SQL_ADD_FULL_RECORD = "INSERT INTO cardfile (user_id, book_bk_id, isDelivery,"
            + " dateOfDelivery, destination, isReturned) values (?,?,?,?,?,?)";
    private static final String SQL_GET_BOOKS_BY_USER_OFFSET = "SELECT * FROM online_library.cardfile\n"
            + "left join book on (book.bk_id = cardfile.book_bk_id) "
            + "left join user on (user.u_id = cardfile.user_id) "
            + "WHERE cardfile.user_id = ? order by c_id DESC limit ?,?";

    private static final String SQL_GET_BOOKS_BY_USER = "SELECT * FROM cardfile WHERE user_id = ?";
    private static final String SQL_GET_ALL_RECORDS = "SELECT * FROM online_library.cardfile "
            + "left join book on (book.bk_id = cardfile.book_bk_id) "
            + "left join user on (user.u_id = cardfile.user_id)";
    private static final String SQL_GET_TABLE_SIZE = "SELECT (*) FROM online_library.cardfile";
    private static final String SQL_GET_ALL_RECORDS_OFFSET = "SELECT * FROM online_library.cardfile "
            + "left join book on (book.bk_id = cardfile.book_bk_id) "
            + "left join user on (user.u_id = cardfile.user_id) order by c_id DESC limit ?,?";
    private static final String SQL_CONFIRM_RECORD = "UPDATE cardfile SET isDelivery = ?, dateOfDelivery = ?, destination = ? WHERE c_id = ?";
    private static final String SQL_RETURN_BOOK = "UPDATE `online_library`.`cardfile` SET `isReturned`='1' "
            + "WHERE `c_id`=?";
    private static final String SQL_WAITING_USERS = "SELECT * FROM cardfile WHERE book_bk_id = ? and isDelivery = 0";
    private static final String SQL_CHECK_RECORD = "SELECT * FROM online_library.cardfile WHERE user_id = ? and book_bk_id= ? and isReturned=0; ";
    private static final String SQL_GET_BOOK = "SELECT * from book join cardfile on "
            + "(cardfile.book_bk_id = book.bk_id) where cardfile.c_id = ?";
    private static final String SQL_DELETE_ORDER = "DELETE from cardfile where c_id=?";

    private static final String COLUMN_USER_ID = "u_id";
    private static final String COLUMN_USER_LOGIN = "u_login";
    private static final String COLUMN_BOOK_ID = "bk_id";
    private static final String COLUMN_BOOK_NAME = "bk_name";
    private static final String COLUMN_BOOK_YEAR = "bk_year";
    private static final String COLUMN_BOOK_PAGECOUNT = "bk_pageCount";
    private static final String COLUMN_BOOK_PUBLISH = "bk_publishingHouse";
    private static final String COLUMN_BOOK_DESCRIPTION = "bk_description";
    private static final String COLUMN_BOOK_WRITER = "bk_writer";
    private static final String COLUMN_BOOK_COPYNUMBER = "bk_copyNumber";

    private static final String COLUMN_CARDFILE_ID = "c_id";
    private static final String COLUMN_CARDFILE_ISDELIVERY = "isDelivery";
    private static final String COLUMN_CARDFILE_DATE = "dateOfDelivery";
    private static final String COLUMN_CARDFILE_ISRETURN = "isReturned";
    private static final String COLUMN_CARDFILE_DESTINATION = "destination";

    private CardfileDao() {
    }

    /**
     * Creates <code>CardfileDao</code> instance if it's not exist.
     *
     * @return <code>CardfileDao</code> instance
     */
    public static synchronized CardfileDao getInstance() {
        if (instance == null) {
            instance = new CardfileDao();
        }
        return instance;
    }

    /**
     *
     * @return <code>CardRecord</code> table size.
     * @throws CardfileDaoException
     */
    @Override
    public int getTableSize() throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int resultNumber = 0;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TABLE_SIZE);
                    ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    resultNumber = resultSet.getInt(1);
                }
            }
            return resultNumber;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param offset
     * @param recordNumber
     * @return list of card records.
     * @throws CardfileDaoException
     */
    @Override
    public ArrayList<CardRecord> getRecords(int offset, int recordNumber) throws CardfileDaoException {
        Connection connection = null;
        ArrayList<CardRecord> recordList = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_ALL_RECORDS_OFFSET)) {
                preparedStatement.setInt(1, offset);
                preparedStatement.setInt(2, recordNumber);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    recordList = new ArrayList<>();

                    while (resultSet.next()) {

                        CardRecord record = new CardRecord();

                        User user = new User();
                        user.setId(resultSet.getInt(COLUMN_USER_ID));
                        user.setLogin(resultSet.getString(COLUMN_USER_LOGIN));

                        Book book = new Book();
                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));

                        record.setBook(book);
                        record.setUser(user);
                        record.setId(resultSet.getInt(COLUMN_CARDFILE_ID));
                        record.setIsDelivery(resultSet.getBoolean(COLUMN_CARDFILE_ISDELIVERY));
                        record.setDateOfDelivery(resultSet.getTimestamp(COLUMN_CARDFILE_DATE));
                        record.setIsReturned(resultSet.getBoolean(COLUMN_CARDFILE_ISRETURN));
                        record.setDestination(resultSet.getString(COLUMN_CARDFILE_DESTINATION));

                        recordList.add(record);

                    }
                }
            }
            return recordList;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }

    }

    /**
     *
     * @param record
     * @return true if record added successful, otherwise - false.
     * @throws DaoException
     */
    @Override
    public Boolean addRecord(Entity record) throws DaoException {
        CardRecord cardRecord = (CardRecord) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_FULL_RECORD)) {
                preparedStatement.setInt(1, cardRecord.getUser().getId());
                preparedStatement.setInt(2, cardRecord.getBook().getId());
                preparedStatement.setBoolean(3, cardRecord.isIsDelivery());
                preparedStatement.setTimestamp(4, cardRecord.getDateOfDelivery());
                preparedStatement.setString(5, cardRecord.getDestination());
                preparedStatement.setBoolean(6, cardRecord.isIsReturned());
                result = preparedStatement.executeUpdate();
            }
            return result != 0;

        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }

    }

    /**
     *
     * @param record
     * @return true if record removed successful, otherwise - false.
     * @throws DaoException
     */
    @Override
    public Boolean deleteRecord(Entity record) throws DaoException {
        CardRecord cardRecord = (CardRecord) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ORDER)) {
                preparedStatement.setInt(1, cardRecord.getId());

                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param userId
     * @param bookId
     * @return true if record added successful, otherwise - false.
     * @throws com.epam.training.database.dao.exceptions.CardfileDaoException
     */
    public boolean addRecord(int userId, int bookId) throws CardfileDaoException {
        Connection connection = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_RECORD)) {
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, bookId);
                result = preparedStatement.executeUpdate();
            }
            return result != 0;

        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }

    }

    /**
     *
     * @param userId
     * @return <code>CardRecord</code> table size according current user.
     * @throws CardfileDaoException
     */
    public int getTableSize(int userId) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int resultNumber = 0;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOKS_BY_USER)) {
                preparedStatement.setInt(1, userId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        resultNumber++;
                    }
                }
            }
            return resultNumber;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param id
     * @param destination
     * @return true if record confirm successful, otherwise - false.
     * @throws CardfileDaoException
     */
    public boolean confirmRecord(int id, String destination) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CONFIRM_RECORD)) {
                Timestamp date = new Timestamp(System.currentTimeMillis());
                preparedStatement.setInt(1, 1);
                preparedStatement.setTimestamp(2, date);
                preparedStatement.setBytes(3, destination.getBytes(CHARSET));
                preparedStatement.setInt(4, id);
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (UnsupportedEncodingException | ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param id
     * @return true if order canceled successful, otherwise - false.
     * @throws CardfileDaoException
     */
    public boolean cancelOrder(int id) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ORDER)) {
                preparedStatement.setInt(1, id);
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param userId
     * @param offset
     * @param recordNumber
     * @return list of user <code>CardRecord</code>
     * @throws CardfileDaoException
     */
    public ArrayList<CardRecord> getUserRecords(int userId, int offset, int recordNumber) throws CardfileDaoException {
        Connection connection = null;
        ArrayList<CardRecord> recordList = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOKS_BY_USER_OFFSET)) {
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, recordNumber);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    recordList = new ArrayList<>();

                    while (resultSet.next()) {
                        CardRecord record = new CardRecord();

                        User user = new User();
                        user.setId(resultSet.getInt(COLUMN_USER_ID));
                        user.setLogin(resultSet.getString(COLUMN_USER_LOGIN));

                        Book book = new Book();
                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));

                        record.setBook(book);
                        record.setUser(user);

                        record.setId(resultSet.getInt(COLUMN_CARDFILE_ID));
                        record.setIsDelivery(resultSet.getBoolean(COLUMN_CARDFILE_ISDELIVERY));
                        record.setDateOfDelivery(resultSet.getTimestamp(COLUMN_CARDFILE_DATE));
                        record.setIsReturned(resultSet.getBoolean(COLUMN_CARDFILE_ISRETURN));
                        record.setDestination(resultSet.getString(COLUMN_CARDFILE_DESTINATION));

                        recordList.add(record);

                    }
                }
            }
            return recordList;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     * Change the user record, set the book as the returned.
     *
     * @param recordId
     * @return true if successful, otherwise - false.
     * @throws CardfileDaoException
     */
    public boolean returnBook(int recordId) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_RETURN_BOOK)) {
                preparedStatement.setInt(1, recordId);
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     *
     * @param bookId
     * @return number of users in the queue for the book.
     * @throws CardfileDaoException
     */
    public int waitingUsersCount(int bookId) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_WAITING_USERS)) {
                preparedStatement.setInt(1, bookId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    result = 0;
                    while (resultSet.next()) {
                        result++;
                    }
                }
            }
            return result;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     * Check if record with <code>userId</code> and <code>bookId</code>
     * parametrs is exist.
     *
     * @param userId User ID.
     * @param bookId Book ID.
     * @return true if such record is exist.
     * @throws CardfileDaoException
     */
    public boolean checkUserRecord(int userId, int bookId) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CHECK_RECORD)) {
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, bookId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return resultSet.next();
                }
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

    /**
     * Return Book according this <code>recordId</code>.
     *
     * @param recordId
     * @return <code>Book</code> object
     * @throws CardfileDaoException
     */
    public Book getBookByRecord(int recordId) throws CardfileDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            Book book = null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BOOK)) {
                preparedStatement.setInt(1, recordId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        book = new Book();

                        book.setId(resultSet.getInt(COLUMN_BOOK_ID));
                        book.setName(resultSet.getString(COLUMN_BOOK_NAME));
                        book.setYear(resultSet.getInt(COLUMN_BOOK_YEAR));
                        book.setPageCount(resultSet.getInt(COLUMN_BOOK_PAGECOUNT));
                        book.setPublishingHouse(resultSet.getString(COLUMN_BOOK_PUBLISH));
                        book.setBookDescription(resultSet.getString(COLUMN_BOOK_DESCRIPTION));
                        book.setWriter(resultSet.getString(COLUMN_BOOK_WRITER));
                        book.setCountOfCopies(resultSet.getInt(COLUMN_BOOK_COPYNUMBER));
                    }
                }
            }
            return book;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new CardfileDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new CardfileDaoException(ex);
            }
        }
    }

}
