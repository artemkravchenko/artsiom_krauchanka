/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.database.dao;

import com.epam.training.database.ConnectionPool;
import com.epam.training.database.dao.exceptions.ConnectionPoolException;
import com.epam.training.database.dao.exceptions.UserDaoException;
import com.epam.training.entity.Entity;
import com.epam.training.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The <code>UserDao</code> class extends <code>AbstractDao</code> class and
 * contains methods that bind the <code>User</code> entity with Database.
 *
 * @author Artem Kravchenko
 * @see AbstractDao
 * @see BookDao
 * @see CardfileDao
 */
public class UserDao extends AbstractDao {

    private static volatile UserDao instance;
    private final static String SQL_GET_USERS = "SELECT * FROM user";
    private final static String SQL_GET_TABLE_SIZE = "SELECT COUNT(*) FROM user";
    private final static String SQL_FIND_BY_LOGIN = "SELECT u_id FROM user WHERE u_login=?";
    private final static String SQL_ADD_USER = "INSERT INTO user (u_login, u_password, u_email) values (?,?,?)";
    private final static String SQL_GET_USER = "SELECT * FROM user WHERE u_login=? and u_password=?";
    private final static String SQL_GET_USERS_OFFSET = "SELECT * FROM user limit ?,?";
    private final static String SQL_DELETE_USER = "DELETE FROM user WHERE u_id=?";

    private static final String COLUMN_USER_ID = "u_id";
    private static final String COLUMN_USER_LOGIN = "u_login";
    private static final String COLUMN_USER_PASSWORD = "u_password";
    private static final String COLUMN_USER_EMAIL = "u_email";
    private static final String COLUMN_USER_ISADMIN = "u_isAdmin";

    private UserDao() {
    }

    /**
     * Creates <code>UserDao</code> instance if it's not exist.
     *
     * @return <code>UserDao</code> instance
     */
    public synchronized static UserDao getInstance() {
        if (instance == null) {
            instance = new UserDao();
        }
        return instance;
    }

    /**
     *
     * @return <code>User</code> table size.
     * @throws UserDaoException
     */
    @Override
    public int getTableSize() throws UserDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int resultNumber = 0;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TABLE_SIZE);
                    ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    resultNumber = resultSet.getInt(1);
                }
            }
            return resultNumber;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }
    }

    /**
     *
     * @param offset
     * @param recordNumber
     * @return list of user records.
     * @throws UserDaoException
     */
    @Override
    public ArrayList<User> getRecords(int offset, int recordNumber) throws UserDaoException {
        Connection connection = null;
        ArrayList<User> recordList = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USERS_OFFSET)) {
                preparedStatement.setInt(1, offset);
                preparedStatement.setInt(2, recordNumber);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    recordList = new ArrayList<>();
                    User user;
                    while (resultSet.next()) {

                        user = new User();
                        user.setId(resultSet.getInt(COLUMN_USER_ID));
                        user.setLogin(resultSet.getString(COLUMN_USER_LOGIN));
                        user.setPassword(resultSet.getString(COLUMN_USER_PASSWORD));
                        user.setEmail(resultSet.getString(COLUMN_USER_EMAIL));
                        user.setIsAdmin(resultSet.getBoolean(COLUMN_USER_ISADMIN));

                        recordList.add(user);

                    }
                }
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }
        return recordList;
    }

    /**
     * delete user from database if it exist.
     *
     * @param record
     *
     * @return true if record removed successful, otherwise - false.
     * @throws UserDaoException
     */
    @Override
    public Boolean deleteRecord(Entity record) throws UserDaoException {
        User user = (User) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER)) {
                preparedStatement.setInt(1, user.getId());
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }

    }

    /**
     * Add a new user to the database.
     *
     * @param record - new user
     * @return true if record added successful, otherwise - false.
     * @throws com.epam.training.database.dao.exceptions.UserDaoException
     */
    @Override
    public Boolean addRecord(Entity record) throws UserDaoException {
        User user = (User) record;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            int result;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER)) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.setString(2, user.getPassword());
                preparedStatement.setString(3, user.getEmail());
                result = preparedStatement.executeUpdate();
            }
            return result != 0;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }

    }

    /**
     * return user with login/password if it exist
     *
     * @param login
     * @param password
     * @return <code>User</code> object
     * @throws com.epam.training.database.dao.exceptions.UserDaoException
     */
    public User getUser(String login, String password) throws UserDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            User user;
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USER)) {
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, password);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    user = null;
                    if (resultSet.next()) {
                        user = new User();
                        user.setId(resultSet.getInt(COLUMN_USER_ID));
                        user.setLogin(resultSet.getString(COLUMN_USER_LOGIN));
                        user.setPassword(resultSet.getString(COLUMN_USER_PASSWORD));
                        user.setEmail(resultSet.getString(COLUMN_USER_EMAIL));
                        user.setIsAdmin(resultSet.getBoolean(COLUMN_USER_ISADMIN));
                    }
                }
            }

            return user;
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }

    }

    /**
     * Check if user with login is exist.
     *
     * @param login - user login
     * @return
     * @throws com.epam.training.database.dao.exceptions.UserDaoException
     */
    public synchronized boolean isUserExist(String login) throws UserDaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_LOGIN)) {
                preparedStatement.setString(1, login);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return resultSet.next();
                }
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new UserDaoException(ex);
        } finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (ConnectionPoolException ex) {
                throw new UserDaoException(ex);
            }
        }
    }

}
