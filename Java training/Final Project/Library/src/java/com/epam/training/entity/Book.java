/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.entity;

import java.util.Objects;

/**
 * class <code>Book</code> is DB entity class which describes <code>Book</code>
 * DB table .
 *
 * @author Artem Kravchenko
 */
public class Book extends Entity {

    
    private String name;
    private int year;
    private int pageCount;
    private int countOfCopies;
    private String bookDescription;
    private String publishingHouse;
    private String writer;

    /**
     * Public constructor without parameters. Creates a new <code>Book</code>
     * object with default values.
     */
    public Book() {
    }

    /**
     * Public constructor with parameters. Creates a new <code>Book</code>
     * object with current values.
     *
     *
     * @param name - book name
     * @param year - writing date
     * @param pageCount - count of page
     * @param countOfCopies - count of copies in the library
     * @param bookDescription - short book description
     * @param publishingHouse - name of the publisher
     * @param writer - writer name
     */
    public Book(String name, int year, int pageCount, int countOfCopies, String bookDescription, String publishingHouse, String writer) {
      
        this.name = name;
        this.year = year;
        this.pageCount = pageCount;
        this.countOfCopies = countOfCopies;
        this.bookDescription = bookDescription;
        this.publishingHouse = publishingHouse;
        this.writer = writer;
    }

    

    /**
     *
     * @return book name.
     */
    public String getName() {
        return name;
    }

    /**
     * set book name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return book year.
     */
    public int getYear() {
        return year;
    }

    /**
     * Set book year.
     *
     * @param year
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     *
     * @return book writer.
     */
    public String getWriter() {
        return writer;
    }

    /**
     * Set book writer.
     *
     * @param writer
     */
    public void setWriter(String writer) {
        this.writer = writer;
    }

    /**
     *
     * @return book count of page.
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Set page cpunt.
     *
     * @param pageCount
     */
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    /**
     *
     * @return count of copies.
     */
    public int getCountOfCopies() {
        return countOfCopies;
    }

    /**
     * Set count of copies.
     *
     * @param countOfCopies
     */
    public void setCountOfCopies(int countOfCopies) {
        this.countOfCopies = countOfCopies;
    }

    /**
     *
     * @return book description.
     */
    public String getBookDescription() {
        return bookDescription;
    }

    /**
     * Set book description.
     *
     * @param bookDescription
     */
    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    /**
     *
     * @return book publishing house.
     */
    public String getPublishingHouse() {
        return publishingHouse;
    }

    /**
     * Set book publishing house.
     *
     * @param publishingHouse
     */
    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return "Book{" + "id=" + getId() + ", name=" + name + ", year=" + year + ", pageCount="
                + pageCount + ", countOfCopies=" + countOfCopies + ", bookDescription="
                + bookDescription + ", publishingHouse=" + publishingHouse + ", writer="
                + writer + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.getId();
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + this.year;
        hash = 67 * hash + this.pageCount;
        hash = 67 * hash + this.countOfCopies;
        hash = 67 * hash + Objects.hashCode(this.bookDescription);
        hash = 67 * hash + Objects.hashCode(this.publishingHouse);
        hash = 67 * hash + Objects.hashCode(this.writer);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.year != other.year) {
            return false;
        }
        if (this.pageCount != other.pageCount) {
            return false;
        }
        if (this.countOfCopies != other.countOfCopies) {
            return false;
        }
        if (!Objects.equals(this.bookDescription, other.bookDescription)) {
            return false;
        }
        if (!Objects.equals(this.publishingHouse, other.publishingHouse)) {
            return false;
        }
        if (!Objects.equals(this.writer, other.writer)) {
            return false;
        }
        return true;
    }

}
