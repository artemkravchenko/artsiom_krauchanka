/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.entity;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * class <code>CardRecord</code> is DB entity class which describes
 * <code>CardRecord</code> DB table .
 *
 * @author Artem Kravchenko
 */
public class CardRecord extends Entity {

    private User user;
    private Book book;
    private boolean isDelivery;
    private Timestamp dateOfDelivery;
    private boolean isReturned;
    private String destination;

    /**
     * Public constructor without parameters. Creates a new
     * <code>CardRecord</code> object with default values.
     */
    public CardRecord() {
    }

    /**
     * Public constructor with parameters. Creates a new <code>CardRecord</code>
     * object with current values.
     *
     *
     * @param user - <code>User</code> object.
     * @param book - <code>Book</code> object.
     * @param isDelivery - is book delivered to the destination.
     * @param dateOfDelivery - date of delivery in YYYY-MM-DD HH-MM-SS format.
     * @param destination - delivery destination.
     * @param isReturned - is book returned to the library.
     *
     */
    public CardRecord(User user, Book book, boolean isDelivery, Timestamp dateOfDelivery, boolean isReturned, String destination) {

        this.user = user;
        this.book = book;
        this.isDelivery = isDelivery;
        this.dateOfDelivery = dateOfDelivery;
        this.isReturned = isReturned;
        this.destination = destination;
    }

    /**
     *
     * @return <code>User</code> object
     */
    public User getUser() {
        return user;
    }

    /**
     * set <code>User</code> object
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return <code>Book</code> object.
     */
    public Book getBook() {
        return book;
    }

    /**
     * set <code>Book</code> object.
     *
     * @param book
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     *
     * @return date of book delivery
     */
    public Timestamp getDateOfDelivery() {
        return dateOfDelivery;
    }

    /**
     * set date of book delivery
     *
     * @param dateOfDelivery
     */
    public void setDateOfDelivery(Timestamp dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    /**
     *
     * @return is book delivery
     */
    public boolean isIsDelivery() {
        return isDelivery;
    }

    /**
     * set is book delivery
     *
     * @param isDelivery
     */
    public void setIsDelivery(boolean isDelivery) {
        this.isDelivery = isDelivery;
    }

    /**
     *
     * @return is book returned
     */
    public boolean isIsReturned() {
        return isReturned;
    }

    /**
     * set is book returned
     *
     * @param isReturned
     */
    public void setIsReturned(boolean isReturned) {
        this.isReturned = isReturned;
    }

    /**
     *
     * @return delivery destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * set delivery destination
     *
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.getId();
        hash = 89 * hash + Objects.hashCode(this.user);
        hash = 89 * hash + Objects.hashCode(this.book);
        hash = 89 * hash + (this.isDelivery ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.dateOfDelivery);
        hash = 89 * hash + (this.isReturned ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.destination);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CardRecord other = (CardRecord) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.book, other.book)) {
            return false;
        }
        if (this.isDelivery != other.isDelivery) {
            return false;
        }
        if (!Objects.equals(this.dateOfDelivery, other.dateOfDelivery)) {
            return false;
        }
        if (this.isReturned != other.isReturned) {
            return false;
        }
        if (!Objects.equals(this.destination, other.destination)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CardRecord{" + "id=" + getId() + ", user=" + user + ", book=" + book + ", isDelivery=" + isDelivery + ", dateOfDelivery=" + dateOfDelivery + ", isReturned=" + isReturned + ", destination=" + destination + '}';
    }

}
