/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.entity;

import java.io.Serializable;

/**
 * abstract class <code>IEntity</code> provide a common API for entity classes.
 *
 * @author Artem Kravchenko
 */
public abstract class Entity implements Serializable {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
