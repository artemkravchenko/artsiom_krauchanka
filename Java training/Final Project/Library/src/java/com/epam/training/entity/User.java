/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.entity;

import java.util.Objects;

/**
 * class <code>User</code> is DB entity class which describes <code>User</code>
 * DB table .
 *
 * @author Artem Kravchenko
 */
public class User extends Entity {

    private String login;
    private String password;
    private String email;
    private boolean isAdmin;

    /**
     * Public constructor without parameters. Creates a new <code>User</code>
     * object with default values.
     */
    public User() {
    }

    /**
     * Public constructor with parameters. Creates a new <code>User</code>
     * object with current values.
     *
     *
     * @param login - user login.
     * @param password - user password.
     * @param email - user email.
     * @param isAdmin - whether is the user the administrator.
     */
    public User(String login, String password, String email, boolean isAdmin) {

        this.login = login;
        this.password = password;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    /**
     *
     * @return user login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Set user Login.
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return user password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set user password.
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return user e-mail.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set user e-mail.
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return whether is the user the administrator
     */
    public boolean isAdmin() {
        return isAdmin;
    }

    /**
     *
     * @param isAdmin
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + getId();
        hash = 53 * hash + Objects.hashCode(this.login);
        hash = 53 * hash + Objects.hashCode(this.password);
        hash = 53 * hash + Objects.hashCode(this.email);
        hash = 53 * hash + (this.isAdmin ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (this.isAdmin != other.isAdmin) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + getId() + ", login=" + login + ", password=" + password
                + ", email=" + email + ", isAdmin=" + isAdmin + '}';
    }

}
