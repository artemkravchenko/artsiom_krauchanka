/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.filter;

/**
 *
 * @author Artem Kravchenko
 */
public class AccessPermissionException extends Exception {

    /**
     * Constructs a new exception with {@code null} as its detail message. The
     * cause is not initialized.
     */
    public AccessPermissionException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message. The cause
     * is not initialized.
     *
     * @param message the detail message.
     */
    public AccessPermissionException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message.
     *
     *
     * @param cause the cause.
     */
    public AccessPermissionException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     *
     * @param message the detail message .
     * @param cause the cause.
     */
    public AccessPermissionException(String message, Throwable cause) {
        super(message, cause);
    }

}
