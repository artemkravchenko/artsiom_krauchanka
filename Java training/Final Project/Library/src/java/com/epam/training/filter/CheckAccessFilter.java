/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.filter;

import com.epam.training.command.CommandEnum;
import com.epam.training.entity.User;
import com.epam.training.resource.Resource;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <code>CheckAccessFilter</code> class is a servlet filter. Its implements
 * <code>Filter</code> interface. The filter purpose is to check access of
 * current user to perform current command.
 *
 * @author Artem Kravchenko
 * @see Filter
 */
public class CheckAccessFilter implements Filter {

    private static final String PARAM_USER = "user";
    private static final String PARAM_COMMAND = "command";
    private static final String PARAM_EXCEPTION = "exception";
    private static final String PARAM_MESSAGE = "error.filter.message";

    private static final String FORWARD_ERROR = "forward.error";

    /**
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute(PARAM_USER);
        CommandEnum commandEnum = CommandEnum.valueOf(req.getParameter(PARAM_COMMAND));
        boolean checkAccess = false;

        switch (commandEnum.getCommandPrivilege()) {
            case ADMIN:
                if (user != null) {
                    if (!user.isAdmin()) {
                        checkAccess = true;
                    }
                } else {
                    checkAccess = true;
                }
                break;
            case USER:
                if (user != null) {
                    if (user.isAdmin()) {
                        checkAccess = true;
                    }
                } else {
                    checkAccess = true;
                }
                break;
            case ALL:
                if (user == null) {
                    checkAccess = true;
                }
                break;
            case UNREPORT:
                break;
        }

        if (!checkAccess) {
            chain.doFilter(request, response);
        } else {
            request.setAttribute(PARAM_EXCEPTION, new AccessPermissionException(Resource.getResource(PARAM_MESSAGE)));
            request.getRequestDispatcher(Resource.getResource(FORWARD_ERROR)).forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }

}
