/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The <code>Resource</code> class provides access to the properties files.
 *
 * @author ArtemKravchenko
 */
public class Resource {

    private static final String PATH = "com.epam.training.resource.Resource";
    private static ResourceBundle resource;

    public Resource() {
    }

    /**
     * Return property value according key and default locale.
     *
     * @param res - property key.
     * @return property value.
     */
    public static String getResource(String res) {
        resource = ResourceBundle.getBundle(PATH);
        return resource.getString(res);
    }

    /**
     * Return property value according key and locale.
     *
     * @param res - property key.
     * @param locale - Locale instance.
     * @return property value.
     */
    public static String getResource(String res, Locale locale) {
        if (locale != null) {
            resource = ResourceBundle.getBundle(PATH, locale);
        } else {
            resource = ResourceBundle.getBundle(PATH);
        }
        return resource.getString(res);
    }
}
