/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * The <code>TablTag</code> class extends <code>BodyTagSupport</code> class. Its
 * displays given collection splitting it on the pages.
 *
 * @author Artem Kravchenko
 */
public class TableTag extends BodyTagSupport {

    private static final String TABLE_BEGIN = "<table class=\"tableSwitch\"><tr>";
    private static final String TABLE_END = "</tr></table>";

    private int numberOfRecords;
    private int recordsPerPage;
    private String tableName;
    private JspWriter writer;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public void setNumberOfRecords(int numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    public int getRecordsPerPage() {
        return recordsPerPage;
    }

    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    /**
     *
     * @return <code>EVAL_BODY_INCLUDE</code>
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        try {
            writer = pageContext.getOut();
            int countOfPages = (int) Math.ceil(numberOfRecords * 1.0 / recordsPerPage);
            String commandName = "GET_" + tableName + "_TABLE";
            writer.print(TABLE_BEGIN);
            for (int i = 1; i <= countOfPages; i++) {
                writer.print("<td><a href=\"WebController?command=" + commandName + "&page="
                        + i + "&recordsPerPage=" + recordsPerPage + "\">"
                        + i + "</a></td>");
            }
            writer.print(TABLE_END);
        } catch (IOException ex) {
            throw new JspException(ex);
        }
        return EVAL_BODY_INCLUDE;
    }

}
