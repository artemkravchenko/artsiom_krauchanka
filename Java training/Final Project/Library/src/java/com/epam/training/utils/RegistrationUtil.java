/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.utils;

import com.epam.training.resource.Resource;

/**
 * <code>RegistrationUtil</code> class provide util methods for user
 * registration.
 *
 * @author Artem Kravchenko
 */
public class RegistrationUtil {

    private static final String PATTERN_LOGIN = "pattern.login";
    private static final String PATTERN_PASSWORD = "pattern.password";
    private static final String PATTERN_EMAIL = "pattern.email";

    /**
     * Check entered username is equivalent to regular expression.
     *
     * @param login - user name.
     * @return true if current username is equivalent to regular expression,
     * otherwise - false.
     */
    public boolean checkLogin(String login) {
        return !login.isEmpty() && login.matches(Resource.getResource(PATTERN_LOGIN));
    }

    /**
     * Check entered password is equivalent to regular expression.
     *
     * @param password - user password.
     * @return true if current password is equivalent to regular expression,
     * otherwise - false.
     */
    public boolean checkPassword(String password) {
        return !password.isEmpty() && password.matches(Resource.getResource(PATTERN_PASSWORD));
    }

    /**
     *
     * @param password
     * @param confirmPassword
     * @return true if current password is equivalent to first password,
     * otherwise - false.
     *
     */
    public boolean checkConfirmPassword(String password, String confirmPassword) {
        return !confirmPassword.isEmpty() && password.equals(confirmPassword);
    }

    /**
     * Check entered email is equivalent to regular expression.
     *
     * @param email
     * @return true if current email is equivalent to regular expression,
     * otherwise - false.
     */
    public boolean checkEmail(String email) {
        return !email.isEmpty() && email.matches(Resource.getResource(PATTERN_EMAIL));
    }
}
