<%-- 
    Document   : result
    Created on : 11.05.2015, 11:11:10
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/tlds/taglib.tld" prefix="tableTag" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <div id="content-wrapper">
            <div id = "wrapper">
                <jsp:include page="../WEB-INF/jspf/header.jsp"/>
                
                <div id = "content">
                    <div class="content-header">
                        <h2 style="color: darkblue; text-indent: 130px; float: left;">
                            <fmt:message bundle='${loc}' key='jsp.main.bookList'/>
                        </h2>

                        <div class="searching">
                            <form action="WebController" method="GET">
                                <input type="hidden" name="command" value="BOOK_SEARCH">
                                <input type="text" name="bookName" autocomplete="off" placeholder="<fmt:message bundle='${loc}' key='jsp.main.search.placeholder'/>">
                                
                                <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.main.search.button'/>">
                            </form>
                        </div>
                    </div>
                            
                    <div id="table">
                        <tableTag:getTable tableName="BOOK" recordsPerPage="7" numberOfRecords="${numberOfBooks}">
                            <jsp:body>
                                <table class="customTable">
                                    <tr> 
                                        <td> <fmt:message bundle='${loc}' key='jsp.main.table.id'/> </td>
                                        <td> <fmt:message bundle='${loc}' key='jsp.main.table.name'/> </td>
                                        <td> <fmt:message bundle='${loc}' key='jsp.main.table.writer'/> </td>
                                        <td> <fmt:message bundle='${loc}' key='jsp.main.table.year'/> </td>
                                        <td> <fmt:message bundle='${loc}' key='jsp.main.table.copies'/> </td>
                                    </tr>
                                    <c:forEach var="book" items="${bookList}">
                                        <tr>
                                            <td> ${book.id} </td>
                                            <td>
                                                <a href="WebController?command=BOOK_VIEW&bookID=${book.id}">
                                                    <strong>${book.name}</strong>
                                                </a>
                                            </td>
                                            <td> ${book.writer} </td>
                                            <td> ${book.year} </td>
                                            <td> ${book.countOfCopies} </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </jsp:body>
                        </tableTag:getTable>   
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../WEB-INF/jspf/footer.jsp"/>
    </body>
</html>
