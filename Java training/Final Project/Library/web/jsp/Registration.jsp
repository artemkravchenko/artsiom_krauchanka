<%-- 
    Document   : Registration
    Created on : 10.05.2015, 21:25:34
    Author     : артём
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<!DOCTYPE html>

<html>
    <head>
        <fmt:setLocale value="${pageContext.session.getAttribute('locale')}"/>
        <fmt:setBundle basename="com.epam.training.resource.Resource" var="loc"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/AuthorizationStyle.css" type="text/css">
        <title> 
            <fmt:message bundle="${loc}" key="jsp.title"/> 
        </title>
    </head>

    <body>
        <section class = "container">
            <div class = "login">
                <div style="float: left; margin:-15px -15px;">
                    <a href="WebController?command=SWITCH_TO_PAGE&forwardPage=<fmt:message bundle='${loc}' key='forward.authorization'/>">
                        <img src="images/arrow.png">
                    </a>
                </div>

                <h1> 
                    <fmt:message bundle='${loc}' key='jsp.registration.header'/> 
                </h1>

                <form action="WebController" method="POST">
                    <fmt:message bundle='${loc}' key='jsp.registration.login'/>

                    <input type="text" name="login" placeholder="<fmt:message bundle='${loc}' key='jsp.registration.login_placeholder'/>"/>

                    <fmt:message bundle='${loc}' key='jsp.registration.password'/>
                    <input type="password" name="password" placeholder="<fmt:message bundle='${loc}' key='jsp.registration.password_placeholder'/>"/>

                    <fmt:message bundle='${loc}' key='jsp.registration.confirm_password'/>
                    <input type="password" name="confirm_password"/>

                    <fmt:message bundle='${loc}' key='jsp.registration.email'/>
                    <input type="text" name="email" placeholder="<fmt:message bundle='${loc}' key='jsp.registration.email_placeholder'/>"/>

                    <input type="hidden" name="command" value="REGISTRATION"/>
                    <input type="submit" value="<fmt:message bundle='${loc}' key='jsp.registration.button'/>">

                </form>
            </div>
            <p id="error_string">${message}</p>
        </section>
    </body>
</html>
