/* Insert data to the AUTHOR table  */
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (1, 'Ford');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (2, 'Andrew');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (3, 'Garry');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (4, 'Hudson');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (5, 'Leon');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (6, 'Frank');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (7, 'James');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (8, 'Willis');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (9, 'Carry');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (10, 'Martin');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (11, 'Cooper');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (12, 'Armstrong');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (13, 'Arnold');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (14, 'Richard');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (15, 'Arthur');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (16, 'Eddy');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (17, 'Michael');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (18, 'Theodor');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (19, 'Peter');
insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (20, 'Ray');


/*  Insert data to the NEWS table  */
insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (1, 'Preamble', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation. Second, to "secure the blessings of liberty", which were to be enjoyed by not only the first generation, but for all who came after, "our posterity".',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');
insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (2, 'Article One','Article One describes the Congress, the legislative branch of the federal government.', 'Article One describes the Congress, the legislative branch of the federal government. Section 1, reads, "All legislative powers herein granted shall be vested in a Congress of the United States, which shall consist of a Senate and House of Representatives." The article establishes the manner of election and the qualifications of members of each body. Representatives must be at least 25 years old, be a citizen of the United States for seven years, and live in the state they represent. Senators must be at least 30 years old, be a citizen for nine years, and live in the state they represent.
Article I, Section 8 enumerates the powers delegated to the legislature. Financially, Congress has the power to tax, borrow, pay debt and provide for the common defense and the general welfare; to regulate commerce, bankruptcies, and coin money. To regulate internal affairs, it has the power to regulate and govern military forces and militias, suppress insurrections and repel invasions. It is to provide for naturalization, standards of weights and measures, post offices and roads, and patents; to directly govern the federal district and cessions of land by the states for forts and arsenals. Internationally, Congress has the power to define and punish piracies and offenses against the Law of Nations, to declare war and make rules of war. The final Necessary and Proper Clause, also known as the Elastic Clause, expressly confers incidental powers upon Congress without the Articles requirement for express delegation for each and every power. Article I, Section 9 lists eight specific limits on congressional power.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (3, 'Article Two','Article Two describes the office of the President of the United States.', 'Article Two describes the office of the President of the United States. The President is head of the executive branch of the federal government, as well as the nations head of state and head of government.
Article Two describes the office, qualifications and duties of the President of the United States and the Vice President. It is modified by the 12th Amendment which tacitly acknowledges political parties, and the 25th Amendment relating to office succession. The president is to receive only one compensation from the federal government. The inaugural oath is specified to preserve, protect and defend the Constitution.
The president is the Commander in Chief of the United States Armed Forces and state militias when they are mobilized. He makes treaties with the advice and consent of a two-thirds quorum of the Senate. To administer the federal government, he commissions all the offices of the federal government as Congress directs; he may require the opinions of its principle officers and make "recess appointments" while Congress is not in session. The president is to see that the laws are faithfully executed, though he may grant reprieves and pardons except regarding Congressional impeachment of himself or other federal officers. The president reports to Congress on the State of the Union, and by the Recommendation Clause, recommends "necessary and expedient" national measures. He may convene and adjourn Congress under special circumstances.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (4, 'Article Three','Article Three describes the court system (the judicial branch), including the Supreme Court.', 'Article Three describes the court system (the judicial branch), including the Supreme Court. There shall be one court called the Supreme Court. The article describes the kinds of cases the court takes as original jurisdiction. Congress can create lower courts and an appeals process. Congress enacts law defining crimes and providing for punishment. Article Three also protects the right to trial by jury in all criminal cases, and defines the crime of treason.
Section 1 vests the judicial power of the United States in federal courts, and with it, the authority to interpret and apply the law to a particular case. Also included is the power to punish, sentence, and direct future action to resolve conflicts. The Constitution outlines the U.S. judicial system. In the Judiciary Act of 1789, Congress began to fill in details. Currently, Title 28 of the U.S. Code[30] describes judicial powers and administration.
As of the First Congress, the Supreme Court justices rode circuit to sit as panels to hear appeals from the district courts.[a] In 1891, Congress enacted a new system. District courts would have original jurisdiction. Intermediate appellate courts (circuit courts) with exclusive jurisdiction heard regional appeals before consideration by the Supreme Court. The Supreme Court holds discretionary jurisdiction, meaning that it does not have to hear every case that is brought to it.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (5, 'Article Four', 'Article Four outlines the relation between the states and the relation between each state and the federal government.', 'Article Four outlines the relation between the states and the relation between each state and the federal government. In addition, it provides for such matters as admitting new states and border changes between the states. For instance, it requires states to give "full faith and credit" to the public acts, records, and court proceedings of the other states. Congress is permitted to regulate the manner in which proof of such acts, records, or proceedings may be admitted. The "privileges and immunities" clause prohibits state governments from discriminating against citizens of other states in favor of resident citizens, e.g., having tougher penalties for residents of Ohio convicted of crimes within Michigan.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (6, 'Article Five','Article Five outlines the process for amending the Constitution.', 'Article Five outlines the process for amending the Constitution. Eight state constitutions in effect in 1787 included an amendment mechanism. Amendment making power rested with the legislature in three of the states and in the other five it was given to specially elected conventions. The Articles of Confederation provided that amendments were to be proposed by Congress and ratified by the unanimous vote of all thirteen state legislatures. This proved to be a major flaw in the Articles, as it created an insurmountable obstacle to constitutional reform. The amendment process crafted during the Philadelphia Constitutional Convention was, according to The Federalist No. 43, designed to establish a balance between pliancy and rigidity',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (7, 'Article Six', 'Article Six establishes the Constitution, and all federal laws and treaties of the United States made according to it.', 'Article Six establishes the Constitution, and all federal laws and treaties of the United States made according to it, to be the supreme law of the land, and that "the judges in every state shall be bound thereby, any thing in the laws or constitutions of any state notwithstanding." It validates national debt created under the Articles of Confederation and requires that all federal and state legislators, officers, and judges take oaths or affirmations to support the Constitution. This means that the states constitutions and laws should not conflict with the laws of the federal constitution and that in case of a conflict, state judges are legally bound to honor the federal laws and constitution over those of any state. Article Six also states "no religious Test shall ever be required as a Qualification to any Office or public Trust under the United States."',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (8, 'Article Seven','Article Seven describes the process for establishing the proposed new frame of government.', 'Article Seven describes the process for establishing the proposed new frame of government. Anticipating that the influence of many state politicians would be Antifederalist, delegates to the Philadelphia Convention provided for ratification of the Constitution by popularly elected ratifying conventions in each state. The convention method also made it possible that judges, ministers and others ineligible to serve in state legislatures, could be elected to a convention. Suspecting that Rhode Island, at least, might not ratify, delegates decided that the Constitution would go into effect as soon as nine states (two-thirds rounded up) ratified.[36] Once ratified by this minimum number of states, it was anticipated that the proposed Constitution would become this Constitution between the nine or more that signed. It would not cover the four or fewer states that might not have signed.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (9, 'Safeguards of liberty', 'The First Amendment (1791) prohibits Congress from obstructing the exercise of certain individual freedoms.', 'The First Amendment (1791) prohibits Congress from obstructing the exercise of certain individual freedoms: freedom of religion, freedom of speech, freedom of the press, freedom of assembly, and right to petition. Its Free Exercise Clause guarantees a persons right to hold whatever religious beliefs he or she wants, and to freely exercise that belief, and its Establishment Clause prevents the federal government from creating an official national church or favoring one set of religious beliefs over another. The amendment guarantees an individuals right to express and to be exposed to a wide range of opinions and views. It was intended to ensure a free exchange of ideas even if the ideas are unpopular. It also guarantees an individuals right to physically gather with a group of people to picket or protest; or associate with others in groups for economic, political or religious purposes. Additionally, it guarantees an individuals right to petition the government for a redress of grievances.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (10, 'Safeguards of justice','The Fourth Amendment (1791) protects people against unreasonable searches and seizures of either self or property by government officials.', 'The Fourth Amendment (1791) protects people against unreasonable searches and seizures of either self or property by government officials. A search can mean everything from a frisking by a police officer or to a demand for a blood test to a search of an individuals home or car. A seizure occurs when the government takes control of an individual or something in his or her possession. Items that are seized often are used as evidence when the individual is charged with a crime. It also imposes certain limitations on police investigating a crime and prevents the use of illegally obtained evidence at trial.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (11, 'Unenumerated rights and reserved powers','The Ninth Amendment (1791) declares that individuals have other fundamental rights, in addition to those stated in the Constitution.', 'The Ninth Amendment (1791) declares that individuals have other fundamental rights, in addition to those stated in the Constitution. During the Constitutional ratification debates Anti-Federalists argued that a Bill of Rights should be added. One of the arguments the Federalists gave against the addition of a Bill of Rights was that, because it was impossible to list every fundamental right, it would be dangerous to list just some of them, for fear of suggesting that the list was explicit and exhaustive, thus enlarging the power of the federal government by implication. The Anti-Federalists persisted in favor of a Bill of Rights, and consequently several state ratification conventions refused to ratify the Constitution without a more specific list of protections, so the First Congress added what became the Ninth Amendment as a compromise. Because the rights protected by the Ninth Amendment are not specified, they are referred to as "unenumerated". The Supreme Court has found that unenumerated rights include such important rights as the right to travel, the right to vote, the right to keep personal matters private and to make important decisions about ones health care or body.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (12, 'Governmental authority', 'The Eleventh Amendment (1795) specifically prohibits federal courts from hearing cases in which a state is sued by an individual from another state or another country, thus extending to the states sovereign immunity protection from certain types of legal liability.', 'The Eleventh Amendment (1795) specifically prohibits federal courts from hearing cases in which a state is sued by an individual from another state or another country, thus extending to the states sovereign immunity protection from certain types of legal liability. Article Three, Section 2, Clause 1 has been affected by this amendment, which also overturned the Supreme Courts decision in Chisholm v. Georgia.
The Sixteenth Amendment (1913) removed existing Constitutional constraints that limited the power of Congress to lay and collect direct taxes on income. Specifically, the apportionment constraints delineated in Article 1, Section 9, Clause 4 have been removed by this amendment, which also overturned an 1895 Supreme Court decision, in Pollock v. Farmers Loan & Trust Co., that declared a federal income taxe on rents, dividends, and interest unconstitutional. This amendment has become the basis for all subsequent federal income tax legislation and has greatly expanded the scope of federal taxing and spending in the years since.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (13, 'Safeguards of civil rights', 'The Thirteenth Amendment (1865) abolished slavery and involuntary servitude, except as punishment for a crime, and authorized Congress to enforce abolition.', 'The Thirteenth Amendment (1865) abolished slavery and involuntary servitude, except as punishment for a crime, and authorized Congress to enforce abolition. Though millions of slaves had been declared free by the 1863 Emancipation Proclamation, their post Civil War status was unclear, as was the status of other millions.[62] Congress intended the Thirteenth Amendment to be a proclamation of freedom for all slaves throughout the nation and to take the question of emancipation away from politics. This amendment rendered inoperative or moot several of the original parts of the constitution.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (14, 'Government processes and procedures','The Twelfth Amendment (1804) modifies the way the Electoral College chooses the President and Vice President.', 'The Twelfth Amendment (1804) modifies the way the Electoral College chooses the President and Vice President. It stipulates that each elector must cast a distinct vote for President and Vice President, instead of two votes for President. It also suggests that the President and Vice President should not be from the same state. The electoral process delineated by Article II, Section 1, Clause 3 has been superseded by that of this amendment, which also extends the eligibility requirements to become President to the Vice President.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (15, 'Scope and theory', 'Courts established by the Constitution can regulate government under the Constitution, the supreme law of the land.', 'Courts established by the Constitution can regulate government under the Constitution, the supreme law of the land. First, they have jurisdiction over actions by an officer of government and state law. Second, federal courts may rule on whether coordinate branches of national government conform to the Constitution. Until the twentieth century, the Supreme Court of the United States may have been the only high tribunal in the world to use a court for constitutional interpretation of fundamental law, others generally depending on their national legislature.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (16, 'Establishment','When John Marshall followed Oliver Ellsworth as Chief Justice of the Supreme Court in 1801...', 'When John Marshall followed Oliver Ellsworth as Chief Justice of the Supreme Court in 1801, the federal judiciary had been established by the Judiciary Act, but there were few cases, and less prestige. "The fate of judicial review was in the hands of the Supreme Court itself." Review of state legislation and appeals from state supreme courts was understood. But the Courts life, jurisdiction over state legislation was limited. The Marshall Courts landmark Barron v. Baltimore held that the Bill of Rights restricted only the federal government, and not the states.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (17, 'Self-restraint', 'The power of judicial review could not have been preserved long in a democracy unless it had been...', 'The Court controls almost all of its business by choosing what cases to consider, writs of certiorari. In this way, it can avoid opinions on embarrassing or difficult cases. The Supreme Court limits itself by defining for itself what is a "justiciable question." First, the Court is fairly consistent in refusing to make any "advisory opinions" in advance of actual cases.[m] Second, "friendly suits" between those of the same legal interest are not considered. Third, the Court requires a "personal interest", not one generally held, and a legally protected right must be immediately threatened by government action. Cases are not taken up if the litigant has no standing to sue. Simply having the money to sue and being injured by government action are not enough.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (18, 'Separation of powers','The Supreme Court balances several pressures to maintain its roles in national government...', 'The Supreme Court balances several pressures to maintain its roles in national government. It seeks to be a co-equal branch of government, but its decrees must be enforceable. The Court seeks to minimize situations where it asserts itself superior to either President or Congress, but federal officers must be held accountable. The Supreme Court assumes power to declare acts of Congress as unconstitutional but it self-limits its passing on constitutional questions.[93] But the Courts guidance on basic problems of life and governance in a democracy is most effective when American political life reinforce its rulings.[94]
Justice Brandeis summarized four general guidelines that the Supreme Court uses to avoid constitutional decisions relating to Congress:[n] The Court will not anticipate a question of constitutional law nor decide open questions unless a case decision requires it. If it does, a rule of constitutional law is formulated only as the precise facts in the case require. The Court will choose statutes or general law for the basis of its decision if it can without constitutional grounds. If it does, the Court will choose a constitutional construction of an Act of Congress, even if its constitutionality is seriously in doubt. ',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (19, 'Subsequent Courts','Supreme Courts under the leadership of subsequent Chief Justices have also used judicial review to interpret the...', 'Supreme Courts under the leadership of subsequent Chief Justices have also used judicial review to interpret the Constitution among individuals, states and federal branches. Notable contributions were made by the Chase Court, the Taft Court, the Warren Court, and the Rehnquist Court.
Further information: List of United States Supreme Court cases by the Chase Court
Salmon P. Chase was a Lincoln appointee, serving as Chief Justice from 1864 to 1873. His career encompassed service as a U.S. Senator and Governor of Ohio. He coined the slogan, "Free soil, free Labor, free men." One of Lincolns "team of rivals", he was appointed Secretary of Treasury during the Civil War, issuing "greenbacks". To appease radical Republicans, Lincoln appointed him to replace Chief Justice Roger B. Taney of Dred Scott case fame.
In one of his first official acts, Chase admitted John Rock, the first African-American to practice before the Supreme Court. The "Chase Court" is famous for Texas v. White, which asserted a permanent Union of indestructible states. Veazie Bank v. Fenno upheld the Civil War tax on state banknotes. Hepburn v. Griswold found parts of the Legal Tender Acts unconstitutional, though it was reversed under a late Supreme Court majority.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (20, 'Civic religion', 'There is a viewpoint that some Americans have come to see the documents of the Constitution..', 'There is a viewpoint that some Americans have come to see the documents of the Constitution, along with the Declaration of Independence and the Bill of Rights, as being a cornerstone of a type of civil religion. This is suggested by the prominent display of the Constitution, along with the Declaration of Independence and the Bill of Rights, in massive, bronze-framed, bulletproof, moisture-controlled glass containers vacuum-sealed in a rotunda by day and in multi-ton bomb-proof vaults by night at the National Archives Building.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');


/* Insert data in NEWS_AUTHOR table */
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (1, 16);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (2, 8);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (3, 1);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (4, 9);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (5, 13);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (6, 13);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (7, 4);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (8, 20);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (9, 14);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (10, 19);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (11, 1);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (12, 4);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (13, 13);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (14, 4);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (15, 20);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (16, 14);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (17, 19);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (18, 1);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (19, 4);
insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (20, 4);


/*Insert data in COMMENTS table */
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) values (1, 'I knew it! After all, I was right!', TIMESTAMP '2013-01-15 22:11:35', 8);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) values (2, 'A drop in the ocean.',TIMESTAMP '2013-01-15 22:11:35', 10);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (3, 'A load off ones mind',TIMESTAMP '2013-01-15 22:11:35', 7);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (4, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',12);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (5, 'As plain as the nose on your face', TIMESTAMP '2013-01-15 22:11:35',17);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (6, 'it is very interesting', TIMESTAMP '2013-01-15 22:11:35',11);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (7, 'I knew it!', TIMESTAMP '2013-01-15 22:11:35',1);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (8, 'good to know it', TIMESTAMP '2013-01-15 22:11:35',3);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (9, 'it is very interesting',TIMESTAMP '2013-01-15 22:11:35', 8);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (10, 'Carrot and stick', TIMESTAMP '2013-01-15 22:11:35',1);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (11, 'A political football', TIMESTAMP '2013-01-15 22:11:35',16);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (12, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',14);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (13, 'cool', TIMESTAMP '2013-01-15 22:11:35',2);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (14, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',3);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (15, 'ut mauris',TIMESTAMP '2013-01-15 22:11:35', 4);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (16, 'useful information!', TIMESTAMP '2013-01-15 22:11:35',15);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (17, 'cool',TIMESTAMP '2013-01-15 22:11:35', 1);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (18, 'useful information!', TIMESTAMP '2013-01-15 22:11:35',9);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) values (19, 'good', TIMESTAMP '2013-01-15 22:11:35',20);
insert into COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) values (20, 'it is very interesting', TIMESTAMP '2013-01-15 22:11:35',10);


/* Insert data in TAG table */
insert into TAG (TAG_ID, TAG_NAME) values (1, 'ft.com');
insert into TAG (TAG_ID, TAG_NAME) values (2, 'yandex.ru');
insert into TAG (TAG_ID, TAG_NAME) values (3, 'chron.com');
insert into TAG (TAG_ID, TAG_NAME) values (4, 'indiatimes.com');
insert into TAG (TAG_ID, TAG_NAME) values (5, 'dmoz.org');
insert into TAG (TAG_ID, TAG_NAME) values (6, 'un.org');
insert into TAG (TAG_ID, TAG_NAME) values (7, 'noaa.gov');
insert into TAG (TAG_ID, TAG_NAME) values (8, 'xinhuanet.com');
insert into TAG (TAG_ID, TAG_NAME) values (9, 'auda.org.au');
insert into TAG (TAG_ID, TAG_NAME) values (10, 'unesco.org');
insert into TAG (TAG_ID, TAG_NAME) values (11, 'cafepress.com');
insert into TAG (TAG_ID, TAG_NAME) values (12, 'imageshack.us');
insert into TAG (TAG_ID, TAG_NAME) values (13, 'desdev.cn');
insert into TAG (TAG_ID, TAG_NAME) values (14, 'blogs.com');
insert into TAG (TAG_ID, TAG_NAME) values (15, 'google.co.uk');
insert into TAG (TAG_ID, TAG_NAME) values (16, 'ask.com');
insert into TAG (TAG_ID, TAG_NAME) values (17, 'webeden.co.uk');
insert into TAG (TAG_ID, TAG_NAME) values (18, 'ca.gov');
insert into TAG (TAG_ID, TAG_NAME) values (19, 'dev.by');
insert into TAG (TAG_ID, TAG_NAME) values (20, 'habrahabr.ru');


/*  Insert data in NEWS_TAG table  */
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (1, 20);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (2, 3);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (3, 10);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (4, 9);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (5, 5);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (6, 20);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (7, 18);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (7, 4);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (8, 15);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (8, 1);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (9, 9);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (10, 6);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (10, 15);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (11, 6);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (11, 14);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (11, 3);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (12, 7);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (12, 4);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (13, 17);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (13, 16);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (14, 1);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (14, 9);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (15, 6);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (15, 15);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (16, 6);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (17, 14);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (17, 3);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (18, 7);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (19, 4);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (20, 17);
insert into NEWS_TAG ( NEWS_ID, TAG_ID) values (20, 16);

