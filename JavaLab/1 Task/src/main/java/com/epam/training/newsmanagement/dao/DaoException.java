/**
 *
 */
package com.epam.training.newsmanagement.dao;

/**
 * The class {@code DaoException} is a form of {@code Throwable} that indicates
 * conditions that a reasonable application might want to catch in DAO layer.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new exception with {@code null} as its detail message.
	 */
	public DaoException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail cause.
	 * 
	 * @param message
	 *            the detail message.
	 */
	public DaoException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param cause
	 *            the cause of exception
	 */
	public DaoException(Throwable cause) {
		super(cause);

	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message
	 *            the detail message.
	 * @param cause
	 *            the cause.
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * Constructs a new exception with the specified detail message, cause,
	 * suppression enabled or disabled, and writable stack trace enabled or
	 * disabled.
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
