package com.epam.training.newsmanagement.dao;

import java.util.List;
import com.epam.training.newsmanagement.entity.Comment;

/**
 * {@code ICommentDao} interface extends {@code IGenericDao} interface and
 * provides additional methods for {@code Comment} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanegement.dao.IGenericDao
 *
 */
public interface ICommentDao extends IGenericDao<Comment, Long> {

	/**
	 * Search list of comments in the DB according given news.
	 *
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return list of {@code Comment} instances.
	 * @throws DaoException
	 */
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException;

	/**
	 * Delete all {@code Comment} instances associated by given {@code News}
	 * instance.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @throws DaoException
	 */
	public void deleteCommentsByNews(Long newsId) throws DaoException;
}
