package com.epam.training.newsmanagement.dao;

import java.util.List;

/**
 * Generic {@code GenericDao} interface <br>
 * Contains set of standard C.R.U.D. operations for working with DB.
 *
 * @author Artsiom_Krauchanka
 * 
 */
public interface IGenericDao<Entity, PK> {

	/**
	 * Adds record to the DB.
	 *
	 * @param entity
	 *            record to be added.
	 * @return added entity ID.
	 * @throws DaoException
	 */
	public PK create(Entity entity) throws DaoException;

	/**
	 * Reads record from the DB.
	 *
	 * @param entityId
	 *            entity ID.
	 * @return Entity instance.
	 * @throws DaoException
	 */
	public Entity read(PK entityId) throws DaoException;

	/**
	 * Read all records from DB.
	 *
	 * @return list of record
	 * @throws DaoException
	 */
	public List<Entity> readAll() throws DaoException;

	/**
	 * Updates record in the DB.
	 *
	 * @param entity
	 *            record to be updated.
	 * @throws DaoException
	 */
	public void update(Entity entity) throws DaoException;

	/**
	 * Delete record from DB.
	 *
	 * @param entityId
	 *            entity ID to be deleted.
	 * 
	 * @throws DaoException
	 */
	public void delete(PK entityId) throws DaoException;
}
