package com.epam.training.newsmanagement.dao;

import java.util.List;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.SearchFilter;
import com.epam.training.newsmanagement.entity.Tag;

/**
 * {@code INewsDao} interface extends {@code IGenericDao} interface and provides
 * additional methods for {@code News} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanegement.dao.IGenericDao
 *
 */
public interface INewsDao extends IGenericDao<News, Long> {
	/**
	 * Read all records from DB in the range [beginIndex, endIndex] order by
	 * modification date and count of comments.
	 * 
	 * @param beginIndex
	 *            start index.
	 * @param endIndex
	 *            end index.
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> readAll(int beginIndex, int endIndex) throws DaoException;

	/**
	 * Search news according given tag.
	 *
	 * @param tagId
	 *            {@code Tag} instance ID.
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> searchNewsByTag(Long tagId) throws DaoException;

	/**
	 * Search news according given author.
	 *
	 * @param authorId
	 *            {@code Author} instance ID.
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException;

	/**
	 * Associate {@code News} and {@code Author} instance in the DB.
	 * 
	 * @param news
	 * @param authorId
	 * @throws DaoException
	 */
	public void associateNewsAuthor(Long newsId, Long authorId)
			throws DaoException;

	/**
	 * Associate {@code News} and {@code Tag} instance in the DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @param tagId
	 *            {@code Tag} instance ID.
	 *
	 * @throws DaoException
	 */
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException;

	/**
	 * Associate {@code News} instance and list of {@code Tag} instances in the
	 * DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @param tagList
	 *            list of {@code Tag} instances.
	 *
	 * @throws DaoException
	 */
	public void associateNewsTagList(Long newsId, List<Tag> tagList)
			throws DaoException;

	/**
	 * Search news in the DB according searchFilter criteria.
	 * 
	 * @param searchFilter
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> searchNewsByFilter(SearchFilter searchFilter)
			throws DaoException;

	/**
	 * Delete rows in the {@code NEWS_AUTHOR} table according news ID.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @throws DaoException
	 */
	public void deleteAuthorLinks(Long newsId) throws DaoException;

	/**
	 * Delete rows in the {@code NEWS_TAG} table according news ID.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @throws DaoException
	 */
	public void deleteTagLinks(Long newsId) throws DaoException;

}
