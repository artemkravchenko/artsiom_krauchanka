/**
 *
 */
package com.epam.training.newsmanagement.dao;

import java.util.List;
import com.epam.training.newsmanagement.entity.Tag;

/**
 * {@code ITagDao} interface extends {@code IGenericDao} interface and provides
 * additional methods for {@code Tag} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanegement.dao.IGenericDao
 *
 */
public interface ITagDao extends IGenericDao<Tag, Long> {

	/**
	 * Search {@code Tag} instances in the DB according given news.
	 *
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return list of {@code Tag} instances.
	 * @throws DaoException
	 */
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException;

	/**
	 * Delete records from intermediate table {@code NEWS_TAG} according tag ID.
	 * 
	 * @param tagId
	 *            {@code Tag} instance ID.
	 * @throws DaoException
	 */
	public void deleteLinks(Long tagId) throws DaoException;

}
