package com.epam.training.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.training.newsmanagement.dao.IAuthorDao;
import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.utils.DatabaseUtils;

/**
 * Public class {@code AuthorDaoImpl} is a part of Data Access layer.
 * Implementation of {@code IAuthorDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.training.newsmanagement.dao.IAuthorDao
 * @see com.epam.training.newsmanagement.entity.Author
 *
 */
@Repository
public class AuthorDaoImpl implements IAuthorDao {

	private static final String SQL_CREATE = "INSERT INTO AUTHOR (AUTHOR_ID, "
			+ "AUTHOR_NAME) VALUES (AUTHOR_ID_SEQ.NEXTVAL, ?)";
	private static final String SQL_READ = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_READALL = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR ORDER BY AUTHOR_ID";
	private static final String SQL_UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? "
			+ "WHERE AUTHOR_ID = ?";
	private static final String SQL_SET_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_LINKS = "DELETE FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.AUTHOR_ID = ?";
	private static final String SQL_DELETE = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_SEARCH_BY_NEWS = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED FROM AUTHOR JOIN NEWS_AUTHOR ON "
			+ "AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID = ?";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(Author author) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String authorIdColumn = "AUTHOR_ID";
		String generatedColumns[] = { authorIdColumn };

		Long authorId = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);

			preparedStatement.setString(1, author.getName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			}
			return authorId;
		} catch (SQLException e) {
			throw new DaoException("Creating operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	@Override
	public Author read(Long authorId) throws DaoException {
		Author author = null;
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);

			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong(1));
				author.setName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));
			}
			return author;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<Author> readAll() throws DaoException {
		List<Author> authorList = new ArrayList<>();
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READALL);

			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(1));
				author.setName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));

				authorList.add(author);
			}
			return authorList;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, author.getName());

			if (author.getExpired() != null) {
				preparedStatement.setTimestamp(2, new Timestamp(author
						.getExpired().getTime()));
			} else {
				preparedStatement.setNull(2, Types.NULL);
			}

			preparedStatement.setLong(3, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Update operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteLinks(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_LINKS);

			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public void setExpired(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED);

			preparedStatement.setTimestamp(1,
					new Timestamp(System.currentTimeMillis()));
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public Author searchAuthorByNews(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Author author = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS);

			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong(1));
				author.setName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));
			}

			return author;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

}
