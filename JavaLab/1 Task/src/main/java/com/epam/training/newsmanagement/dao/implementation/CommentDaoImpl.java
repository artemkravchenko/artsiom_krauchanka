package com.epam.training.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.training.newsmanagement.dao.ICommentDao;
import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.entity.Comment;
import com.epam.training.newsmanagement.utils.DatabaseUtils;

/**
 * Public class {@code CommentDaoImpl} is a part of Data Access layer.
 * Implementation of {@code ICommentDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.training.newsmanagement.dao.ICommentDao
 * @see com.epam.training.newsmanagement.entity.Comment
 */
@Repository
public class CommentDaoImpl implements ICommentDao {

	private static final String SQL_CREATE = "INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID) "
			+ "VALUES (COMMENT_ID_SEQ.NEXTVAL, ?,?,?)";
	private static final String SQL_READ = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_READALL = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS ORDER BY COMMENT_ID";
	private static final String SQL_UPDATE = "UPDATE COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ?, "
			+ "NEWS_ID = ?  WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE_BY_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private static final String SQL_SEARCH_BY_NEWS = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS WHERE NEWS_ID = ?";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(Comment comment) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String commentIdColumn = "COMMENT_ID";
		String generatedColumns[] = { commentIdColumn };
		Long commentId = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);

			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsId());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				commentId = resultSet.getLong(1);
			}
			return commentId;
		} catch (SQLException e) {
			throw new DaoException("Creating operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public Comment read(Long commentId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Comment comment = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				comment = new Comment();
				comment.setId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				comment.setNewsId(resultSet.getLong(4));
			}
			return comment;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<Comment> readAll() throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;

		List<Comment> commentList = new ArrayList<>();
		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READALL);

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				comment.setNewsId(resultSet.getLong(4));
				commentList.add(comment);
			}
			return commentList;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(Comment comment) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);

			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.setLong(4, comment.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Update operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);

			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteCommentsByNews(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		List<Comment> commentList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				comment.setNewsId(resultSet.getLong(4));
				commentList.add(comment);
			}
			return commentList;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

}
