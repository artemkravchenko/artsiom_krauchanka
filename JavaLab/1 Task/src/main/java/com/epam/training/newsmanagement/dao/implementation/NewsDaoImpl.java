package com.epam.training.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.dao.INewsDao;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.SearchFilter;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.utils.DatabaseUtils;

/**
 * Public class {@code NewsDaoImpl} is a part of Data Access layer.
 * Implementation of {@code INewsDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.training.newsmanagement.dao.INewsDao
 * @see com.epam.training.newsmanagement.entity.News
 */
@Repository
public class NewsDaoImpl implements INewsDao {

	private static final String SQL_CREATE = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.NEXTVAL,?,?,?,?,?)";

	private static final String SQL_READ = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
			+ "FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_UPDATE = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, "
			+ "FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ?  WHERE NEWS_ID = ?";
	private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_AUTHOR_LINK = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_TAG_LINK = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";

	private static final String SQL_SEARCH_BY_AUTHOR = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON "
			+ "NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ?";

	private static final String SQL_SEARCH_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON "
			+ "NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID = ?";
	private static final String SQL_READALL_SORTED_BY_COMMENTS_AND_DATE = "SELECT  NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT,"
			+ " NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS "
			+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, "
			+ "NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID), MODIFICATION_DATE DESC";

	private static final String SQL_READALL_OFFSET_ORDER_BY_COMMENTS_AND_DATE = "SELECT NS.NEWS_ID, NS.TITLE, NS.SHORT_TEXT, NS.FULL_TEXT, NS.CREATION_DATE, NS.MODIFICATION_DATE "
			+ "FROM (SELECT  NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE , rownum rn FROM NEWS) NS "
			+ "FULL OUTER JOIN COMMENTS ON NS.NEWS_ID = COMMENTS.NEWS_ID WHERE rn >= ? AND rn <= ? "
			+ "GROUP BY (NS.NEWS_ID, NS.SHORT_TEXT, NS.FULL_TEXT, NS.TITLE, NS.CREATION_DATE, NS.MODIFICATION_DATE) "
			+ "ORDER BY COUNT(COMMENTS.COMMENT_ID), NS.MODIFICATION_DATE DESC";

	private static final String SQL_BIND_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	private static final String SQL_BIND_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";

	/* filter query parts */
	private static final String FILTER_QUERY_BASE = "SELECT DISTINCT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS ";
	private static final String FILTER_JOIN_AUTHOR = " JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID";
	private static final String FILTER_JOIN_TAG = " JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID ";
	private static final String FILTER_JOIN_COMMENTS = " JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";
	private static final String FILTER_GROUP_BY = " GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, "
			+ "NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE)  ";
	private static final String FILTER_WHERE = " WHERE ";
	private static final String FILTER_AUTHOR_ID_CLAUSE = " AUTHOR_ID = ";
	private static final String FILTER_AND = " AND ";
	private static final String FILTER_OR = " OR ";
	private static final String FILTER_TAG_ID_CLAUSE = " TAG_ID = ";
	private static final String FILTER_ORDER_BY = " ORDER BY COUNT(COMMENTS.COMMENT_ID), MODIFICATION_DATE DESC";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(News news) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String newsIdColumn = "NEWS_ID";
		String generatedColumns[] = { newsIdColumn };
		Long newsId = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);

			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			}

			return newsId;
		} catch (SQLException e) {
			throw new DaoException("Creating operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public News read(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		News news = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
			}
			return news;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override public method {@code readAll} is used to read all {@code News}
	 * records from the DB. Return list of {@code News} instances sorted by
	 * comment count and modification date.
	 *
	 * @return list of {@code News} instances from the DB.
	 * @throws DaoException
	 */
	@Override
	public List<News> readAll() throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement
					.executeQuery(SQL_READALL_SORTED_BY_COMMENTS_AND_DATE);

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public List<News> readAll(int beginIndex, int endIndex) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_READALL_OFFSET_ORDER_BY_COMMENTS_AND_DATE);
			preparedStatement.setInt(1, beginIndex);
			preparedStatement.setInt(2, endIndex);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public void update(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);

			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			preparedStatement.setLong(6, news.getId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Update operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);

			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<News> searchNewsByTag(Long tagId) throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_TAG);
			preparedStatement.setLong(1, tagId);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
				newsList.add(news);
			}

			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_AUTHOR);

			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public void associateNewsAuthor(Long newsId, Long authorId)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_BIND_NEWS_AUTHOR);

			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
			// throw new RuntimeException("test Exception");
		} catch (SQLException e) {
			throw new DaoException("Binding operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);

		}
	}

	@Override
	public void associateNewsTagList(Long newsId, List<Tag> tagList)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(SQL_BIND_NEWS_TAG);

			for (Tag tag : tagList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tag.getId());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			throw new DaoException("Binding operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_BIND_NEWS_TAG);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Binding operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public void deleteAuthorLinks(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_AUTHOR_LINK);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteTagLinks(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_TAG_LINK);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<News> searchNewsByFilter(SearchFilter searchFilter)
			throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		List<News> newsList = new ArrayList<>();
		String searchFilterQuery = buildSearchQuery(searchFilter);

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(searchFilterQuery);

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	private String buildSearchQuery(SearchFilter searchFilter) {
		StringBuilder queryBuilder = new StringBuilder(FILTER_QUERY_BASE);
		boolean checkTagList = false;
		boolean checkAuthor = false;

		if (searchFilter.getAuthorId() != null) {
			queryBuilder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (searchFilter.getTagIdList() != null
				&& !searchFilter.getTagIdList().isEmpty()) {
			queryBuilder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}
		queryBuilder.append(FILTER_JOIN_COMMENTS);
		queryBuilder.append(FILTER_WHERE);

		if (checkAuthor) {
			queryBuilder.append(FILTER_AUTHOR_ID_CLAUSE);
			queryBuilder.append(searchFilter.getAuthorId());
		}

		if (checkTagList) {
			queryBuilder.append(FILTER_AND);
			for (Long tagId : searchFilter.getTagIdList()) {
				queryBuilder.append(FILTER_TAG_ID_CLAUSE);
				queryBuilder.append(tagId);
				queryBuilder.append(FILTER_OR);
			}
			queryBuilder.delete(queryBuilder.length() - 3,
					queryBuilder.length());
		}
		queryBuilder.append(FILTER_GROUP_BY);
		queryBuilder.append(FILTER_ORDER_BY);
		return queryBuilder.toString();
	}
}
