package com.epam.training.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.dao.ITagDao;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.utils.DatabaseUtils;

/**
 * Public class {@code TagDaoImpl} is a part of Data Access layer.
 * Implementation of {@code ITagDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.training.newsmanagement.dao.ITagDao
 * @see com.epam.training.newsmanagement.entity.Tag
 */
@Repository
public class TagDaoImpl implements ITagDao {

	private static final String SQL_CREATE = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES "
			+ " (TAG_ID_SEQ.NEXTVAL, ?)";
	private static final String SQL_READ = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	private static final String SQL_READALL = "SELECT TAG_ID, TAG_NAME FROM TAG ORDER BY TAG_ID";
	private static final String SQL_UPDATE = "UPDATE TAG SET TAG_NAME = ?  WHERE TAG_ID = ?";
	private static final String SQL_DELETE = "DELETE FROM TAG WHERE TAG_ID = ?";
	private static final String SQL_DELETE_LINKS = "DELETE FROM NEWS_TAG WHERE NEWS_TAG.TAG_ID = ?";
	private static final String SQL_SEARCH_BY_NEWS = "SELECT DISTINCT TAG.TAG_ID, TAG.TAG_NAME FROM TAG JOIN NEWS_TAG ON "
			+ "TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_TAG.NEWS_ID = ?";
	
	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(Tag tag) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String tagIdColumn = "TAG_ID";
		String generatedColumns[] = { tagIdColumn };
		Long tagId = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				tagId = resultSet.getLong(1);
			}
			return tagId;
		} catch (SQLException e) {
			throw new DaoException("Creating operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public Tag read(Long tagId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Tag tag = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = new Tag();
				tag.setId(resultSet.getLong(1));
				tag.setName(resultSet.getString(2));
			}
			return tag;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<Tag> readAll() throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;
		List<Tag> tagList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READALL);

			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(1));
				tag.setName(resultSet.getString(2));
				tagList.add(tag);
			}
			return tagList;
		} catch (SQLException e) {
			throw new DaoException("Reading operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Update operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteLinks(Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_LINKS);

			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Delete operation error.", e);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		List<Tag> tagList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(1));
				tag.setName(resultSet.getString(2));
				tagList.add(tag);
			}
			return tagList;
		} catch (SQLException e) {
			throw new DaoException("Searching operation error.", e);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

}
