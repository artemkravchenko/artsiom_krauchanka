package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Public class {@code Author} is DB entity class which describes Author table
 * in the DB.
 *
 * @author Artsiom_Krauchanka
 */
public class Author implements Serializable {

	private static final long serialVersionUID = 6210056743110213516L;
	private Long id;
	private String name;
	private Date expired;
	
	public Author() {

	}

	public Author(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.id);
		hash = 53 * hash + Objects.hashCode(this.name);
		hash = 53 * hash + Objects.hashCode(this.expired);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Author other = (Author) obj;

		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.expired, other.expired)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired
				+ "]";
	}

	

}
