package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Public class {@code Comment} is DB entity class which describes Comment table
 * in the DB.
 *
 * @author Artsiom_Krauchanka
 */
public class Comment implements Serializable {

	private static final long serialVersionUID = 473699854147992329L;
	private Long id;
	private String text;
	private Date creationDate;
	private Long newsId;

	public Comment() {

	}

	public Comment(Long id, String commentText, Date creationDate,
			Long newsId) {
		this.id = id;
		this.text = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	public String getCommentText() {
		return text;
	}

	public void setCommentText(String commentText) {
		this.text = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + Objects.hashCode(this.id);
		hash = 97 * hash + Objects.hashCode(this.text);
		hash = 97 * hash + Objects.hashCode(this.creationDate);
		hash = 97 * hash + Objects.hashCode(this.newsId);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Comment other = (Comment) obj;

		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		if (!Objects.equals(this.text, other.text)) {
			return false;
		}
		if (!Objects.equals(this.creationDate, other.creationDate)) {
			return false;
		}
		if (!Objects.equals(this.newsId, other.newsId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentText=" + text
				+ ", creationDate=" + creationDate + ", newsId=" + newsId + "]";
	}

}
