package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Public class {@code News} is DB entity class which describes News table in
 * the DB.
 *
 * @author Artsiom_Krauchanka
 */
public class News implements Serializable {

	private static final long serialVersionUID = 8648344003364039122L;
	private Long id;
	private String title;
	private String shortText;
	private String fullText;
	private Date creationDate;
	private Date modificationDate;

	public News() {

	}

	public News(Long id, String title, String shortText, String fullText,
			Date creationDate, Date modificationDate) {
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + Objects.hashCode(this.id);
		hash = 17 * hash + Objects.hashCode(this.title);
		hash = 17 * hash + Objects.hashCode(this.shortText);
		hash = 17 * hash + Objects.hashCode(this.fullText);
		hash = 17 * hash + Objects.hashCode(this.creationDate);
		hash = 17 * hash + Objects.hashCode(this.modificationDate);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final News other = (News) obj;

		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		if (!Objects.equals(this.title, other.title)) {
			return false;
		}
		if (!Objects.equals(this.shortText, other.shortText)) {
			return false;
		}
		if (!Objects.equals(this.fullText, other.fullText)) {
			return false;
		}
		if (!Objects.equals(this.creationDate, other.creationDate)) {
			return false;
		}
		if (!Objects.equals(this.modificationDate, other.modificationDate)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate + "]";
	}

}
