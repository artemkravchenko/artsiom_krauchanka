package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Artsiom_Krauchanka
 *
 */
public class NewsVO implements Serializable {

	private static final long serialVersionUID = -318572220353175082L;
	private News news;
	private Author author;
	private List<Comment> commentList;
	private List<Tag> tagList;

	public NewsVO() {

	}

	public NewsVO(News news, Author author, List<Comment> commentList,
			List<Tag> tagList) {
		this.news = news;
		this.author = author;
		this.commentList = commentList;
		this.tagList = tagList;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 97 * hash + Objects.hashCode(this.news);
		hash = 97 * hash + Objects.hashCode(this.author);
		hash = 97 * hash + Objects.hashCode(this.commentList);
		hash = 97 * hash + Objects.hashCode(this.tagList);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final NewsVO other = (NewsVO) obj;

		if (!Objects.equals(this.news, other.news)) {
			return false;
		}
		if (!Objects.equals(this.author, other.author)) {
			return false;
		}
		if (!Objects.equals(this.commentList, other.commentList)) {
			return false;
		}
		if (!Objects.equals(this.tagList, other.tagList)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "NewsPost [news=" + news + ", author=" + author
				+ ", commentList=" + commentList + ", tagList=" + tagList + "]";
	}

}
