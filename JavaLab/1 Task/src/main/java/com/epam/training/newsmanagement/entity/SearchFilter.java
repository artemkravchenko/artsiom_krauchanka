package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Search object. {@code SeachFilter} object may contains: author entity ID and
 * list of tags ID. It will be using for constructing SQL queries in DAO layer.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class SearchFilter implements Serializable {

	private static final long serialVersionUID = 5418831118464750009L;
	private Long authorId;
	private List<Long> tagIdList;

	public SearchFilter() {

	}

	public SearchFilter(Long authorId, List<Long> tagIdList) {
		this.authorId = authorId;
		this.tagIdList = tagIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.authorId);
		hash = 53 * hash + Objects.hashCode(this.tagIdList);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SearchFilter other = (SearchFilter) obj;

		if (!Objects.equals(this.authorId, other.authorId)) {
			return false;
		}
		if (!Objects.equals(this.tagIdList, other.tagIdList)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SearchFilter [authorId=" + authorId + ", tagIdList="
				+ tagIdList + "]";
	}

}
