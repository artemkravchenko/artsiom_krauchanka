package com.epam.training.newsmanagement.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Public class {@code Tag} is DB entity class which describes Tag table in the
 * DB.
 *
 * @author Artsiom_Krauchanka
 */
public class Tag implements Serializable {

	private static final long serialVersionUID = 3645439362730218722L;
	private Long id;
	private String name;

	public Tag() {

	}

	public Tag(Long id, String tagName) {
		this.id = id;
		this.name = tagName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.id);
		hash = 59 * hash + Objects.hashCode(this.name);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Tag other = (Tag) obj;

		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", tagName=" + name + "]";
	}

}
