/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service;

import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;

/**
 * Interface {@code IAuthorService} describes the behavior of a particular
 * service layer which working with {@code Author} instance using Data Access
 * layer implementations of interface {@code IAuthorDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.training.newsmanagement.service.IGenericService
 * @see com.epam.training.newsmanagement.dao.IAuthorDao
 * @see com.epam.training.newsmanagement.entity.Author
 */
public interface IAuthorService extends IGenericService<Author, Long> {

	/**
	 * Search Author in the DB according given news using data access layer.
	 *
	 * @param news
	 * @return {@code Author} instance.
	 * @throws ServiceException
	 */
	public Author searchAuthorByNews(News news) throws ServiceException;

	/**
	 * Update expired field in the {@code AUTHOR} table.
	 * 
	 * @param authorId
	 *            {@code Author} instance ID.
	 * @throws ServiceException
	 */
	public void setExpired(Long authorId) throws ServiceException;
}
