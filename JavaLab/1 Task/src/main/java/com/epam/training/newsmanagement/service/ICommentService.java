/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service;

import com.epam.training.newsmanagement.entity.Comment;
import com.epam.training.newsmanagement.entity.News;

import java.util.List;

/**
 * Interface {@code ICommentService} describes the behavior of a particular
 * service layer which working with {@code Comment} instance using Data Access
 * layer implementations of interface {@code ICommentDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.training.newsmanagement.service.IGenericService
 * @see com.epam.training.newsmanagement.dao.ICommentDao
 * @see com.epam.training.newsmanagement.entity.Comment
 */
public interface ICommentService extends IGenericService<Comment, Long> {

    /**
     * Search list of comments in the DB using Data access layer.
     *
     * @param news
     * @return list of comments according given news.
     * @throws ServiceException
     */
    public List<Comment> searchCommentsByNews(News news) throws ServiceException;
    
    /**
	 * Delete all {@code Comment} instances associated by given {@code News}
	 * instance.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteCommentsByNews(Long newsId) throws ServiceException;
}
