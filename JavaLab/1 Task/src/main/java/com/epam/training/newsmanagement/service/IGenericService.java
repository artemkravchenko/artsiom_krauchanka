/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic {@code IGenericService} interface. Contains set of standard C.R.U.D.
 * operations for working with data access layer.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.dao.IGenericDao
 */
public interface IGenericService<Entity, PK> {

	public static final Logger logger = LoggerFactory
			.getLogger(IGenericService.class);

	/**
	 * Adds record to the DB using {@code create()} method from
	 * {@code IGenericDao} interface.
	 *
	 * @param entity
	 *            record to be added.
	 * @return added entity ID.
	 * @throws ServiceException
	 */
	public PK create(Entity entity) throws ServiceException;

	/**
	 * Read record from the DB using {@code read()} method from
	 * {@code IGenericDao} interface.
	 *
	 * @param entityId
	 *            entity ID.
	 * @return Entity record.
	 * @throws ServiceException
	 */
	public Entity read(PK entityId) throws ServiceException;

	/**
	 * Read all records from DB using {@code readAll()} method from
	 * {@code IGenericDao} interface.
	 *
	 * @return read list of entity instances.
	 * @throws ServiceException
	 */
	public List<Entity> readAll() throws ServiceException;

	/**
	 * Updates record in the DB using {@code update()} method from
	 * {@code IGenericDao} interface.
	 *
	 * @param entity
	 *            record to be updated.
	 * 
	 * @throws ServiceException
	 */
	public void update(Entity entity) throws ServiceException;

	/**
	 * Delete record from DB using {@code delete()} method from
	 * {@code IGenericDao} interface.
	 *
	 * @param entityId
	 *            record ID to be deleted.
	 * 
	 * @throws ServiceException
	 */
	public void delete(PK entityId) throws ServiceException;
}
