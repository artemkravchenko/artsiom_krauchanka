package com.epam.training.newsmanagement.service;

import com.epam.training.newsmanagement.entity.NewsVO;

/**
 * General interface {@code INewsManagerService} describes transactional
 * operations for {@code NewsVO} instance working.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public interface INewsManagerService {
	/**
	 * Save {@code NewsVO} instance to the DB.
	 * 
	 * @param newsPost
	 * @throws ServiceException
	 */
	public void saveNewsPost(NewsVO newsPost) throws ServiceException;

	/**
	 * Delete {@code NewsVO} instance from the DB.
	 * 
	 * @param newsPost
	 * @throws ServiceException
	 */
	public void deleteNewsPost(NewsVO newsPost) throws ServiceException;
}
