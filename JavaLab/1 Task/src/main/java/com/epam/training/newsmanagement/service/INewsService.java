/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service;

import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;

import java.util.List;

/**
 * Interface {@code INewsService} describes the behavior of a particular service
 * layer which working with {@code News} instance using Data Access layer
 * implementations of interface {@code INewsDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.training.newsmanagement.service.IGenericService
 * @see com.epam.training.newsmanagement.dao.INewsDao
 * @see com.epam.training.newsmanagement.entity.News
 */
public interface INewsService extends IGenericService<News, Long> {
	/**
	 * Read all records from DB in the range [beginIndex, endIndex] order by
	 * modification date and count of comments.
	 * 
	 * @param beginIndex
	 *            start index.
	 * @param endIndex
	 *            end index.
	 * @return list of {@code News} instances.
	 * @throws ServiceException
	 */
	public List<News> readAll(int beginIndex, int endIndex)
			throws ServiceException;

	/**
	 * Search news according given tag.
	 *
	 * @param tag
	 * @return list of @{code News} instances.
	 * @throws ServiceException
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException;

	/**
	 * Search news according given author.
	 *
	 * @param author
	 * @return list of @{code News} instances.
	 * @throws ServiceException
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException;

	/**
	 * Bind {@code News} and {@code Author} instance in the DB.
	 * 
	 * @param news
	 * @param author
	 * @return
	 * @throws DaoException
	 */
	public void associateNewsAuthor(News news, Author author)
			throws ServiceException;

	/**
	 * Bind {@code News} instance and list of {@code Tag} instances in the DB.
	 * 
	 * @param news
	 * @param tag
	 * @return
	 * @throws DaoException
	 */
	public void associateNewsTagList(News news, List<Tag> tagList)
			throws ServiceException;

	/**
	 * Delete records in the {@code NEWS_AUTHOR} table according news ID.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteAuthorLinks(Long newsId) throws ServiceException;

	/**
	 * Delete records in the {@code NEWS_TAG} table according news ID.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteTagLinks(Long newsId) throws ServiceException;

}
