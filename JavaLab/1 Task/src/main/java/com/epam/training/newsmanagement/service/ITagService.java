/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service;

import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;
import java.util.List;

/**
 * Interface {@code ITagService} describes the behavior of a particular service
 * layer which working with {@code Tag} instance using Data Access layer
 * implementations of interface {@code ITagDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.training.newsmanagement.service.IGenericService
 * @see com.epam.training.newsmanagement.dao.ITagDao
 * @see com.epam.training.newsmanagement.entity.Tag
 */
public interface ITagService extends IGenericService<Tag, Long> {

	/**
	 * Search tags according given news.
	 *
	 * @param news
	 * @return list of {@Tag} instances.
	 * @throws ServiceException
	 */
	public List<Tag> searchTagsByNews(News news) throws ServiceException;
}
