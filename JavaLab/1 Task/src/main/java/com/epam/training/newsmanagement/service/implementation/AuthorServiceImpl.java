/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service.implementation;

import com.epam.training.newsmanagement.dao.IAuthorDao;
import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.service.IAuthorService;
import com.epam.training.newsmanagement.service.ServiceException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code AuthorServiceImpl} class is an element of Service layer. Its works
 * with {@code Author} database instance. {@code AuthorService} interface
 * implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.service.IAuthorService
 * @see com.epam.training.newsmanagement.entity.Author
 */
@Service
public class AuthorServiceImpl implements IAuthorService {

	@Autowired
	private IAuthorDao authorDao;

	@Override
	public Long create(Author author) throws ServiceException {
		if (author != null) {
			try {
				Long authorId = authorDao.create(author);
				logger.info("Author instance created. ID: " + authorId);
				return authorId;

			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public Author read(Long entityId) throws ServiceException {
		if (entityId > 0) {
			try {
				Author author = authorDao.read(entityId);
				logger.info("Author record read: " + author);
				return author;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<Author> readAll() throws ServiceException {
		try {
			List<Author> authorList = authorDao.readAll();
			logger.info("List of Author records read: " + authorList);
			return authorList;
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		if (author != null) {
			try {
				authorDao.update(author);
				logger.info("Author instance updated. ID: " + author.getId());
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public void setExpired(Long authorId) throws ServiceException {
		if (authorId > 0) {
			try {
				authorDao.setExpired(authorId);
				logger.info("Author instance updated. ID: " + authorId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Transactional
	@Override
	public void delete(Long authorId) throws ServiceException {
		if (authorId > 0) {
			try {
				authorDao.deleteLinks(authorId);
				authorDao.delete(authorId);
				logger.info("Author instance deleted. ID: " + authorId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public Author searchAuthorByNews(News news) throws ServiceException {
		if (news != null) {
			try {
				Author author = authorDao.searchAuthorByNews(news.getId());
				logger.info("Author instance founded: " + author);
				return author;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

}
