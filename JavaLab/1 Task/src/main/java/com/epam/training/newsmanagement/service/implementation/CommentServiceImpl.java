/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service.implementation;

import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.dao.ICommentDao;
import com.epam.training.newsmanagement.entity.Comment;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.service.ICommentService;
import com.epam.training.newsmanagement.service.ServiceException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * {@code CommentServiceImpl} class is an element of Service layer. Its works
 * with {@code Comment} database instance. {@code CommentService} interface
 * implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.service.ICommentService
 * @see com.epam.training.newsmanagement.entity.Comment
 */
@Service
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private ICommentDao commentDao;

	@Override
	public Long create(Comment comment) throws ServiceException {
		if (comment != null) {
			try {
				Long commentId = commentDao.create(comment);
				logger.info("Comment instance created. ID: " + commentId);
				return commentId;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public Comment read(Long commentId) throws ServiceException {
		if (commentId > 0) {
			try {
				Comment comment = commentDao.read(commentId);
				logger.info("Comment record read: " + comment);
				return comment;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<Comment> readAll() throws ServiceException {
		try {
			List<Comment> commentList = commentDao.readAll();
			logger.info("List of Comment records read: " + commentList);
			return commentList;
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Comment comment) throws ServiceException {
		if (comment != null) {
			try {
				commentDao.update(comment);
				logger.info("Comment instance updated. ID: " + comment.getId());
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public void delete(Long commentId) throws ServiceException {
		if (commentId > 0) {
			try {
				commentDao.delete(commentId);
				logger.info("Comment instance updated. ID: " + commentId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public void deleteCommentsByNews(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				commentDao.deleteCommentsByNews(newsId);
				logger.info("Comment instance updated. ID: " + newsId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<Comment> searchCommentsByNews(News news)
			throws ServiceException {
		if (news != null) {
			try {
				List<Comment> commentList = commentDao
						.searchCommentsByNews(news.getId());
				logger.info("List of comments founded: " + commentList);
				return commentList;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

}
