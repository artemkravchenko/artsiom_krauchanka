package com.epam.training.newsmanagement.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.training.newsmanagement.entity.NewsVO;
import com.epam.training.newsmanagement.service.ICommentService;
import com.epam.training.newsmanagement.service.INewsManagerService;
import com.epam.training.newsmanagement.service.INewsService;
import com.epam.training.newsmanagement.service.ServiceException;

/**
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Service
public class NewsManagerServiceImpl implements INewsManagerService {

	@Autowired
	private INewsService newsService;

	@Autowired
	private ICommentService commentService;

	@Transactional
	@Override
	public void saveNewsPost(NewsVO newsPost) throws ServiceException {
		Long newsId = newsService.create(newsPost.getNews());
		newsPost.getNews().setId(newsId);
		
		newsService.associateNewsAuthor(newsPost.getNews(),
				newsPost.getAuthor());
		newsService.associateNewsTagList(newsPost.getNews(),
				newsPost.getTagList());
	}

	@Transactional
	@Override
	public void deleteNewsPost(NewsVO newsPost) throws ServiceException {
		Long newsId = newsPost.getNews().getId();
		
		commentService.deleteCommentsByNews(newsId);
		newsService.deleteAuthorLinks(newsId);
		newsService.deleteTagLinks(newsId);
		newsService.delete(newsId);
	}
}
