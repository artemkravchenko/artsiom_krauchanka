/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service.implementation;

import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.dao.INewsDao;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.service.INewsService;
import com.epam.training.newsmanagement.service.ServiceException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code NewsServiceImpl} class is an element of Service layer. Its works with
 * {@code News} database instance. {@code NewsService} interface implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.service.INewsService
 * @see com.epam.training.newsmanagement.entity.News
 */
@Service
public class NewsServiceImpl implements INewsService {

	@Autowired
	private INewsDao newsDao;

	@Override
	public Long create(News news) throws ServiceException {
		if (news != null) {
			try {
				Long newsId = newsDao.create(news);
				logger.info("News instance created. ID: " + newsId);
				return newsId;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public News read(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				News news = newsDao.read(newsId);
				logger.info("News record read: " + news);
				return news;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	/**
	 * Override public method {@code readAll} is used to read all {@code News}
	 * records sorted by comment count from the DB using {@code NewsDao}
	 * instance.
	 *
	 * @return list of {@code News} instances from the DB.
	 * @throws ServiceException
	 */
	@Override
	public List<News> readAll() throws ServiceException {
		try {
			List<News> newsList = newsDao.readAll();
			logger.info("List of News records read: " + newsList);
			return newsList;
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> readAll(int beginIndex, int endIndex)
			throws ServiceException {
		if (beginIndex < 0 || endIndex < 0 || beginIndex >= endIndex) {
			throw new ServiceException("Invalid search range.");
		} else {

			try {
				List<News> newsList = newsDao.readAll(beginIndex, endIndex);
				logger.info("List of News records read: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		}
	}

	@Override
	public void update(News news) throws ServiceException {
		if (news != null) {
			try {
				newsDao.update(news);
				logger.info("news instance updated. ID: " + news.getId());
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public void delete(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDao.delete(newsId);
				logger.info("news instance deleted. ID: " + newsId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				List<News> newsList = newsDao.searchNewsByTag(tag.getId());
				logger.info("List of News records founded: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

	@Override
	public List<News> searchNewsByAuthor(Author author) throws ServiceException {
		if (author != null) {
			try {
				List<News> newsList = newsDao
						.searchNewsByAuthor(author.getId());
				logger.info("List of News records founded: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

	@Override
	public void associateNewsAuthor(News news, Author author)
			throws ServiceException {
		if (news == null || author == null) {
			throw new ServiceException("Empty parameters error.");
		}
		try {
			newsDao.associateNewsAuthor(news.getId(), author.getId());
			logger.info("news and author instances associated.");
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	@Transactional
	public void associateNewsTagList(News news, List<Tag> tagList)
			throws ServiceException {
		if (news == null || tagList == null) {
			throw new ServiceException("Empty parameters error.");
		}
		try {
			newsDao.associateNewsTagList(news.getId(), tagList);
			logger.info("news and author instances associated.");
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteAuthorLinks(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDao.deleteAuthorLinks(newsId);
				logger.info("associated News and Author records deleted.");
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public void deleteTagLinks(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDao.deleteTagLinks(newsId);
				logger.info("associated Tag and Author records deleted.");
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

}
