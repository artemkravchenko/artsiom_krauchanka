/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.training.newsmanagement.service.implementation;

import com.epam.training.newsmanagement.dao.DaoException;
import com.epam.training.newsmanagement.dao.ITagDao;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.service.ServiceException;
import com.epam.training.newsmanagement.service.ITagService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code TagServiceImpl} class is an element of Service layer. Its works with
 * {@code Tag} database instance. {@code TagService} interface implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.service.ITagService
 * @see com.epam.training.newsmanagement.entity.Tag
 */
@Service
public class TagServiceImpl implements ITagService {

	@Autowired
	private ITagDao tagDao;

	@Override
	public Long create(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				Long tagId = tagDao.create(tag);
				logger.info("Tag instance created. ID: " + tagId);
				return tagId;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Override
	public Tag read(Long tagId) throws ServiceException {
		if (tagId > 0) {
			try {
				Tag tag = tagDao.read(tagId);
				logger.info("Tag record read: " + tag);
				return tag;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<Tag> readAll() throws ServiceException {
		try {
			List<Tag> tagList = tagDao.readAll();
			logger.info("list of Tag records read: " + tagList);
			return tagList;
		} catch (DaoException ex) {
			logger.error(ex.getMessage());
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				tagDao.update(tag);
				logger.info("Tag instance updated. ID: " + tag.getId());
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Entity instance is empty");
		}
	}

	@Transactional
	@Override
	public void delete(Long tagId) throws ServiceException {
		if (tagId > 0) {
			try {
				tagDao.deleteLinks(tagId);
				tagDao.delete(tagId);
				logger.info("Tag record deleted. ID: " + tagId);
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid entity ID.");
		}
	}

	@Override
	public List<Tag> searchTagsByNews(News news) throws ServiceException {
		if (news != null) {
			try {
				List<Tag> tagList = tagDao.searchTagsByNews(news.getId());
				logger.info("list of Tag records founded: " + tagList);
				return tagList;
			} catch (DaoException ex) {
				logger.error(ex.getMessage());
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

}
