package com.epam.training.newsmanagement.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * {@code DatabaseUtils} class provides methods for connection pool working.
 * 
 * @author Artsiom_Krauchanka
 * @see org.springframework.jdbc.datasource.DataSourceUtils;
 */
public class DatabaseUtils {
	private DataSource dataSource;
	private Logger logger = LoggerFactory.getLogger(DatabaseUtils.class);

	public DatabaseUtils(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * provide {@code Connection} instance from pool using
	 * {@code DataSourceUtils} class.
	 * 
	 * @return {@code Connection} instance from pool.
	 * @throws SQLException
	 */
	public synchronized Connection getConnection() throws SQLException {
		return DataSourceUtils.getConnection(dataSource);
	}

	/**
	 * Execute closing resources operations.
	 * 
	 * @param resultSet
	 * @param statement
	 * @param connection
	 */
	public void closeResources(ResultSet resultSet, Statement statement,
			Connection connection) {
		try {
			if (resultSet != null && !resultSet.isClosed()) {
				resultSet.close();
			}

			if (statement != null && !statement.isClosed()) {
				statement.close();
			}

			if (connection != null && !connection.isClosed()) {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
		}
	}

	/**
	 * Execute closing resources operations.
	 * 
	 * @param statement
	 * @param connection
	 */
	public void closeResources(Statement statement, Connection connection) {
		try {
			if (statement != null && !statement.isClosed()) {
				statement.close();
			}

			if (connection != null && !connection.isClosed()) {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
		}
	}

}
