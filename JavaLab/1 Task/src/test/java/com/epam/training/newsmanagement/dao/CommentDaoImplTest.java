package com.epam.training.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.training.newsmanagement.dao.ICommentDao;
import com.epam.training.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * {@code CommentDaoImplTest} class content list of DB Unit tests, which check
 * right work of Data Access layer ( {@code ICommentDao} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.dao.ICommentDao
 * @see com.epam.training.newsmanagement.entity.Comment
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/spring.test.config.xml" })
@DatabaseSetup(value = "/test.dataset.xml")
public class CommentDaoImplTest {

	@Autowired
	private ICommentDao commentDao;

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long commentId = null;
		String expectedCommentText = "comment text";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2015-05-05 13:14:15");
		Long expectedNewsId = 2L;

		Comment comment = new Comment();
		comment.setCommentText(expectedCommentText);
		comment.setCreationDate(expectedCreationDate);
		comment.setNewsId(expectedNewsId);

		commentId = commentDao.create(comment);
		comment = commentDao.read(commentId);

		assertEquals(expectedCommentText, comment.getCommentText());
		assertEquals(expectedCreationDate, comment.getCreationDate());
		assertEquals(expectedNewsId, comment.getNewsId());
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long commentId = 1L;
		String expectedCommentText = "COMMENT1";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2015-08-14 12:12:12");
		Long expectedNewsId = 1L;

		Comment actualComment = commentDao.read(commentId);

		assertEquals(expectedCommentText, actualComment.getCommentText());
		assertEquals(expectedCreationDate, actualComment.getCreationDate());
		assertEquals(expectedNewsId, actualComment.getNewsId());
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		int expectedSize = 3;
		String expectedCommentText = "COMMENT1";

		List<Comment> commentList = commentDao.readAll();
		assertEquals(expectedSize, commentList.size());
		assertEquals(expectedCommentText, commentList.get(0).getCommentText());
	}

	/**
	 * Data Base Unit test, that verify work upadte() method in the
	 * {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long commentId = 1L;
		String expectedCommentText = "NEW TEXT COMMENT1";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2015-01-11 11:11:11");
		Long expectedNewsId = 1L;

		Comment comment = new Comment();
		comment.setId(commentId);
		comment.setCommentText(expectedCommentText);
		comment.setCreationDate(expectedCreationDate);
		comment.setNewsId(expectedNewsId);

		commentDao.update(comment);
		Comment actualComment = commentDao.read(commentId);

		assertEquals(expectedCommentText, actualComment.getCommentText());
		assertEquals(expectedCreationDate, actualComment.getCreationDate());
		assertEquals(expectedNewsId, actualComment.getNewsId());
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long commentId = 1L;
		commentDao.delete(commentId);
		assertNull(commentDao.read(commentId));
	}

	/**
	 * Data Base Unit test, that verify work deleteCommentsByNews() method in
	 * the {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteCommentsByNews() throws Exception {
		Long newsId = 1l;
		Long commentId = 1l;
		commentDao.deleteCommentsByNews(newsId);
		assertNull(commentDao.read(commentId));
	}

	/**
	 * Data Base Unit test, that verify work searchCommentsByNews() method in
	 * the {@code CommentDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.CommentDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchCommentsByNews() throws Exception {
		Long expectedNewsId = 1L;
		String expectedCommentText = "COMMENT1";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2015-08-14 12:12:12");

		List<Comment> commentList = commentDao
				.searchCommentsByNews(expectedNewsId);
		Comment actualComment = commentList.get(0);

		assertEquals(expectedCommentText, actualComment.getCommentText());
		assertEquals(expectedCreationDate, actualComment.getCreationDate());
		assertEquals(expectedNewsId, actualComment.getNewsId());
	}
}
