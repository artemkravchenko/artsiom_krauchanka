package com.epam.training.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.training.newsmanagement.dao.INewsDao;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.NewsVO;
import com.epam.training.newsmanagement.entity.SearchFilter;
import com.epam.training.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * {@code NewsDaoImplTest} class content list of DB Unit tests, which check
 * right work of Data Access layer ( {@code INewsDao} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.newsmanagement.dao.INewsDao
 * @see com.epam.training.newsmanagement.entity.News
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/spring.test.config.xml" })
@DatabaseSetup(value = "/test.dataset.xml")
public class NewsDaoImplTest {

	@Autowired
	private INewsDao newsDao;

	/**
	 * Data Base Unit test, that verify transactional methods in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@Transactional
	public void testTransactioanl() throws Exception {
		Long newsId = null;
		String title = "news title";
		String shortText = "short Text";
		String fullText = "full text";
		Timestamp creationDate = Timestamp.valueOf("2014-08-08 10:34:12");
		Date modificationDate = Date.valueOf("2015-08-08");

		News news = new News();
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);

		Author author = new Author();
		author.setId(Long.valueOf(1L));
		author.setName("aaa");

		List<Tag> tagList = new ArrayList<Tag>();

		for (int i = 0; i < 2; i++) {
			Tag tag = new Tag();
			tag.setId(Long.valueOf(i + 1));
			tag.setName(String.valueOf(i));
			tagList.add(tag);
		}
		NewsVO newsPost = new NewsVO();
		newsPost.setNews(news);
		newsPost.setAuthor(author);
		newsPost.setTagList(tagList);

		newsId = newsDao.create(news);
		newsDao.associateNewsAuthor(newsId, author.getId());
		newsDao.associateNewsTagList(newsId, tagList);

	}

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;
		String expectedTitle = "news title";
		String expectedShortText = "short Text";
		String expectedFullText = "full text";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2014-08-08 10:34:12");
		Date expectedModificationDate = Date.valueOf("2015-08-08");

		News news = new News();
		news.setTitle(expectedTitle);
		news.setShortText(expectedShortText);
		news.setFullText(expectedFullText);
		news.setCreationDate(expectedCreationDate);
		news.setModificationDate(expectedModificationDate);

		newsId = newsDao.create(news);
		news = newsDao.read(newsId);

		assertEquals(expectedTitle, news.getTitle());
		assertEquals(expectedShortText, news.getShortText());
		assertEquals(expectedFullText, news.getFullText());
		assertEquals(expectedCreationDate, news.getCreationDate());
		assertEquals(expectedModificationDate, news.getModificationDate());
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String expectedTitle = "TITLE1";
		String expectedShortText = "SHORTTEXT1";
		String expectedFullText = "FULLTEXT1";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2015-08-14 12:12:12");
		Date expectedModificationDate = Date.valueOf("2015-08-14");

		News news = newsDao.read(newsId);

		assertEquals(expectedTitle, news.getTitle());
		assertEquals(expectedShortText, news.getShortText());
		assertEquals(expectedFullText, news.getFullText());
		assertEquals(expectedCreationDate, news.getCreationDate());
		assertEquals(expectedModificationDate, news.getModificationDate());
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		int expectedSize = 3;
		String expectedTitle = "TITLE2";
		String expectedShortText = "SHORTTEXT2";
		String expectedFullText = "FULLTEXT2";

		List<News> newsList = newsDao.readAll();
		News news = newsList.get(0);

		assertEquals(expectedSize, newsList.size());
		assertEquals(expectedTitle, news.getTitle());
		assertEquals(expectedShortText, news.getShortText());
		assertEquals(expectedFullText, news.getFullText());
	}

	/**
	 * Data Base Unit test, that verify work readAll(startIndex, endIndex)
	 * method in the {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readInRange() throws Exception {
		int expectedSize = 2;
		List<News> newsList = newsDao.readAll(1, 2);
		assertEquals(expectedSize, newsList.size());
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long newsId = 1L;
		String expectedTitle = "new title";
		String expectedShortText = "new short Text";
		String expectedFullText = "new full text";
		Timestamp expectedCreationDate = Timestamp
				.valueOf("2014-08-18 01:36:12");
		Date expectedModificationDate = Date.valueOf("2015-04-18");

		News news = new News();
		news.setId(newsId);
		news.setTitle(expectedTitle);
		news.setShortText(expectedShortText);
		news.setFullText(expectedFullText);
		news.setCreationDate(expectedCreationDate);
		news.setModificationDate(expectedModificationDate);

		newsDao.update(news);
		news = newsDao.read(newsId);

		assertEquals(expectedTitle, news.getTitle());
		assertEquals(expectedShortText, news.getShortText());
		assertEquals(expectedFullText, news.getFullText());
		assertEquals(expectedCreationDate, news.getCreationDate());
		assertEquals(expectedModificationDate, news.getModificationDate());
	}

	/**
	 * Data Base Unit test, that verify work searchNewsByTag() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		Long tagId = 1L;
		Long expectedNewsId = 1L;
		List<News> newsList = newsDao.searchNewsByTag(tagId);

		assertEquals(expectedNewsId, newsList.get(0).getId());
	}

	/**
	 * Data Base Unit test, that verify work searchNewsByAuthor() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		Long authorId = 1L;
		Long expectedNewsId = 1L;
		List<News> newsList = newsDao.searchNewsByAuthor(authorId);

		assertEquals(expectedNewsId, newsList.get(0).getId());
	}

	/**
	 * Data Base Unit test, that verify work associateNewsAuthor() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = "/data.after.associate.xml", table = "NEWS_AUTHOR", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void associateNewsAuthor() throws Exception {
		Long authorId = 3L;
		Long newsId = 3L;
		newsDao.associateNewsAuthor(newsId, authorId);
	}

	/**
	 * Data Base Unit test, that verify work associateNewsTag() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = "/data.after.associate.xml", table = "NEWS_TAG", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void associateNewsTag() throws Exception {
		Long tagId = 3L;
		Long newsId = 3L;
		newsDao.associateNewsTag(newsId, tagId);
	}

	/**
	 * Data Base Unit test, that verify work searchNewsByFilter() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByFilter() throws Exception {
		Long newsId = 1L;
		Long anotherNewsId = 2L;
		SearchFilter filter = new SearchFilter();
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(Long.valueOf(1));

		filter.setAuthorId(Long.valueOf(1));
		filter.setTagIdList(tagIdList);

		List<News> resultList = newsDao.searchNewsByFilter(filter);
		assertNotNull(resultList);
		assertEquals(newsId, resultList.get(0).getId());
		assertEquals(anotherNewsId, resultList.get(1).getId());
	}

	/**
	 * Data Base Unit test, that verify work deleteAuthorLinks() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = "/data.after.delete.xml", table = "NEWS_AUTHOR", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void deleteAuthorLinks() throws Exception {
		Long newsId = 1L;
		newsDao.deleteAuthorLinks(newsId);
	}

	/**
	 * Data Base Unit test, that verify work deleteTagLinks() method in the
	 * {@code NewsDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.NewsDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = "/data.after.delete.xml", table = "NEWS_TAG", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void deleteTagLinks() throws Exception {
		Long newsId = 1L;
		newsDao.deleteTagLinks(newsId);
	}

}
