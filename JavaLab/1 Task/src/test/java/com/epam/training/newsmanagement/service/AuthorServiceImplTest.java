package com.epam.training.newsmanagement.service;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import com.epam.training.newsmanagement.dao.IAuthorDao;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl;

/**
 * Public class {@code AuthorServiceImplTest} content list of public Mockito
 * Unit tests, which check right work of Service layer class (
 * {@code AuthorServiceImpl} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.mewsmanegement.service.implementation.AuthorServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
	@Mock
	private IAuthorDao authorDao;

	@Autowired
	@InjectMocks
	private AuthorServiceImpl authorService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * {@code AuthorServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long authorId = null;

		Author author = new Author();
		when(authorDao.create(author)).thenReturn(Long.valueOf(1L));

		authorId = authorService.create(author);
		verify(authorDao, times(1)).create(author);
		assertEquals(authorId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * {@code AuthorServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long authorId = 1L;
		String authorName = "Author Name";

		Author author = new Author();
		author.setId(authorId);
		author.setName(authorName);
		when(authorDao.read(authorId)).thenReturn(author);

		Author actualAuthor = authorService.read(authorId);

		verify(authorDao, times(1)).read(authorId);
		assertEquals(actualAuthor, author);

	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * {@code AuthorServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<Author> authorList;
		when(authorDao.readAll()).thenReturn(new ArrayList<>());

		authorList = authorService.readAll();
		verify(authorDao, times(1)).readAll();
		assertNotNull(authorList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * {@code AuthorServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long authorId = 1L;
		String authorName = "New Author Name";
		Author author = new Author();
		author.setId(authorId);
		author.setName(authorName);

		authorService.update(author);
		verify(authorDao, times(1)).update(author);

	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * {@code AuthorServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.AuthorServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long authorId = 1L;
		authorService.delete(authorId);
		verify(authorDao, times(1)).deleteLinks(authorId);
		verify(authorDao, times(1)).delete(authorId);

	}
}
