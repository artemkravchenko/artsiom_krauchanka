package com.epam.training.newsmanagement.service;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.training.newsmanagement.dao.ICommentDao;
import com.epam.training.newsmanagement.entity.Comment;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.service.implementation.CommentServiceImpl;

/**
 * Public class {@code CommentServiceImplTest} content list of public Mockito
 * Unit tests, which check right work of Service layer class (
 * {@code CommentServiceImpl} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.mewsmanegement.service.implementation.CommentServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	@Mock
	private ICommentDao commentDao;

	@Autowired
	@InjectMocks
	private CommentServiceImpl commentService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long commentId = null;

		Comment comment = new Comment();
		when(commentDao.create(comment)).thenReturn(Long.valueOf(1L));

		commentId = commentService.create(comment);
		verify(commentDao, times(1)).create(comment);
		assertEquals(commentId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long commentId = 1L;
		String commentText = "Comment Text";
		Timestamp creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		when(commentDao.read(commentId)).thenReturn(comment);

		Comment actualComment = commentDao.read(commentId);
		verify(commentDao, times(1)).read(commentId);
		assertEquals(actualComment, comment);
	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<Comment> authorList;
		when(commentDao.readAll()).thenReturn(new ArrayList<Comment>());

		authorList = commentService.readAll();
		verify(commentDao, times(1)).readAll();
		assertNotNull(authorList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long commentId = 1L;
		String commentText = "comment text";
		Comment comment = new Comment();
		comment.setId(commentId);
		comment.setCommentText(commentText);

		commentService.update(comment);
		verify(commentDao, times(1)).update(comment);
	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long commentId = 1L;

		commentService.delete(commentId);
		verify(commentDao, times(1)).delete(commentId);
	}

	/**
	 * Public Mockito Unit test, that verify work deleteCommentsByNews() method
	 * in the {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteCommentsByNews() throws Exception {
		Long newsId = 1L;

		commentService.deleteCommentsByNews(newsId);
		verify(commentDao, times(1)).deleteCommentsByNews(newsId);
	}

	/**
	 * Public Mockito Unit test, that verify work searchCommentsByNews() method
	 * in the {@code CommentServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.CommentServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchCommentsByNews() throws Exception {
		List<Comment> commentList = null;
		News news = new News();

		when(commentDao.searchCommentsByNews(news.getId())).thenReturn(
				new ArrayList<Comment>());

		commentList = commentService.searchCommentsByNews(news);
		verify(commentDao, times(1)).searchCommentsByNews(news.getId());
		assertNotNull(commentList);
	}
}
