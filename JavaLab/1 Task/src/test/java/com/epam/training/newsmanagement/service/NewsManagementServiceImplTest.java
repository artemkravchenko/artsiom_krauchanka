package com.epam.training.newsmanagement.service;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.NewsVO;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.service.INewsService;
import com.epam.training.newsmanagement.service.implementation.NewsManagerServiceImpl;

/**
 * Public class {@code NewsManagementServiceImplTest} content list of public
 * Mockito Unit tests, which check right work of Service layer class (
 * {@code NewsManagerServiceImpl} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.mewsmanegement.service.implementation.NewsManagerServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceImplTest {
	@Mock
	private INewsService newsService;

	@Mock
	private ICommentService commentService;

	@InjectMocks
	@Autowired
	private NewsManagerServiceImpl newsManagerService;

	/**
	 * Public Mockito Unit test, that verify work saveNewsPost() method in the
	 * {@code NewsManagerServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsManagerServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void saveNewsPost() throws Exception {
		News news = new News();
		Author author = new Author();
		List<Tag> tagList = new ArrayList<Tag>();

		for (int i = 0; i < 3; i++) {
			tagList.add(new Tag());
		}

		NewsVO newsPost = new NewsVO();
		newsPost.setNews(news);
		newsPost.setAuthor(author);
		newsPost.setTagList(tagList);

		newsManagerService.saveNewsPost(newsPost);

		verify(newsService, times(1)).create(news);
		verify(newsService, times(1)).associateNewsAuthor(news, author);
		verify(newsService, times(1)).associateNewsTagList(news, tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work deleteNewsPost() method in the
	 * {@code NewsManagerServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsManagerServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteNewsPost() throws Exception {
		News news = new News();
		news.setId(Long.valueOf(1L));
		Author author = new Author();
		author.setId(Long.valueOf(1L));

		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag = new Tag();
		tag.setId(Long.valueOf(1L));
		tagList.add(tag);

		NewsVO newsPost = new NewsVO();
		newsPost.setNews(news);
		newsPost.setAuthor(author);
		newsPost.setTagList(tagList);
		newsManagerService.deleteNewsPost(newsPost);

		verify(commentService, times(1)).deleteCommentsByNews(news.getId());
		verify(newsService, times(1)).deleteAuthorLinks(news.getId());
		verify(newsService, times(1)).deleteTagLinks(news.getId());
		verify(newsService, times(1)).delete(news.getId());
	}
}
