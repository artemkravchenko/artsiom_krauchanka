package com.epam.training.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.training.newsmanagement.dao.INewsDao;
import com.epam.training.newsmanagement.entity.Author;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.service.implementation.NewsServiceImpl;

/**
 * Public class {@code NewsServiceImplTest} content list of public Mockito Unit
 * tests, which check right work of Service layer class (
 * {@code NewsServiceImpl} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.mewsmanegement.service.implementation.NewsServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	@Mock
	private INewsDao newsDao;

	@Autowired
	@InjectMocks
	private NewsServiceImpl newsService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;

		News news = new News();
		when(newsDao.create(news)).thenReturn(Long.valueOf(1L));

		newsId = newsService.create(news);
		verify(newsDao, times(1)).create(news);
		assertEquals(newsId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String shortText = "NEW_SH1";
		String fullText = "NEW_FL1";
		String title = "NEW_T1";
		Timestamp creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		Date modificationDate = Date.valueOf("2015-03-29");

		News news = new News();
		news.setId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);

		when(newsDao.read(newsId)).thenReturn(news);

		News actualNews = newsService.read(newsId);
		verify(newsDao, times(1)).read(newsId);
		assertEquals(actualNews, news);
	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<News> tagList = null;
		when(newsDao.readAll()).thenReturn(new ArrayList<News>());

		tagList = newsService.readAll();
		verify(newsDao, times(1)).readAll();
		assertNotNull(tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work readAll(startIndex, endIndex)
	 * method in the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readInRange() throws Exception {
		List<News> tagList = null;
		int startIndex = 1;
		int endIndex = 2;
		when(newsDao.readAll(startIndex, endIndex)).thenReturn(
				new ArrayList<News>());

		tagList = newsService.readAll(startIndex, endIndex);
		verify(newsDao, times(1)).readAll(startIndex, endIndex);
		assertNotNull(tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long newsId = 1L;
		News news = new News();
		news.setId(newsId);

		newsService.update(news);
		verify(newsDao, times(1)).update(news);
	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long newsId = 1L;

		newsService.delete(newsId);
		verify(newsDao, times(1)).delete(newsId);
	}

	/**
	 * Public Mockito Unit test, that verify work searchNewsByTag() method in
	 * the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		List<News> newsList = null;
		Tag tag = new Tag();

		when(newsDao.searchNewsByTag(tag.getId())).thenReturn(
				new ArrayList<News>());

		newsList = newsService.searchNewsByTag(tag);
		verify(newsDao, times(1)).searchNewsByTag(tag.getId());
		assertNotNull(newsList);
	}

	/**
	 * Public Mockito Unit test, that verify work searchNewsByAuthor() method in
	 * the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		List<News> newsList = null;
		Author author = new Author();

		when(newsDao.searchNewsByAuthor(author.getId())).thenReturn(
				new ArrayList<News>());

		newsList = newsService.searchNewsByAuthor(author);
		verify(newsDao, times(1)).searchNewsByAuthor(author.getId());
		assertNotNull(newsList);
	}

	/**
	 * Public Mockito Unit test, that verify work associateNewsAuthor() method
	 * in the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void associateNewsAuthor() throws Exception {
		News news = new News();
		Author author = new Author();

		newsService.associateNewsAuthor(news, author);
		verify(newsDao, times(1)).associateNewsAuthor(news.getId(),
				author.getId());
	}

	/**
	 * Public Mockito Unit test, that verify work associateNewsTag() method in
	 * the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void associateNewsTag() throws Exception {
		News news = new News();
		List<Tag> tagList = new ArrayList<Tag>();

		newsService.associateNewsTagList(news, tagList);
		verify(newsDao, times(1)).associateNewsTagList(news.getId(), tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work deleteAuthorLinks() method in
	 * the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteAuthorLinks() throws Exception {
		Long newsId = 1L;

		newsService.deleteAuthorLinks(newsId);
		verify(newsDao, times(1)).deleteAuthorLinks(newsId);
	}

	/**
	 * Public Mockito Unit test, that verify work deleteTagLinks() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteTagLinks() throws Exception {
		Long newsId = 1L;

		newsService.deleteTagLinks(newsId);
		verify(newsDao, times(1)).deleteTagLinks(newsId);
	}

}
