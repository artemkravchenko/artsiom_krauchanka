package com.epam.training.newsmanagement.service;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.training.newsmanagement.dao.ITagDao;
import com.epam.training.newsmanagement.entity.News;
import com.epam.training.newsmanagement.entity.Tag;
import com.epam.training.newsmanagement.service.implementation.TagServiceImpl;

/**
 * Public class {@code TagServiceImplTest} content list of public Mockito Unit
 * tests, which check right work of Service layer class ( {@code TagServiceImpl}
 * methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.training.mewsmanegement.service.implementation.TagServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	@Mock
	private ITagDao tagDao;

	@Autowired
	@InjectMocks
	private TagServiceImpl tagService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long tagId = null;

		Tag tag = new Tag();
		when(tagDao.create(tag)).thenReturn(Long.valueOf(1L));

		tagId = tagService.create(tag);
		verify(tagDao, times(1)).create(tag);
		assertEquals(tagId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long tagId = 1L;
		String tagName = "Tag Name";

		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setName(tagName);
		when(tagDao.read(tagId)).thenReturn(tag);

		tag = tagService.read(tagId);
		verify(tagDao, times(1)).read(tagId);
		assertEquals(tag.getId(), tagId);
		assertEquals(tag.getName(), tagName);

	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<Tag> tagList;
		when(tagDao.readAll()).thenReturn(new ArrayList<Tag>());

		tagList = tagService.readAll();
		verify(tagDao, times(1)).readAll();
		assertNotNull(tagList);

	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long tagId = 1L;
		Tag tag = new Tag();
		tag.setId(tagId);

		tagService.update(tag);
		verify(tagDao, times(1)).update(tag);

	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long tagId = 1L;

		tagService.delete(tagId);
		verify(tagDao, times(1)).delete(tagId);
	}

	/**
	 * Public Mockito Unit test, that verify work searchTagByNews() method in
	 * the {@code NewsServiceImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.service.implementation.NewsServiceImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagByNews() throws Exception {
		List<Tag> tagList = null;
		News news = new News();

		when(tagDao.searchTagsByNews(news.getId())).thenReturn(
				new ArrayList<Tag>());

		tagList = tagService.searchTagsByNews(news);
		verify(tagDao, times(1)).searchTagsByNews(news.getId());
		assertNotNull(tagList);
	}

}
