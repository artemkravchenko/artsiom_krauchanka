package com.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;
import com.epam.newsmanagement.utils.validators.NewsValidator;

/**
 * {@code AddNewsController} is a controller. It contains methods which handles
 * all the requests which refer to the {@code add-news} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class AddNewsController {

	private static final String AUTHOR_LIST = "authorList";
	private static final String TAG_LIST = "tagList";
	private static final String NEWS_ID = "id";
	private static final String NEWS = "newsVO";

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private INewsManagerService newsManagerService;

	@Autowired
	private NewsValidator newsValidator;

	@RequestMapping("/add-news")
	public String addNews(Model model) throws ServiceException {
		List<Tag> tagList = tagService.readAll();
		List<Author> authorList = authorService.readAll();

		model.addAttribute(TAG_LIST, tagList);
		model.addAttribute(AUTHOR_LIST, authorList);
		model.addAttribute(NEWS, new NewsVO());
		return PropertyUtils.VIEW_ADD_NEWS;
	}

	@RequestMapping(value = "/save-news", method = RequestMethod.POST)
	public String saveNews(Model model,
			@RequestParam(required = false) String[] tagNameList,
			@Valid NewsVO news, Errors result) throws ServiceException {

		newsValidator.validate(news, result);
		if (result.hasErrors()) {
			List<Tag> tagList = tagService.readAll();
			List<Author> authorList = authorService.readAll();

			model.addAttribute(TAG_LIST, tagList);
			model.addAttribute(AUTHOR_LIST, authorList);
			return PropertyUtils.VIEW_ADD_NEWS;
		}

		news.getNews().setCreationDate(
				new Timestamp(System.currentTimeMillis()));
		news.getNews().setModificationDate(
				new Timestamp(System.currentTimeMillis()));
		Author author = authorService.read(news.getAuthor().getName());
		news.setAuthor(author);
		List<Tag> tagList = new ArrayList<Tag>();
		if (tagNameList != null) {
			for (String tagName : tagNameList) {
				Tag tag = tagService.read(tagName);
				tagList.add(tag);
			}
		}

		news.setTagList(tagList);
		newsManagerService.saveNewsPost(news);
		model.addAttribute(NEWS_ID, news.getNews().getId());
		return PropertyUtils.REDIRECT_NEWS_PAGE;
	}

}
