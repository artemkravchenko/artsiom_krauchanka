package com.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;
import com.epam.newsmanagement.utils.validators.NewsValidator;

/**
 * {@code EditNewsController} is a controller. It contains methods which handles
 * all the requests which refer to the {@code edit-news} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class EditNewsController {

	private static final String NEWS_ID = "id";
	private static final String AUTHOR_LIST = "authorList";
	private static final String TAG_LIST = "tagList";
	private static final String NEWS_POST = "newsVO";
	private static final String TAG_ID_LIST = "tagIdList";

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private INewsManagerService newsManagerService;

	@Autowired
	private NewsValidator newsValidator;

	@RequestMapping("/edit-news")
	public String editNews(@RequestParam(required = false) Long id,
			Model model, HttpSession session) throws ServiceException {

		if (id != null) {
			session.setAttribute(NEWS_ID, id);
		} else {
			id = (Long) session.getAttribute(NEWS_ID);
		}

		List<Tag> tagList = tagService.readAll();
		List<Author> authorList = authorService.readAll();

		NewsVO newsVO = newsManagerService.readNewsPost(id);
		if (newsVO != null) {
			List<Long> tagIdList = new ArrayList<Long>();
			for (Tag tag : newsVO.getTagList()) {
				tagIdList.add(tag.getId());
			}

			model.addAttribute(NEWS_POST, newsVO);
			model.addAttribute(TAG_ID_LIST, tagIdList);
			model.addAttribute(TAG_LIST, tagList);
			model.addAttribute(AUTHOR_LIST, authorList);
			return PropertyUtils.VIEW_EDIT_NEWS;
		} else {
			return PropertyUtils.VIEW_ERROR_404;
		}
	}

	@RequestMapping(value = "/update-news")
	public String updateNews(@RequestParam Long newsId,
			@RequestParam String title, @RequestParam String shortText,
			@RequestParam String fullText, @RequestParam Long authorId,
			@RequestParam(required = false) Long[] tagIdList, Model model)
			throws ServiceException {

		NewsVO newsVO = newsManagerService.readNewsPost(newsId);
		newsVO.getNews().setId(newsId);
		newsVO.getNews().setTitle(title);
		newsVO.getNews().setShortText(shortText);
		newsVO.getNews().setFullText(fullText);
		newsVO.getNews().setModificationDate(
				new Timestamp(System.currentTimeMillis()));

		if (!newsValidator.validate(newsVO.getNews())) {
			Author author = authorService.read(authorId);
			newsVO.setAuthor(author);

			List<Tag> tagList = new ArrayList<Tag>();
			if (tagIdList != null) {
				for (Long tagId : tagIdList) {
					Tag tag = tagService.read(tagId);
					tagList.add(tag);
				}
			}
			newsVO.setTagList(tagList);
			newsManagerService.updateNewsPost(newsVO);
		}
		return PropertyUtils.REDIRECT_SHOW_NEWS;
	}
}
