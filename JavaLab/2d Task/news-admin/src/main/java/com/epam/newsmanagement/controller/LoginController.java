package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * {@code LoginController} is a controller. It contains method which handles
 * login request which refer to the {@code login} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class LoginController {
	private static final String ERROR_PARAM = "error";
	private static final String VIEW_LOGIN = "login";

	@RequestMapping("/login")
	public String login(
			@RequestParam(value = "error", required = false) String error,
			Model model) {

		if (error != null) {
			model.addAttribute(ERROR_PARAM, error);
		}
		return VIEW_LOGIN;
	}

}
