package com.epam.newsmanagement.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;
import com.epam.newsmanagement.utils.validators.CommentValidator;

/**
 * {@code NewsPageController} is a controller. It contains methods which handles
 * all the requests which refer to the {@code news-page} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class NewsPageController {

	private static final String NEWS_ID = "id";
	private static final String NEWS_POST = "newsPost";
	private static final String SEARCH_FILTER = "searchFilter";
	private static final String PREV_NEWS_ID = "prevNewsId";
	private static final String NEXT_NEWS_ID = "nextNewsId";
	private static final String COMMENT = "comment";

	@Autowired
	private INewsManagerService newsManagerService;
	@Autowired
	private INewsService newsService;
	@Autowired
	private ICommentService commentService;
	@Autowired
	private CommentValidator commentValidator;

	@RequestMapping("/news-page")
	public String showNewsPost(@RequestParam(required = false) Long id,
			HttpSession session, Model model) throws ServiceException {

		SearchFilter searchFilter = (SearchFilter) session
				.getAttribute(SEARCH_FILTER);

		if (id != null) {
			session.setAttribute(NEWS_ID, id);
		} else {
			id = (Long) session.getAttribute(NEWS_ID);
		}

		return setNewsInformation(id, searchFilter, model);

	}

	@RequestMapping("/delete-comment")
	public String deleteComment(@RequestParam Long newsId, HttpSession session,
			@RequestParam Long commentId, Model model) throws ServiceException {

		commentService.delete(commentId);
		model.addAttribute(NEWS_ID, newsId);

		return PropertyUtils.REDIRECT_NEWS_PAGE;
	}

	@RequestMapping(value = "/post-comment", method = RequestMethod.POST)
	public String postComment(Model model, HttpSession session,
			@Valid Comment comment, Errors result) throws ServiceException {

		SearchFilter searchFilter = (SearchFilter) session
				.getAttribute(SEARCH_FILTER);
		Long newsId = (Long) session.getAttribute(NEWS_ID);

		commentValidator.validate(comment, result);
		if (result.hasErrors()) {

			NewsVO newsPost = newsManagerService.readNewsPost(newsId);

			Long nextNewsId = newsService.next(newsId, searchFilter);
			Long prevNewsId = newsService.previous(newsId, searchFilter);
			model.addAttribute(NEWS_POST, newsPost);
			model.addAttribute(NEXT_NEWS_ID, nextNewsId);
			model.addAttribute(PREV_NEWS_ID, prevNewsId);
			return PropertyUtils.VIEW_NEWS_PAGE;
		}
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		commentService.create(comment);
		return PropertyUtils.REDIRECT_NEWS_PAGE;
	}

	private String setNewsInformation(Long newsId, SearchFilter searchFilter,
			Model model) throws ServiceException {

		NewsVO newsPost = newsManagerService.readNewsPost(newsId);
		if (newsPost == null) {
			return PropertyUtils.VIEW_ERROR_404;
		}

		Long nextNewsId = newsService.next(newsId, searchFilter);
		Long prevNewsId = newsService.previous(newsId, searchFilter);
		model.addAttribute(NEWS_POST, newsPost);
		model.addAttribute(NEXT_NEWS_ID, nextNewsId);
		model.addAttribute(PREV_NEWS_ID, prevNewsId);
		model.addAttribute(COMMENT, new Comment());

		return PropertyUtils.VIEW_NEWS_PAGE;
	}
}
