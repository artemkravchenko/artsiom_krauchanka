package com.epam.newsmanagement.utils;

public class PropertyUtils {
	public static final int NEWS_ON_PAGE = 3;
	public static final int NAV_BUTTONS_ON_PAGE = 7;
	
	public static final String VIEW_ADD_NEWS = "add-news";
	public static final String REDIRECT_ADD_NEWS = "redirect:/add-news";
	public static final String VIEW_EDIT_NEWS = "edit-news";
	public static final String REDIRECT_EDIT_NEWS = "redirect:/edit-news";
	public static final String VIEW_NEWS_PAGE = "news-page";
	public static final String REDIRECT_NEWS_PAGE = "redirect:/news-page";
	public static final String VIEW_SHOW_AUTHORS = "show-authors";
	public static final String REDIRECT_SHOW_AUTHORS = "redirect:/show-authors";
	public static final String VIEW_SHOW_TAGS = "show-tags";
	public static final String REDIRECT_SHOW_TAGS = "redirect:/show-tags";
	public static final String VIEW_SHOW_NEWS = "show-news";
	public static final String REDIRECT_SHOW_NEWS = "redirect:/show-news";
	public static final String VIEW_ERROR_404 = "error404";
	public static final String VIEW_ERROR_500 = "error500";
}
