package com.epam.newsmanagement.utils.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Comment;

/**
 * {@code ComemntValidator} is {@code Validator} interface implementation. It
 * Validates @{code Comment} instances.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Component
public class CommentValidator implements Validator {

	private static final int TEXT_FIELD_LENGTH = 200;
	private static final String TEXT_FIELD = "text";
	private static final String TEXT_FIELD_ERRORCODE = "messages.newspage.comment.error";

	@Override
	public boolean supports(Class<?> clazz) {
		return Comment.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Comment comment = (Comment) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, TEXT_FIELD,
				TEXT_FIELD_ERRORCODE);
		if (comment.getText().length() > TEXT_FIELD_LENGTH) {
			errors.rejectValue(TEXT_FIELD, TEXT_FIELD_ERRORCODE);
		}
	}
}
