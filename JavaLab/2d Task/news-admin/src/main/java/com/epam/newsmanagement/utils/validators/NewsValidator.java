package com.epam.newsmanagement.utils.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;

/**
 * {@code NewsValidator} is {@code Validator} interface implementation. It
 * Validates @{code NewsVO} instances.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Component
public class NewsValidator implements Validator {

	private static final int TITLE_LENGTH = 50;
	private static final int BRIEF_LENGTH = 200;
	private static final int CONTENT_LENGTH = 2000;

	private static final String TITLE_ERRORCODE = "messages.title.error";
	private static final String BRIEF_ERRORCODE = "messages.brief.error";
	private static final String CONTENT_ERRORCODE = "messages.content.error";
	private static final String AUTHOR_ERRORCODE = "messages.authorselect.error";

	private static final String TITLE_FIELD = "news.title";
	private static final String BRIEF_FIELD = "news.shortText";
	private static final String CONTENT_FIELD = "news.fullText";
	private static final String AUTHOR_FIELD = "author.name";

	@Override
	public boolean supports(Class<?> clazz) {
		return News.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		NewsVO news = (NewsVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, TITLE_FIELD,
				TITLE_ERRORCODE);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, BRIEF_FIELD,
				BRIEF_ERRORCODE);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, CONTENT_FIELD,
				CONTENT_ERRORCODE);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, AUTHOR_FIELD,
				AUTHOR_ERRORCODE);

		if (news.getNews().getTitle().length() > TITLE_LENGTH) {
			errors.rejectValue(TITLE_FIELD, TITLE_ERRORCODE);
		}

		if (news.getNews().getShortText().length() > BRIEF_LENGTH) {
			errors.rejectValue(BRIEF_FIELD, BRIEF_ERRORCODE);
		}

		if (news.getNews().getFullText().length() > CONTENT_LENGTH) {
			errors.rejectValue(CONTENT_FIELD, CONTENT_ERRORCODE);
		}
	}

	public boolean validate(Object target) {
		News news = (News) target;
		return validateBrief(news.getShortText())
				|| validateTitle(news.getTitle())
				|| validateContent(news.getFullText());

	}

	private boolean validateTitle(String title) {
		title = title.trim();
		return title.isEmpty() || (title.length() > TITLE_LENGTH);
	}

	private boolean validateBrief(String shortText) {
		shortText = shortText.trim();
		return shortText.isEmpty() || (shortText.length() > BRIEF_LENGTH);
	}

	private boolean validateContent(String fullText) {
		fullText = fullText.trim();
		return fullText.isEmpty() || (fullText.length() > CONTENT_LENGTH);
	}
}
