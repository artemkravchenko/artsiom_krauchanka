package com.epam.newsmanagement.utils.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code TagValidator} is {@code Validator} interface implementation. It
 * Validates @{code Tag} instances.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Component
public class TagValidator implements Validator {
	private Logger logger = LoggerFactory.getLogger(AuthorValidator.class);

	private static final int TAG_NAME_LENGTH = 30;
	private static final String TAG_NAME = "name";
	private static final String TAG_NAME_ERRORCODE = "messages.tag.error";

	@Autowired
	private ITagService tagService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Tag.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Tag tag = (Tag) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, TAG_NAME,
				TAG_NAME_ERRORCODE);
		try {
			if (tag.getName().length() > TAG_NAME_LENGTH
					|| tagService.read(tag.getName()) != null) {
				errors.rejectValue(TAG_NAME, TAG_NAME_ERRORCODE);
			}
		} catch (ServiceException ex) {
			logger.error("tag validation error.", ex);
			errors.reject(TAG_NAME_ERRORCODE);
		}
	}

	public boolean validate(Object target) throws ServiceException {
		Tag tag = (Tag) target;
		String tagName = tag.getName().trim();
		return tagName.isEmpty() || (tagName.length() > TAG_NAME_LENGTH)
				|| tagService.read(tagName) != null;
	}

}
