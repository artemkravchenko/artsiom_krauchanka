<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<form id="update-news-form" action="update-news" class="newspage-content">

	<div class="newspage-content-error-message">
		<span id="title-error-message" class="error-message">
			<spring:message code="messages.title.error"/>
		</span>
	</div>
	
	<div class="newspage-title">
		<span>
			<spring:message code="messages.label.title"/> 
		</span> 
		<input id="titleId" type="text" name="title" value="${newsVO.news.title}" maxlength="50"/>
	</div>
	
	<div class="newspage-content-error-message">
		<span id="brief-error-message" class="error-message">
			<spring:message code="messages.brief.error"/>
		</span>
	</div>
		
	<div class="newspage-brief">
		<span>
			<spring:message code="messages.label.brief"/> 
		</span>
		<textarea id="briefId" name="shortText" maxlength="200">${newsVO.news.shortText}</textarea>

	</div>
	
	<div class="newspage-content-error-message">
		<span id="content-error-message" class="error-message">
			<spring:message code="messages.content.error"/>
		</span>
	</div>
		
	<div class="newspage-text">
		<span>
			<spring:message code="messages.label.content"/> 
		</span> 
			
		<textarea id="contentId" name="fullText" maxlength="2000"><c:out value="${newsVO.news.fullText}"/></textarea>
	</div>
	
	<div class="newspage-content-error-message">
		<span id="authorseelct-error-message" class="error-message">
			<spring:message code="messages.authorselect.error"/>
		</span>
	</div>
		 
	<div class="newspage-select">
			
		<select id="authorSelect" required name="authorId" class="single-select">
			<option value='' disabled selected style="display: none;">
				<spring:message code="messages.filter.author.label" />
			</option>
				
			<c:forEach items="${authorList}" var="author">
				<option value="${author.id}">${author.name}</option>
			</c:forEach>
		</select> 
		
		
		<select id="tagSelect" class="multi-select" multiple name="tagIdList" 
			data-placeholder="<spring:message code="messages.filter.tag.label" />">
				
			<c:forEach items="${tagList}" var="tag">
				<option value="${tag.id}">${tag.name}</option>
			</c:forEach>
		</select>
		<script>
			$(document).ready(setAuthorSelect('${newsVO.author.id}'));
			$(document).ready(setTagSelect('${tagIdList}'));
		</script>
	</div>
		
	<script>
		"use strict";
		$(".single-select").chosen({
			disable_search_threshold : 50
		});
		$(".multi-select").chosen();
	</script>
		
	<div class="newspage-submit">
		<input type="hidden" name="newsId" value="${newsVO.news.id}">
		<input type="submit" class="button" value="<spring:message code="messages.tag.button.update" />">
	</div>
</form>

 <script>
	$("#update-news-form").submit(isNewsValid);
</script> 
