<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/style.css" type="text/css">
<title>access denied!</title>
</head>

<body>
	<div class="error-page">
		<h1>403</h1>
		<h3>
			Access denied. Please <a href="/news-admin/login">log in</a>
		</h3>
	</div>
</body>
</html>