<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="resources/css/style.css" type="text/css">
	<title>page not found</title>
</head>

<body>
	<div class="error-page">
		<h1 style="font-size: 250px;">404</h1>
		<h3>
			<spring:message code="messages.error"/> 
		</h3>
		<a href="/news-admin/show-news">
			<spring:message code="messages.error.link"/> 
		</a>
	</div>
</body>
</html>