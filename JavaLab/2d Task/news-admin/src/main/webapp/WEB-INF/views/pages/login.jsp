<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="authorization">
	
	<form id="login-form" class="authorization-form" action="j_spring_security_check" method="post">
		<input id="login" type='text' name='j_username' value='' autofocus 
			placeholder="<spring:message code="messages.login.placeholder"/> " 
		>
			
		<input id="password" type='password' name='j_password' value='' 
			placeholder="<spring:message code="messages.password.placeholder"/>" 
		>
			
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
		
		<input class="button authorization-button" type="submit" 
			value="<spring:message code="messages.login.submit"/> ">
		
		<c:if test="${not empty error}">
	 		<spring:message code="messages.invalid.parameters"/>
	 	</c:if>
	 	
	 	<div class="newspage-content-error-message">
			<span id="login-error-message" class="error-message">
				<spring:message code="messages.login.empty"/>
			</span>
		</div>
	</form>
	
	<script>
				$("#login-form").submit(isLoginNotEmpty);
	</script>
</div>
