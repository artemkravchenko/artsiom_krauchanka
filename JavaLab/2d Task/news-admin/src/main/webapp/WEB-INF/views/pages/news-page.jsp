<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="content">
	<div class="newspost-content">
		<div class="newspost-title">
			<h3>
				<c:out value="${newsPost.news.title}"/>
			</h3>
			<span> 
				<script>
					getDate('${newsPost.news.modificationDate}');
				</script>
			</span>
			<span>(<spring:message code="messages.content.author.label"/> 
				<c:out value="${newsPost.author.name}"/>)
			</span>
		</div>
		
		<div class="newspost-brief">
			<article>
				<c:out value="${newsPost.news.fullText}"/>
			</article>
		</div>
		
		<div class="newspost-nav">
			<c:if test="${prevNewsId != 0}">
				<a style="float:left;" href="news-page?id=${prevNewsId}" >
					<spring:message code="messages.news.previous"/>
				</a>
			</c:if>
			<c:if test="${nextNewsId != 0}">
				<a style="float:right;" href="news-page?id=${nextNewsId}">
					<spring:message code="messages.news.next"/>
				</a>
			</c:if>
			<div style="clear:both;"></div>
		</div>
		
		<div class="newspost-comments">
			<div class="comments-form">
				

				<form:form id="comment-form" action="post-comment" modelAttribute="comment" method="post">

					<form:textarea path="text" id="comment-text" />
    				<form:errors path="text" cssClass="error"  /><br/>
    				
    				<form:hidden path="newsId" value="${newsPost.news.id}" />
					<input class="button" type="submit" value='<spring:message code="messages.comment.post" />'>
				</form:form>
				
				 <div>
					<span class="error-message" id="comment-message">
						<spring:message code="messages.newspage.comment.error"/>
					</span>
				</div>
				<script>
					$("#comment-form").submit(isCommentNotEmpty);
				</script>   
			</div>
			
			<c:forEach var="comment" items="${newsPost.commentList}">
				<div class="comment">
					<i>
						<script>
							getDate('${comment.creationDate}');
						</script>
					</i>
					
					<div class="comment-text">
						<p>
							<c:out value="${comment.text}"/>
						</p>
						
						<form action="delete-comment">
							<input type="hidden" name="newsId" value="${newsPost.news.id}">
							<input type="hidden" name="commentId" value="${comment.id}">
							<input type="submit"  class="button" value="x">
						</form>
					</div>
				</div>
			</c:forEach>
			
			
		</div>
		
		
	</div>
	
</div>