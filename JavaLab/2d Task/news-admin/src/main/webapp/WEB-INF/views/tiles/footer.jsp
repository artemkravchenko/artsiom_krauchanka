<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<footer>
	<span>
		Copyright@Epam2015. <spring:message code="messages.footer"/>
	</span>
</footer>