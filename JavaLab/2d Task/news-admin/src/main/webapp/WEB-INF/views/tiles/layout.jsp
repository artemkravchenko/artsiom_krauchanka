
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="stylesheet" href="resources/css/chosen.css" type="text/css">
	<link rel="stylesheet" href="resources/css/chosen.min.css" type="text/css">
	<link rel="stylesheet" href="resources/css/style.css" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,700,300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="resources/scripts/chosen.jquery.min.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.jquery.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.proto.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.proto.min.js" type="text/javascript"></script>
	<script src="resources/scripts/script.js" type="text/javascript"></script>
	
	<title>
		<spring:message code="messages.title"/> 
	</title>
</head>

<body>
	<div class="wrapper">
		<tiles:insertAttribute name="header" />
		<div class="content-wrapper">
			<tiles:insertAttribute name="menu" />
			<tiles:insertAttribute name="content" />
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>