<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<header>
	<div class="header-topic">
		<h1>
			News portal - <spring:message code="messages.header.label"/>
		</h1>		
	</div>
	
	<div class="header-info">
		<div class="localeForm">
			    <a href="/news-admin/reset?lang=en">
			    	<spring:message code="messages.lang.en"/>
			    </a> 
    			|
    			<a href="/news-admin/reset?lang=ru">
    				<spring:message code="messages.lang.ru"/>
    			</a>
		</div>
	</div>
</header>
