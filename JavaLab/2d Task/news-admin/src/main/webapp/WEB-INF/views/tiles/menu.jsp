<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<menu>
	<ul>
		<li><a href="/news-admin/reset"><spring:message code="messages.menu.newslist"/></a></li>
		<li><a href="/news-admin/show-authors"><spring:message code="messages.menu.authorlist"/></a></li>
		<li><a href="/news-admin/show-tags"><spring:message code="messages.menu.taglist"/></a></li>
		<li><a href="/news-admin/add-news"><spring:message code="messages.menu.addnews"/></a></li> 
	</ul>
</menu>
