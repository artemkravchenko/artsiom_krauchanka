package com.epam.newsmanagement.command;

/**
 * {@code CommandEnum} is a enumeration class. It contains application command
 * names.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public enum CommandEnum {
	SHOW_NEWS, NEWS_PAGE, POST_COMMENT, FILTER_NEWS, RESET_FILTER, SET_LOCALE, UNKNOWN;
}
