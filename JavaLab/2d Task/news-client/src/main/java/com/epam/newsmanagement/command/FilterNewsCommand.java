package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code Command} interface implementation. Contains logic of searching news.
 * {@code service} method works when request has FILTER_NEWS parameter.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class FilterNewsCommand implements Command {
	private static final String REQUEST_AUTHOR_ID = "authorId";
	private static final String REQUEST_TAGID_LIST = "tagIdList";
	private static final String REQUEST_PAGENUMBER = "pageNumber";
	private static final String REQUEST_SEARCHFILTER = "searchFilter";
	private static final String REQUEST_AUTHOR_LIST = "authorList";
	private static final String REQUEST_TAG_LIST = "tagList";
	private static final String REQUEST_NEWSVO_LIST = "fullNewsList";
	private static final String NAV_START = "startButton";
	private static final String NAV_END = "endButton";

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private INewsManagerService newsManagerService;

	@Override
	public String service(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
		try {
			int pageNumber = 1;
			if (request.getParameter(REQUEST_PAGENUMBER) != null) {
				pageNumber = Integer.parseInt(request.getParameter(REQUEST_PAGENUMBER));
			}
			SearchFilter filter = buildSearchFilter(request);
			request.getSession().setAttribute(REQUEST_SEARCHFILTER, filter);
			setPageInformation(request, filter, pageNumber);
			return PropertyUtils.JSP_SHOW_NEWS;
		} catch (ServiceException ex) {
			logger.error("Exception occured: " + ex.getMessage());
			return PropertyUtils.JSP_ERROR_500;
		}
	}

	private void setPageInformation(HttpServletRequest request, SearchFilter searchFilter, int pageNumber)
			throws ServiceException {

		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();
		List<NewsVO> fullNewsList = newsManagerService.getNewsOnPage(pageNumber, PropertyUtils.NEWS_ON_PAGE,
				searchFilter);

		int pagesCount = getPagesCount(newsService.getCountOfFilterNews(searchFilter));

		int startButton = pageNumber - 3;
		int endButton = pageNumber + 3;
		if (pagesCount >= PropertyUtils.NAV_BUTTONS_ON_PAGE) {
			if (startButton < 1) {
				endButton = endButton + Math.abs(startButton) + 1;
				startButton = 1;
			}

			if (endButton > pagesCount) {
				startButton = startButton - (endButton - pagesCount);
				endButton = pagesCount;
			}
		} else {
			startButton = 1;
			endButton = pagesCount;
		}

		request.getSession().setAttribute(REQUEST_PAGENUMBER, pageNumber);
		request.setAttribute(REQUEST_SEARCHFILTER, searchFilter);
		request.setAttribute(NAV_START, startButton);
		request.setAttribute(NAV_END, endButton);
		request.setAttribute(REQUEST_AUTHOR_LIST, authorList);
		request.setAttribute(REQUEST_TAG_LIST, tagList);
		request.setAttribute(REQUEST_NEWSVO_LIST, fullNewsList);
	}

	private SearchFilter buildSearchFilter(HttpServletRequest request) {
		SearchFilter searchFilter = new SearchFilter();

		Long authorId = null;
		List<Long> tagIdList = null;

		if (request.getParameter(REQUEST_AUTHOR_ID) != null) {
			authorId = Long.valueOf(request.getParameter(REQUEST_AUTHOR_ID));
		}

		if (request.getParameter(REQUEST_TAGID_LIST) != null) {
			String[] list = request.getParameterValues(REQUEST_TAGID_LIST);
			tagIdList = new ArrayList<Long>();
			for (String tag : list) {
				tagIdList.add(Long.valueOf(tag));
			}
		}

		searchFilter.setAuthorId(authorId);
		searchFilter.setTagIdList(tagIdList);
		return searchFilter;
	}

	private int getPagesCount(int countOfRecords) {
		return (countOfRecords + (PropertyUtils.NEWS_ON_PAGE - 1)) / PropertyUtils.NEWS_ON_PAGE;
	}

}
