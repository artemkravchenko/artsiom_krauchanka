package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code Command} interface implementation. Contains logic of displaying single
 * news. {@code service} method works when request has NEWS_PAGE parameter.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class NewsPageCommand implements Command {

	@Autowired
	private INewsManagerService newsManagerService;
	@Autowired
	private INewsService newsService;

	private static final String REQUEST_NEWS_ID = "newsId";
	private static final String REQUEST_NEWS_POST = "newsPost";
	private static final String REQUEST_PREV_NEWS_ID = "prevId";
	private static final String REQUEST_SEARCHFILTER = "searchFilter";
	private static final String REQUEST_NEXT_NEWS_ID = "nextId";
	private static final String REQUEST_TIME_ZONE = "timeZone";

	@Override
	public String service(HttpServletRequest request,
			HttpServletResponse response) throws ServiceException {
		try {
			SearchFilter searchFilter = (SearchFilter) request.getSession()
					.getAttribute(REQUEST_SEARCHFILTER);

			if (request.getParameter(REQUEST_TIME_ZONE) != null) {
				int timeZone = Integer.valueOf(request
						.getParameter(REQUEST_TIME_ZONE));
				request.getSession().setAttribute(REQUEST_TIME_ZONE, timeZone);
			}
			Long newsId = Long.valueOf(request.getParameter(REQUEST_NEWS_ID));

			NewsVO newsVO = newsManagerService.readNewsPost(newsId);
			if (newsVO != null) {
				Long prevId = newsService.previous(newsId, searchFilter);
				Long nextId = newsService.next(newsId, searchFilter);
				request.setAttribute(REQUEST_NEWS_POST, newsVO);
				request.setAttribute(REQUEST_PREV_NEWS_ID, prevId);
				request.setAttribute(REQUEST_NEXT_NEWS_ID, nextId);

				return PropertyUtils.JSP_NEWS_PAGE;
			} else {
				return PropertyUtils.JSP_ERROR_404;
			}
		} catch (ServiceException ex) {
			logger.error("Exception occured: " + ex.getMessage());
			return PropertyUtils.JSP_ERROR_500;
		}

	}
}
