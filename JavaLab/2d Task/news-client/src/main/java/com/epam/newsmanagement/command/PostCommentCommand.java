package com.epam.newsmanagement.command;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code Command} interface implementation. Contains logic of creating comment
 * instance. {@code service} method works when request has POST_COMMENT
 * parameter.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class PostCommentCommand implements Command {

	@Autowired
	private ICommentService commentService;

	@Autowired
	private INewsManagerService newsManagerService;

	@Autowired
	private INewsService newsService;

	private static final String REQUEST_NEWS_ID = "newsId";
	private static final String REQUEST_COMMENT_TEXT = "commentText";
	private static final String REQUEST_NEWS_POST = "newsPost";
	private static final String REQUEST_PREV_NEWS_ID = "prevId";
	private static final String REQUEST_SEARCHFILTER = "searchFilter";
	private static final String REQUEST_NEXT_NEWS_ID = "nextId";

	@Override
	public String service(HttpServletRequest request,
			HttpServletResponse response) throws ServiceException {
		try {
			SearchFilter searchFilter = (SearchFilter) request.getSession()
					.getAttribute(REQUEST_SEARCHFILTER);

			Long newsId = Long.parseLong(request.getParameter(REQUEST_NEWS_ID));
			String commentText = request.getParameter(REQUEST_COMMENT_TEXT);
			commentText = commentText.trim();

			if (!checkComment(commentText)) {
				Comment comment = new Comment();
				comment.setText(commentText);
				comment.setNewsId(newsId);
				comment.setCreationDate(new Timestamp(System.currentTimeMillis()));

				commentService.create(comment);
			}
			Long prevId = newsService.previous(newsId, searchFilter);
			Long nextId = newsService.next(newsId, searchFilter);
			NewsVO newsVO = newsManagerService.readNewsPost(newsId);
			request.setAttribute(REQUEST_NEWS_POST, newsVO);
			request.setAttribute(REQUEST_PREV_NEWS_ID, prevId);
			request.setAttribute(REQUEST_NEXT_NEWS_ID, nextId);

			return PropertyUtils.JSP_REDIRECT_NEWS_PAGE + newsId;
		} catch (ServiceException ex) {
			logger.error("Exception occured: " + ex.getMessage());
			return PropertyUtils.JSP_ERROR_500;
		}
	}

	private boolean checkComment(String comment) {
		comment = comment.trim();
		return comment.isEmpty() || (comment.length() > 100);
	}
}
