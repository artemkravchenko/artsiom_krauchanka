package com.epam.newsmanagement.utils.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ChangeLocaleFilter implements Filter {

	private static final String REQUEST_LOCALE = "locale";
	private static final String LOCALE_RU = "RU";
	private static final String LOCALE_EN = "EN";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		String locale = req.getParameter(REQUEST_LOCALE);

		if (locale != null && (locale.equals(LOCALE_RU) || locale.equals(LOCALE_EN))) {
			session.setAttribute(REQUEST_LOCALE, locale);
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
