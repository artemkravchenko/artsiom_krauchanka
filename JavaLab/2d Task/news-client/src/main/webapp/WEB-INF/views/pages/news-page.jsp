<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
	<fmt:setLocale value="${sessionScope.locale}" />
	<fmt:setBundle basename="Resources" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" href="resources/css/clientstyle.css" type="text/css">
	<link rel="stylesheet" href="resources/css/chosen.css" type="text/css">
	<link rel="stylesheet" href="resources/css/chosen.min.css" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,700,300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="resources/scripts/chosen.jquery.min.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.jquery.js" type="text/javascript"></script>	
	<script src="resources/scripts/chosen.proto.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.proto.min.js" type="text/javascript"></script>
	<script src="resources/scripts/script.js" type="text/javascript"></script>

	<title>
		<fmt:message key="messages.title" />
	</title>
</head>

<body>
	<div class="wrapper">

		<jsp:include page="/WEB-INF/views/tiles/header.jsp" />

		<div class="content-wrapper">
			<div class="content">
				<a href="do?action=SHOW_NEWS"> 
					<fmt:message key="messages.news.back" />
				</a>
				
				<div class="newspost-content">
					<div class="newspost-title">
						<h3>
							<c:out value="${newsPost.news.title}"/>
						</h3>
						<span>
							<script>
								getDate('${newsPost.news.modificationDate}');
							</script>
						</span> 
						
						<span>
							(
							<fmt:message key="messages.content.author.label" /> 
							<c:out value="${newsPost.author.name}"/>
							)
						</span>
					</div>

					<div class="newspost-brief">
						<article>
							<c:out value="${newsPost.news.fullText}"/>
						</article>
					</div>
					
					<div class="newspost-nav">
						<c:if test="${prevId != 0}">
							<a style="float: left;" href="do?action=NEWS_PAGE&newsId=${prevId}">
								<fmt:message key="messages.news.previous" />
							</a>
						</c:if>
						<c:if test="${nextId != 0}">
							<a style="float: right;" href="do?action=NEWS_PAGE&newsId=${nextId}">
								<fmt:message key="messages.news.next" />
							</a>
						</c:if>
					</div>

					<div class="newspost-comments">
						
						
					
						<div class="comments-form">
							<form id="comment-form" action="do" method="post">
								<textarea
									id="comment-text"
									placeholder="<fmt:message key="messages.comment.placeholder"/>"
									autofocus name="commentText" maxlength="100"></textarea>
								
								<div>
									<span class="error-message" id="comment-message">
										<fmt:message key="messages.newspage.comment.error"/>
									</span>
								</div>
								
								<input type="hidden" name="action" value="POST_COMMENT">
								<input type="hidden" name="newsId" value="${newsPost.news.id}">
								<input type="submit" class="button"
									value="<fmt:message  key="messages.comment.post"/>">
							</form>
							
							<script>
								$("#comment-form").submit(isCommentNotEmpty);
							</script>
						</div>
						
						<c:forEach var="comment" items="${newsPost.commentList}">
							<div class="comment">
								<i>
									<script>
										getDate('${comment.creationDate}');
									</script>
								</i>
								<div class="comment-text">
									<p>
										<c:out value="${comment.text}"/>
									</p>
								</div>
							</div>
						</c:forEach>
					</div>

					
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/views/tiles/footer.jsp" />
	</div>
</body>
</html>