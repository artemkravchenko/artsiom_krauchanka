<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="Resources"/>	
		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" href="resources/css/clientstyle.css" type="text/css">
	<link rel="stylesheet" href="resources/css/chosen.css" type="text/css">
	<link rel="stylesheet" href="resources/css/chosen.min.css" type="text/css">
	
	<link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,700,300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="resources/scripts/chosen.jquery.min.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.jquery.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.proto.js" type="text/javascript"></script>
	<script src="resources/scripts/chosen.proto.min.js" type="text/javascript"></script>
	<script src="resources/scripts/script.js" type="text/javascript"></script>
	
	<title>
		<fmt:message key="messages.title"/>
	</title>
</head>

<body>
	<div class="wrapper">

		<jsp:include page="/WEB-INF/views/tiles/header.jsp" />

		<div class="content-wrapper">
			<div class="content">
				<div class="content-search">
					<div class="content-search-forms">
					
					<form id="search-filter-form" class="search-form" action="do">
						<input type="hidden" name="action" value="FILTER_NEWS"> 
						<select id="authorSelect" 
							class="single-select" name="authorId">
							
							<option value='' disabled selected style="display: none;">
								<fmt:message key="messages.filter.author.label"/>
							</option>
							
							<c:forEach items="${authorList}" var="author">
								<option value="${author.id}">${author.name}</option>
							</c:forEach> 
						</select> 
						
						<select id="tagSelect"
							class="multi-select" multiple name="tagIdList"
							data-placeholder="<fmt:message key="messages.filter.tag.label"/>">
							
							<c:forEach items="${tagList}" var="tag">
								<option value="${tag.id}" >${tag.name}</option>
							</c:forEach>
						</select>
						
						<script>
							$(document).ready(setAuthorSelect('${searchFilter.authorId}'));
							$(document).ready(setTagSelect('${searchFilter.tagIdList}'));
						</script>
			
						<script>
							"use strict";
							$(".single-select").chosen({
								disable_search_threshold : 50
							});
							$(".multi-select").chosen();
						</script>

						
						<input
							id="filterButton" type="submit" class="button"
							value="<fmt:message key="messages.content.button.filter"/>">
					</form>
					
					<script>
						$("#search-filter-form").submit(isFilterOptionsChecked);
					</script>
					
					<form action="do">
						<input type="hidden" name="action" value="RESET_FILTER">
						<input id="resetButton" type="submit" class="button" value="<fmt:message key="messages.content.button.reset"/>">
					</form>
					</div>
					
					<div class="content-search-error-message">
						<span class="error-message" id="search-message">
							<fmt:message key="messages.filter.error"/>
						</span>
					</div>
				</div>
				
				

				<div class="content-newslist">
					<c:forEach items="${fullNewsList}" var="fullNews">
						<div class="content-newspost">
							<div class="newspost-title">
								<a href="do?action=NEWS_PAGE&newsId=${fullNews.news.id}">
									<c:out value="${fullNews.news.title}"/> 
								</a> 
								<span> 
									<script>
										getDate('${fullNews.news.modificationDate}');
									</script>
								</span> 
								<span>
									(
									<fmt:message  key="messages.content.author.label"/> 
									<a href="do?action=FILTER_NEWS&authorId=${fullNews.author.id}">
										<c:out value="${fullNews.author.name}"/>
									</a>
									)
								</span>
							</div>

							<div class="newspost-brief">
								<p>
									<c:out value="${fullNews.news.shortText}"/>
								</p>
							</div>

							<div class="newspost-info">
								<c:forEach items="${fullNews.tagList}" var="tag">
									<i>
										<a href="do?action=FILTER_NEWS&tagIdList=${tag.id}">
											<c:out value="${tag.name}"/>
										</a>
									</i>
								</c:forEach>

								<font color="red" style="float: right;"> 
									<fmt:message  key="messages.content.comments"/> (${fn:length(fullNews.commentList)}) 
								</font>
							</div>
						</div>
					</c:forEach>
					
					<c:if test="${empty fullNewsList}">
						<div class="no-news-message">
							<span> 
								<fmt:message key="messages.content.nonewsmessage"/>
							</span>
							<a href="do?action=FILTER_NEWS">
								<fmt:message  key="messages.content.nonewsmessage.link"/>
							</a>
						</div>	
					</c:if>
				</div>

				<div class="content-pages">
					<form action="do">
						<input type="hidden" value="SHOW_NEWS" name="action" />
						
						<c:forEach var="i" begin="${startButton}" end="${endButton}">
							<c:choose>
								<c:when test="${sessionScope.page eq i}">
									<input type="submit" class="button"
										style="background: #A7A7A7;" value="${i}" name="page" />
								</c:when>
								<c:otherwise>
									<input type="submit" class="button" value="${i}" name="page" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					
					</form>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/views/tiles/footer.jsp" />

	</div>
</body>
</html>