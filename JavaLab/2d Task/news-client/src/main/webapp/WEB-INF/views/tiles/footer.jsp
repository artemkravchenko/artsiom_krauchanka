<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resources"/>		

<div class="footer">
	<span>
		EPAM copyright. <fmt:message key="messages.footer"/>
	</span>
</div>