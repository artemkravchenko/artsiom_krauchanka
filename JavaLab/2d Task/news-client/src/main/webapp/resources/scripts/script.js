/**
 * Method isFilterOptionsChecked works when 'filter' button is clicked. It
 * checks if author and tag selects are not empty. If so, 'filter' button become
 * disabled.
 */
function isFilterOptionsChecked() {
	var authorOption = $("#authorSelect :selected").val();
	var tagOption = $("#tagSelect :selected").val();
	if (authorOption || tagOption) {
		return true;
	} else {
		$("#search-message:hidden").show();
		return false;
	}
}

/**
 * Method setAuthorSelect make specific option in author select selected. This
 * option is selected according to the author ID.
 */
function setAuthorSelect(authorId) {
	var authorOptions = document.getElementById('authorSelect').options;
	for (var i = 0; i < authorOptions.length; i++) {
		if (authorOptions[i].value == authorId) {
			authorOptions[i].selected = true;
		}
	}
}

/**
 * Method setTagSelect make specific options in tag select selected. This
 * options are selected according to the tag IDs.
 */
function setTagSelect(str) {
	var tagOptions = document.getElementById('tagSelect').options;
	var tagIdString = str.substring(1, str.length - 1);
	var regExp = /\s*,\s*/;
	var tagIdList = tagIdString.split(regExp);

	for (var i = 0; i < tagOptions.length; i++) {
		if (tagIdList.indexOf(tagOptions[i].value) != -1) {
			tagOptions[i].selected = true;
		}
	}
}
/**
 * Check for comment text validity.
 * 
 * @returns {Boolean}
 */
function isCommentNotEmpty() {
	var comment = $("#comment-text").val();
	if (comment.length == 0 || comment.length > 100) {
		$("#comment-message:hidden").show();
		return false;
	}
	return true;
}

/**
 * 
 * 
 * @returns {Number}
 */
function getDate(stringDate) {
	stringDate = stringDate.split(".")[0];
	stringDate = stringDate.replace(/-/g, "/");

	var formattedDate = new Date(Date.parse(stringDate));
	var offset = -new Date().getTimezoneOffset() / 60;

	formattedDate.setHours(formattedDate.getHours() + offset, formattedDate
			.getMinutes(), formattedDate.getSeconds(), formattedDate
			.getMilliseconds());

	document.write(formattedDate.toLocaleString());
}
/**
 * Function navigationPagination implements pagination for navigation buttons.
 * it shows only those buttons which are in the specific range.
 * 
 * @param currentPage -
 *            current pressed button.
 */
function navigationPagination(currentPage) {
	currentPage = currentPage - 1;
	var items = $(".content-pages input[type=submit]");
	var ITEM_LENGTH = items.length;
	var ITEMS_PER_PAGE = 7;

	if (ITEM_LENGTH >= ITEMS_PER_PAGE) {
		var showFrom = currentPage - 3;
		var showTo = currentPage + 4;

		if (showFrom < 0) {
			showTo = showTo + Math.abs(showFrom);
			showFrom = 0;
		}

		if (showTo > ITEM_LENGTH) {
			showFrom = showFrom - (showTo - ITEM_LENGTH);
			showTo = ITEM_LENGTH;
		}
		items.hide().slice(showFrom, showTo).show();

	} else {
		items.hide().slice(0, ITEMS_PER_PAGE).show();
	}
}
