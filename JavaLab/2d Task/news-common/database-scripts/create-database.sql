--------------------------------------------------------
-- DDL for drop tables and sequences if it exists
--------------------------------------------------------
DROP TABLE "ARTSIOM_KRAUCHANKA"."AUTHOR";
DROP TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS";
DROP TABLE "ARTSIOM_KRAUCHANKA"."NEWS";
DROP TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR";
DROP TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG";
DROP TABLE "ARTSIOM_KRAUCHANKA"."ROLES";
DROP TABLE "ARTSIOM_KRAUCHANKA"."TAG";
DROP TABLE "ARTSIOM_KRAUCHANKA"."USERS";
DROP SEQUENCE "ARTSIOM_KRAUCHANKA"."AUTHOR_ID_SEQ";
DROP SEQUENCE "ARTSIOM_KRAUCHANKA"."COMMENT_ID_SEQ";
DROP SEQUENCE "ARTSIOM_KRAUCHANKA"."NEWS_ID_SEQ";
DROP SEQUENCE "ARTSIOM_KRAUCHANKA"."TAG_ID_SEQ";


--------------------------------------------------------
--  DDL for Sequence AUTHOR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ARTSIOM_KRAUCHANKA"."AUTHOR_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 50 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COMMENT_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ARTSIOM_KRAUCHANKA"."COMMENT_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 50 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NEWS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ARTSIOM_KRAUCHANKA"."NEWS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 50 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAG_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ARTSIOM_KRAUCHANKA"."TAG_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 50 NOCACHE  NOORDER  NOCYCLE ;

--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."AUTHOR" 
   (	"AUTHOR_ID" 	NUMBER(20,0), 
		"AUTHOR_NAME"	VARCHAR2(30 BYTE), 
		"EXPIRED" 		TIMESTAMP (6) DEFAULT NULL
   );
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" 
   (	"COMMENT_ID" 	NUMBER(20,0), 
		"COMMENT_TEXT" 	VARCHAR2(100 BYTE), 
		"CREATION_DATE" TIMESTAMP (6), 
		"NEWS_ID" 		NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."NEWS" 
   (	"NEWS_ID"			NUMBER(20,0), 
		"TITLE"				VARCHAR2(30 BYTE), 
		"SHORT_TEXT"		VARCHAR2(100 BYTE), 
		"FULL_TEXT" 		VARCHAR2(2000 BYTE), 
		"CREATION_DATE" 	TIMESTAMP (6), 
		"MODIFICATION_DATE" DATE
   );
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR" 
   (	"NEWS_ID" 	NUMBER(20,0), 
		"AUTHOR_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG" 
   (	"NEWS_ID" 	NUMBER(20,0), 
		"TAG_ID"	NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."ROLES" 
   (	"USER_ID" 	NUMBER(20,0), 
		"ROLE_NAME" VARCHAR2(50 BYTE)
   );
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."TAG" 
   (	"TAG_ID" 	NUMBER(20,0), 
		"TAG_NAME" 	VARCHAR2(30 BYTE)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "ARTSIOM_KRAUCHANKA"."USERS" 
   (	"USER_ID"	 	NUMBER(20,0), 
		"USER_NAME" 	VARCHAR2(50 BYTE), 
		"USER_LOGIN" 	VARCHAR2(30 BYTE), 
		"USER_PASSWORD" VARCHAR2(30 BYTE)
   );
  
REM INSERTING into ARTSIOM_KRAUCHANKA.AUTHOR
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.COMMENTS
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.NEWS
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.NEWS_TAG
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.ROLES
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.TAG
SET DEFINE OFF;
REM INSERTING into ARTSIOM_KRAUCHANKA.USERS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ARTSIOM_KRAUCHANKA"."USERS_PK" ON "ARTSIOM_KRAUCHANKA"."USERS" ("USER_ID");
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ARTSIOM_KRAUCHANKA"."AUTHOR_PK" ON "ARTSIOM_KRAUCHANKA"."AUTHOR" ("AUTHOR_ID");
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ARTSIOM_KRAUCHANKA"."COMMENTS_PK" ON "ARTSIOM_KRAUCHANKA"."COMMENTS" ("COMMENT_ID");
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ARTSIOM_KRAUCHANKA"."NEWS_PK" ON "ARTSIOM_KRAUCHANKA"."NEWS" ("NEWS_ID");
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ARTSIOM_KRAUCHANKA"."TAG_PK" ON "ARTSIOM_KRAUCHANKA"."TAG" ("TAG_ID");
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."TAG" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID");
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."TAG" MODIFY ("TAG_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."AUTHOR" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID");
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."AUTHOR" MODIFY ("AUTHOR_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USER_ID");
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."USERS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."USERS" MODIFY ("USER_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."USERS" MODIFY ("USER_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."USERS" MODIFY ("USER_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID");
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" MODIFY ("COMMENT_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" MODIFY ("COMMENT_TEXT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID");
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS" MODIFY ("TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."ROLES" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."ROLES" MODIFY ("ROLE_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."COMMENTS" ADD CONSTRAINT "COMMENTS_FK1" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK1" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."AUTHOR" ("AUTHOR_ID") ON DELETE SET NULL ENABLE;
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_FK2" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."NEWS" ("NEWS_ID") ON DELETE SET NULL ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK1" FOREIGN KEY ("NEWS_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."NEWS" ("NEWS_ID") ON DELETE SET NULL ENABLE;
  ALTER TABLE "ARTSIOM_KRAUCHANKA"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_FK2" FOREIGN KEY ("TAG_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."TAG" ("TAG_ID") ON DELETE SET NULL ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "ARTSIOM_KRAUCHANKA"."ROLES" ADD CONSTRAINT "ROLES_FK1" FOREIGN KEY ("USER_ID")
	  REFERENCES "ARTSIOM_KRAUCHANKA"."USERS" ("USER_ID") ENABLE;
