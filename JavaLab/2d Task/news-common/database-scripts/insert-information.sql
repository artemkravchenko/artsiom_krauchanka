/* Insert data to the AUTHOR table  */
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (1, 'Ford');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (2, 'Andrew');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (3, 'Garry');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (4, 'Hudson');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (5, 'Leon');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (6, 'Frank');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (7, 'James');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (8, 'Willis');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (9, 'Carry');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (10, 'Martin');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (11, 'Cooper');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (12, 'Armstrong');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (13, 'Arnold');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (14, 'Richard');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (15, 'Arthur');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (16, 'Eddy');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (17, 'Michael');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (18, 'Theodor');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (19, 'Peter');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (20, 'Ray');


/*  Insert data to the NEWS table  */
INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (1, 'Preamble',   'The preamble to the Constitution serves as an introductory statement', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');
INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (2, 'Article One','Article One describes the Congress.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (3, 'Article Two','Article Two describes the office of the President.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (4, 'Article Three','Article Three describes the court system.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (5, 'Article Four', 'Article Four outlines the relation between the states.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (6, 'Article Five','Article Five outlines the process for amending the Constitution.', 'Article Five outlines the process for amending the Constitution. Eight state constitutions in effect in 1787 included an amendment mechanism. Amendment making power rested with the legislature in three of the states and in the other five it was given to specially elected conventions. The Articles of Confederation provided that amendments were to be proposed by Congress and ratified by the unanimous vote of all thirteen state legislatures. This proved to be a major flaw in the Articles, as it created an insurmountable obstacle to constitutional reform. The amendment process crafted during the Philadelphia Constitutional Convention was, according to The Federalist No. 43, designed to establish a balance between pliancy and rigidity',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (7, 'Article Six', 'Article Six establishes the Constitution.', 'Article Six establishes the Constitution, and all federal laws and treaties of the United States made according to it, to be the supreme law of the land, and that "the judges in every state shall be bound thereby, any thing in the laws or constitutions of any state notwithstanding." It validates national debt created under the Articles of Confederation and requires that all federal and state legislators, officers, and judges take oaths or affirmations to support the Constitution. This means that the states constitutions and laws should not conflict with the laws of the federal constitution and that in case of a conflict, state judges are legally bound to honor the federal laws and constitution over those of any state. Article Six also states "no religious Test shall ever be required as a Qualification to any Office or public Trust under the United States."',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (8, 'Article Seven','Article Seven describes the process for establishing.', 'Article Seven describes the process for establishing the proposed new frame of government. Anticipating that the influence of many state politicians would be Antifederalist, delegates to the Philadelphia Convention provided for ratification of the Constitution by popularly elected ratifying conventions in each state. The convention method also made it possible that judges, ministers and others ineligible to serve in state legislatures, could be elected to a convention. Suspecting that Rhode Island, at least, might not ratify, delegates decided that the Constitution would go into effect as soon as nine states (two-thirds rounded up) ratified.[36] Once ratified by this minimum number of states, it was anticipated that the proposed Constitution would become this Constitution between the nine or more that signed. It would not cover the four or fewer states that might not have signed.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (9, 'Safeguards of liberty', 'The First Amendment (1791) prohibits Congress...', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (10, 'Safeguards of justice','The Fourth Amendment (1791) protects people against...', 'The Fourth Amendment (1791) protects people against unreasonable searches and seizures of either self or property by government officials. A search can mean everything from a frisking by a police officer or to a demand for a blood test to a search of an individuals home or car. A seizure occurs when the government takes control of an individual or something in his or her possession. Items that are seized often are used as evidence when the individual is charged with a crime. It also imposes certain limitations on police investigating a crime and prevents the use of illegally obtained evidence at trial.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (11, 'rights and powers','The Ninth Amendment (1791) declares that individuals have other fundamental rights.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (12, 'Governmental authority', 'The Eleventh Amendment (1795) specifically prohibits federal courts.', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (13, 'Safeguards', 'The Thirteenth Amendment (1865) abolished slavery and involuntary servitude.', 'The Thirteenth Amendment (1865) abolished slavery and involuntary servitude, except as punishment for a crime, and authorized Congress to enforce abolition. Though millions of slaves had been declared free by the 1863 Emancipation Proclamation, their post Civil War status was unclear, as was the status of other millions.[62] Congress intended the Thirteenth Amendment to be a proclamation of freedom for all slaves throughout the nation and to take the question of emancipation away from politics. This amendment rendered inoperative or moot several of the original parts of the constitution.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (14, 'Government processes','The Twelfth Amendment (1804) modifies the way the Electoral College...', 'The Twelfth Amendment (1804) modifies the way the Electoral College chooses the President and Vice President. It stipulates that each elector must cast a distinct vote for President and Vice President, instead of two votes for President. It also suggests that the President and Vice President should not be from the same state. The electoral process delineated by Article II, Section 1, Clause 3 has been superseded by that of this amendment, which also extends the eligibility requirements to become President to the Vice President.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (15, 'Scope and theory', 'Courts established by the Constitution can regulate government.', 'Courts established by the Constitution can regulate government under the Constitution, the supreme law of the land. First, they have jurisdiction over actions by an officer of government and state law. Second, federal courts may rule on whether coordinate branches of national government conform to the Constitution. Until the twentieth century, the Supreme Court of the United States may have been the only high tribunal in the world to use a court for constitutional interpretation of fundamental law, others generally depending on their national legislature.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (16, 'Establishment','When John Marshall followed Oliver Ellsworth as Chief Justice...', 'When John Marshall followed Oliver Ellsworth as Chief Justice of the Supreme Court in 1801, the federal judiciary had been established by the Judiciary Act, but there were few cases, and less prestige. "The fate of judicial review was in the hands of the Supreme Court itself." Review of state legislation and appeals from state supreme courts was understood. But the Courts life, jurisdiction over state legislation was limited. The Marshall Courts landmark Barron v. Baltimore held that the Bill of Rights restricted only the federal government, and not the states.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (17, 'Self-restraint', 'The power of judicial review could not have been preserved...', 'The Court controls almost all of its business by choosing what cases to consider, writs of certiorari. In this way, it can avoid opinions on embarrassing or difficult cases. The Supreme Court limits itself by defining for itself what is a "justiciable question." First, the Court is fairly consistent in refusing to make any "advisory opinions" in advance of actual cases.[m] Second, "friendly suits" between those of the same legal interest are not considered. Third, the Court requires a "personal interest", not one generally held, and a legally protected right must be immediately threatened by government action. Cases are not taken up if the litigant has no standing to sue. Simply having the money to sue and being injured by government action are not enough.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (18, 'Separation of powers','The Supreme Court balances several pressures...', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (19, 'Subsequent Courts','Supreme Courts under the leadership...', 'The preamble to the Constitution serves as an introductory statement of the documents fundamental purposes and guiding principles. It neither assigns powers to the federal government,[27] nor does it place specific limitations on government action. Rather, it sets out the origin, scope and purpose of the Constitution. Its origin and authority is in "We, the people of the United States". This echoes the Declaration of Independence. "One people" dissolved their connection with another, and assumed among the powers of the earth, a sovereign nation-state. The scope of the Constitution is twofold. First, "to form a more perfect Union" than had previously existed in the "perpetual Union" of the Articles of Confederation.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');

INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (20, 'Civic religion', 'There is a viewpoint that some Americans...', 'There is a viewpoint that some Americans have come to see the documents of the Constitution, along with the Declaration of Independence and the Bill of Rights, as being a cornerstone of a type of civil religion. This is suggested by the prominent display of the Constitution, along with the Declaration of Independence and the Bill of Rights, in massive, bronze-framed, bulletproof, moisture-controlled glass containers vacuum-sealed in a rotunda by day and in multi-ton bomb-proof vaults by night at the National Archives Building.',TIMESTAMP '2013-01-15 22:11:35', DATE '2014-04-13');


/* Insert data in NEWS_AUTHOR table */
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (1, 16);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (2, 8);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (3, 1);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (4, 9);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (5, 13);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (6, 13);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (7, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (8, 20);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (9, 14);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (10, 19);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (11, 1);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (12, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (13, 13);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (14, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (15, 20);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (16, 14);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (17, 19);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (18, 1);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (19, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (20, 4);


/*Insert data in COMMENTS table */
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES (1, 'I knew it! After all, I was right!', TIMESTAMP '2013-01-15 22:11:35', 8);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES (2, 'A drop in the ocean.',TIMESTAMP '2013-01-15 22:11:35', 10);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (3, 'A load off ones mind',TIMESTAMP '2013-01-15 22:11:35', 7);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (4, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',12);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (5, 'As plain as the nose on your face', TIMESTAMP '2013-01-15 22:11:35',17);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (6, 'it is very interesting', TIMESTAMP '2013-01-15 22:11:35',11);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (7, 'I knew it!', TIMESTAMP '2013-01-15 22:11:35',1);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (8, 'good to know it', TIMESTAMP '2013-01-15 22:11:35',3);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (9, 'it is very interesting',TIMESTAMP '2013-01-15 22:11:35', 8);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (10, 'Carrot and stick', TIMESTAMP '2013-01-15 22:11:35',1);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (11, 'A political football', TIMESTAMP '2013-01-15 22:11:35',16);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (12, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',14);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (13, 'cool', TIMESTAMP '2013-01-15 22:11:35',2);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (14, 'As easy as apple pie', TIMESTAMP '2013-01-15 22:11:35',3);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (15, 'ut mauris',TIMESTAMP '2013-01-15 22:11:35', 4);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (16, 'useful information!', TIMESTAMP '2013-01-15 22:11:35',15);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (17, 'cool',TIMESTAMP '2013-01-15 22:11:35', 1);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (18, 'useful information!', TIMESTAMP '2013-01-15 22:11:35',9);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) VALUES (19, 'good', TIMESTAMP '2013-01-15 22:11:35',20);
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) VALUES (20, 'it is very interesting', TIMESTAMP '2013-01-15 22:11:35',10);


/* Insert data in TAG table */
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (1, 'ft.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (2, 'yandex.ru');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (3, 'chron.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (4, 'indiatimes.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (5, 'dmoz.org');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (6, 'un.org');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (7, 'noaa.gov');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (8, 'xinhuanet.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (9, 'auda.org.au');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (10, 'unesco.org');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (11, 'cafepress.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (12, 'imageshack.us');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (13, 'desdev.cn');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (14, 'blogs.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (15, 'google.co.uk');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (16, 'ask.com');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (17, 'webeden.co.uk');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (18, 'ca.gov');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (19, 'dev.by');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (20, 'habrahabr.ru');


/*  Insert data in NEWS_TAG table  */
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (1, 20);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (2, 3);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (3, 10);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (4, 9);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (5, 5);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (6, 20);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 18);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 4);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (8, 15);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (8, 1);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (9, 9);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (10, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (10, 15);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (11, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (11, 14);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (11, 3);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (12, 7);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (12, 4);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (13, 17);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (13, 16);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (14, 1);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (14, 9);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (15, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (15, 15);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (16, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (17, 14);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (17, 3);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (18, 7);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (19, 4);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (20, 17);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (20, 16);

