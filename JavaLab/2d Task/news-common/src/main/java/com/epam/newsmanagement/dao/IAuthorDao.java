package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;

/**
 * {@code IAuthorDao} interface extends {@code IGenericDao} interface and
 * provides additional methods for {@code Author} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanegement.dao.IGenericDao
 *
 */
public interface IAuthorDao extends IGenericDao<Author, Long> {

	/**
	 * read {@code Author} instance from DB by author name.
	 * 
	 * @param authorName
	 * @return {@code Author} instance;
	 * @throws DaoException
	 */
	public Author read(String authorName) throws DaoException;

	/**
	 * Search {@code Author} instance in the DB according given {@code News}
	 * instance.
	 *
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return {@code Author} instance.
	 * @throws DaoException
	 */
	public Author searchAuthorByNews(Long newsId) throws DaoException;

	/**
	 * Delete records from intermediate table {@code NEWS_AUTHOR} according
	 * news ID.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @throws DaoException
	 */
	public void deleteAuthorLinks(Long newsId) throws DaoException;

	/**
	 * Update expired field in the AUTHOR table.
	 * 
	 * @param authorId
	 *            {@code Author} instance ID.
	 * 
	 * @throws DaoException
	 */
	public void setExpired(Long authorId) throws DaoException;
}
