package com.epam.newsmanagement.dao;

import java.util.List;
import com.epam.newsmanagement.entity.Tag;

/**
 * {@code ITagDao} interface extends {@code IGenericDao} interface and provides
 * additional methods for {@code Tag} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanegement.dao.IGenericDao
 *
 */
public interface ITagDao extends IGenericDao<Tag, Long> {

	/**
	 * Read {@code Tag} instance from the DB by tag name.
	 * 
	 * @param tagName
	 *            {@code Tag} instance name.
	 * @return {@code Tag} instance.
	 * @throws DaoException
	 */
	public Tag read(String tagName) throws DaoException;

	/**
	 * Search {@code Tag} instances in the DB according given news.
	 *
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return list of {@code Tag} instances.
	 * @throws DaoException
	 */
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException;

	/**
	 * Delete records from table {@code NEWS_TAG} according tag ID.
	 * 
	 * @param tagId
	 *            {@code Tag} instance ID.
	 * @throws DaoException
	 */
	public void deleteLinks(Long tagId) throws DaoException;
	
	/**
	 * Delete records from table {@code NEWS_TAG} according news ID.
	 * 
	 * @param tagId
	 *            {@code News} instance ID.
	 * @throws DaoException
	 */
	public void deleteTagLinks(Long newsId) throws DaoException;

}
