package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * {@code Author} class is POJO class. It contains fields according to the
 * {@code AUTHOR} table in the database.
 * 
 * @author Artsiom_Krauchanka
 * 
 */
public class Author implements Serializable {

	private static final long serialVersionUID = 1869139131182905726L;
	private Long id;
	private String name;
	private Date expired;

	public Author() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash + ((expired == null) ? 0 : expired.hashCode());
		hash = prime * hash + ((id == null) ? 0 : id.hashCode());
		hash = prime * hash + ((name == null) ? 0 : name.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Author other = (Author) obj;

		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired
				+ "]";
	}

}