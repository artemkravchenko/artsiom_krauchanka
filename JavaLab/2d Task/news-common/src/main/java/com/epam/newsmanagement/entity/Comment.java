package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * {@code Comment} class is POJO class. It contains fields according to the
 * {@code Comment} table in the database.
 *
 * @author Artsiom_Krauchanka
 */
public class Comment implements Serializable {

	private static final long serialVersionUID = 473699854147992329L;
	
	private Long id;
	private String text;
	private Date creationDate;
	private Long newsId;

	public Comment() {

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		hash = prime * hash + ((id == null) ? 0 : id.hashCode());
		hash = prime * hash + ((newsId == null) ? 0 : newsId.hashCode());
		hash = prime * hash + ((text == null) ? 0 : text.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Comment other = (Comment) obj;

		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;

		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentText=" + text
				+ ", creationDate=" + creationDate + ", newsId=" + newsId + "]";
	}

}
