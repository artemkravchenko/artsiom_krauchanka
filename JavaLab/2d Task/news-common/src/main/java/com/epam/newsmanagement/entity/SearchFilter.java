package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Search object. {@code SeachFilter} object may contains: author entity ID and
 * list of tags ID. It will be using for constructing SQL search queries in DAO
 * layer.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class SearchFilter implements Serializable {

	private static final long serialVersionUID = 5418831118464750009L;
	private Long authorId;
	private List<Long> tagIdList;

	public SearchFilter() {

	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((tagIdList == null) ? 0 : tagIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		SearchFilter other = (SearchFilter) obj;

		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;

		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SearchFilter [authorId=" + authorId + ", tagIdList="
				+ tagIdList + "]";
	}

}