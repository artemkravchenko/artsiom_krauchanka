package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;

/**
 * {@code IAuthorService} interface describes the behavior of a particular
 * service layer which working with {@code Author} instance using Data Access
 * layer implementations of interface {@code IAuthorDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.newsmanagement.service.IGenericService
 * @see com.epam.newsmanagement.dao.IAuthorDao
 * @see com.epam.newsmanagement.entity.Author
 */
public interface IAuthorService extends IGenericService<Author, Long> {
	/**
	 * Read {@code Author} instance by name.
	 * 
	 * @param authorName
	 * @return {@code Author} instance.
	 * @throws ServiceException
	 */
	public Author read(String authorName) throws ServiceException;

	/**
	 * Search {@code Author} in the DB according given {@code News} instance
	 * using data access layer.
	 *
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return {@code Author} instance.
	 * @throws ServiceException
	 */
	public Author searchAuthorByNews(Long newsId) throws ServiceException;

	/**
	 * Update expired field in the {@code AUTHOR} table.
	 * 
	 * @param authorId
	 *            {@code Author} instance ID.
	 * @throws ServiceException
	 */
	public void setExpired(Long authorId) throws ServiceException;
	
	/**
	 * Delete records in the {@code NEWS_AUTHOR} table according news ID.
	 * 
	 * @param newsId
	 *            {@code News} instance ID
	 * @throws ServiceException
	 */
	public void deleteAuthorLinks(Long newsId) throws ServiceException;
}
