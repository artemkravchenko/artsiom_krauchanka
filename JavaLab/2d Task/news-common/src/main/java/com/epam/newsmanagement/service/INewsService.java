package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;

import java.util.List;

/**
 * {@code INewsService} interface describes the behavior of a particular service
 * layer which working with {@code News} instance using Data Access layer
 * implementations of interface {@code INewsDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.newsmanagement.service.IGenericService
 * @see com.epam.newsmanagement.dao.INewsDao
 * @see com.epam.newsmanagement.entity.News
 */
public interface INewsService extends IGenericService<News, Long> {
	/**
	 * Return next {@code News} instance ID according search filter.
	 * 
	 * @param newsId
	 *            current {@code News} instance ID
	 * @param searchFilter
	 *            {@code SearchFilter} object
	 * @return next {@code News} instance ID
	 * @throws ServiceException
	 */
	public Long next(Long newsId, SearchFilter searchFilter)
			throws ServiceException;

	/**
	 * Return previous {@code News} instance ID according search filter.
	 * 
	 * @param newsId
	 *            current {@code News} instance ID
	 * @param searchFilter
	 *            {@code SearchFilter} object
	 * @return previous {@code News} instance ID
	 * @throws ServiceException
	 */
	public Long previous(Long newsId, SearchFilter searchFilter)
			throws ServiceException;

	/**
	 * Counts {@code News} instances in the DB according search filter.
	 * 
	 * @param searchFilter
	 *            {@code SearchFilter} object.
	 * @return if searchFilter is null, method returns count of {@code News}
	 *         instances. Otherwise - count of filtered {@code News} instances
	 *         according to searchFilter.
	 * @throws ServiceException
	 */
	public int getCountOfFilterNews(SearchFilter searchFilter)
			throws ServiceException;

	/**
	 * Read all records according searchFilter if exists from DB in the range
	 * [beginIndex, endIndex] order by modification date and count of comments.
	 * 
	 * @param beginIndex
	 * @param endIndex
	 * @param searchFilter
	 *            {@code SearchFilter} object
	 * @return list of {@code News} instances
	 * @throws ServiceException
	 */
	public List<News> readAll(int pageNumber, int newsOnPage,
			SearchFilter searchFilter) throws ServiceException;

	/**
	 * Search {@code News} instances according given {@code Tag} instance.
	 *
	 * @param tag
	 *            {@code Tag} instance
	 * @return list of @{code News} instances.
	 * @throws ServiceException
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException;

	/**
	 * Search {@code News} instances according given {@code Author} instance.
	 *
	 * @param author
	 *            {@code Author} instance
	 * @return list of @{code News} instances.
	 * @throws ServiceException
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException;

	/**
	 * Bind {@code News} and {@code Author} instance in the DB.
	 * 
	 * @param news
	 *            {@code News} instance
	 * @param author
	 *            {@code Author} instance
	 * @throws ServiceException
	 */
	public void associateNewsAuthor(News news, Author author)
			throws ServiceException;

	/**
	 * Bind {@code News} instance and list of {@code Tag} instances in the DB.
	 * 
	 * @param news
	 *            {@code News} instance
	 * @param tagList
	 *            {@code Tag} instances
	 * @throws ServiceException
	 */
	public void associateNewsTagList(News news, List<Tag> tagList)
			throws ServiceException;

}
