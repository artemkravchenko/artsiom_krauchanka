package com.epam.newsmanagement.service;

/**
 * The class {@code ServiceException} is a form of {@code Exception} that
 * indicates conditions that a reasonable application might want to catch in
 * service layer.
 * 
 * @author Artsiom_Krauchanka
 * @see java.lang.Exception
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 952614215494919084L;

	/**
	 * Constructs a new exception with {@code null} as its detail message.
	 */
	public ServiceException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail cause.
	 * 
	 * @param message
	 *            the detail message.
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param cause
	 *            the cause of exception
	 */
	public ServiceException(Throwable cause) {
		super(cause);

	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message
	 *            the detail message.
	 * @param cause
	 *            the cause.
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * Constructs a new exception with the specified detail message, cause,
	 * suppression enabled or disabled, and writable stack trace enabled or
	 * disabled.
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ServiceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
