package com.epam.newsmanagement.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code NewsManagerServiceImpl} class is an element of Service layer. Its
 * works with {@code NewsVO} instance.{@code INewsManagerService} interface
 * implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.entity.NewsVO
 * @see com.epam.newsmanagement.service.INewsManagerService
 */

public class NewsManagerServiceImpl implements INewsManagerService {

	@Autowired
	private INewsService newsService;

	@Autowired
	private ICommentService commentService;

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ITagService tagService;

	@Override
	public NewsVO readNewsPost(Long newsId) throws ServiceException {
		NewsVO newsVO = null;
		if (newsService.read(newsId) != null) {
			newsVO = new NewsVO();
			newsVO.setAuthor(authorService.searchAuthorByNews(newsId));
			newsVO.setNews(newsService.read(newsId));
			newsVO.setCommentList(commentService.searchCommentsByNews(newsId));
			newsVO.setTagList(tagService.searchTagsByNews(newsId));
		}
		return newsVO;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveNewsPost(NewsVO newsPost) throws ServiceException {
		Long newsId = newsService.create(newsPost.getNews());
		newsPost.getNews().setId(newsId);

		newsService.associateNewsAuthor(newsPost.getNews(),
				newsPost.getAuthor());
		newsService.associateNewsTagList(newsPost.getNews(),
				newsPost.getTagList());
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateNewsPost(NewsVO newsPost) throws ServiceException {
		Long newsId = newsPost.getNews().getId();

		newsService.update(newsPost.getNews());
		authorService.deleteAuthorLinks(newsId);
		tagService.deleteTagLinks(newsId);
		newsService.associateNewsAuthor(newsPost.getNews(),
				newsPost.getAuthor());

		newsService.associateNewsTagList(newsPost.getNews(),
				newsPost.getTagList());
	}

	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteNewsPost(Long newsId) throws ServiceException {
		commentService.deleteCommentsByNews(newsId);
		authorService.deleteAuthorLinks(newsId);
		tagService.deleteTagLinks(newsId);
		newsService.delete(newsId);
	}

	
	
	
	@Override
	public List<NewsVO> getNewsOnPage(int pageNumber, int newsOnPage,
			SearchFilter searchFilter) throws ServiceException {
		Author author = null;
		List<Tag> tagList = null;
		List<Comment> commentList = null;

		List<NewsVO> fullNewsList = new ArrayList<NewsVO>();
		List<News> newsList = newsService.readAll(pageNumber, newsOnPage,
				searchFilter);

		for (News news : newsList) {
			author = authorService.searchAuthorByNews(news.getId());
			tagList = tagService.searchTagsByNews(news.getId());
			commentList = commentService.searchCommentsByNews(news.getId());

			NewsVO newsVO = new NewsVO();
			newsVO.setNews(news);
			newsVO.setAuthor(author);
			newsVO.setCommentList(commentList);
			newsVO.setTagList(tagList);
			
			fullNewsList.add(newsVO);
		}

		return fullNewsList;
	}

}
