package com.epam.newsmanagement.service.implementation;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ServiceException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * {@code NewsServiceImpl} class is an element of Service layer. Its works with
 * {@code News} database instance. {@code INewsService} interface
 * implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.service.INewsService
 * @see com.epam.newsmanagement.entity.News
 */
@Service
public class NewsServiceImpl implements INewsService {

	@Autowired
	private INewsDao newsDao;

	@Override
	public Long create(News news) throws ServiceException {
		if (news != null) {
			try {
				Long newsId = newsDao.create(news);
				logger.info("News instance created. ID: " + newsId);
				return newsId;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

	@Override
	public News read(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				News news = newsDao.read(newsId);
				logger.info("News record read: " + news);
				return news;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

	@Override
	public List<News> readAll() throws ServiceException {
		try {
			List<News> newsList = newsDao.readAll();
			logger.info("List of News records read: " + newsList);
			return newsList;
		} catch (DaoException ex) {
			logger.error("News service error: ",ex);
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> readAll(int pageNumber, int newsOnPage,
			SearchFilter searchFilter) throws ServiceException {

		if (pageNumber < 1 || newsOnPage < 1) {
			throw new ServiceException("Invalid search range.");
		} else {

			try {
				int endIndex = pageNumber * newsOnPage;
				int beginIndex = endIndex - (newsOnPage - 1);
				List<News> newsList = newsDao.readAll(beginIndex, endIndex,
						searchFilter);
				logger.info("List of News records read: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		}
	}

	@Override
	public void update(News news) throws ServiceException {
		if (news != null) {
			try {
				newsDao.update(news);
				logger.info("News instance updated. ID: " + news.getId());
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("News instance is empty");
		}
	}

	@Override
	public void delete(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDao.delete(newsId);
				logger.info("News instance deleted. ID: " + newsId);
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

	public int getCountOfFilterNews(SearchFilter searchFilter)
			throws ServiceException {
		try {
			return newsDao.getCountOfFilterNews(searchFilter);
		} catch (DaoException ex) {
			logger.error("News service error: ",ex);
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				List<News> newsList = newsDao.searchNewsByTag(tag.getId());
				logger.info("List of News records founded: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Tag instance is empty");
		}
	}

	@Override
	public List<News> searchNewsByAuthor(Author author) throws ServiceException {
		if (author != null) {
			try {
				List<News> newsList = newsDao
						.searchNewsByAuthor(author.getId());
				logger.info("List of News records founded: " + newsList);
				return newsList;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Author instance is empty");
		}
	}

	@Override
	public void associateNewsAuthor(News news, Author author)
			throws ServiceException {
		if (news == null || author == null) {
			throw new ServiceException("Empty parameters error.");
		}
		try {
			newsDao.associateNewsAuthor(news.getId(), author.getId());
			logger.info("news and author instances associated.");
		} catch (DaoException ex) {
			logger.error("News service error: ",ex);
			throw new ServiceException(ex);
		}
	}

	@Override
	public void associateNewsTagList(News news, List<Tag> tagList)
			throws ServiceException {
		if (news == null || tagList == null) {
			throw new ServiceException("Empty parameters error.");
		}
		try {
			newsDao.associateNewsTagList(news.getId(), tagList);
			logger.info("news and tags instances associated.");
		} catch (DaoException ex) {
			logger.error("News service error: ",ex);
			throw new ServiceException(ex);
		}
	}

	

	
	@Override
	public Long next(Long newsId, SearchFilter searchFilter)
			throws ServiceException {
		if (newsId > 0) {
			try {
				Long nextId = newsDao.next(newsId, searchFilter);
				logger.info("News instance founded. ID: " + nextId);
				return nextId;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

	@Override
	public Long previous(Long newsId, SearchFilter searchFilter)
			throws ServiceException {
		if (newsId > 0) {
			try {
				Long prevId = newsDao.previous(newsId, searchFilter);
				logger.info("News instance founded. ID: " + prevId);
				return prevId;
			} catch (DaoException ex) {
				logger.error("News service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

}
