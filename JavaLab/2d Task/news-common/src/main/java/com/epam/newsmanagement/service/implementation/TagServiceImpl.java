package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code TagServiceImpl} class is an element of Service layer. Its works with
 * {@code Tag} database instance. {@code ITagService} interface implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.service.ITagService
 * @see com.epam.newsmanagement.entity.Tag
 */
public class TagServiceImpl implements ITagService {

	@Autowired
	private ITagDao tagDao;

	@Override
	public Long create(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				Long tagId = tagDao.create(tag);
				logger.info("Tag instance created. ID: " + tagId);
				return tagId;
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Tag instance is empty");
		}
	}

	@Override
	public Tag read(Long tagId) throws ServiceException {
		if (tagId > 0) {
			try {
				Tag tag = tagDao.read(tagId);
				logger.info("Tag record read: " + tag);
				return tag;
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid Tag instance ID.");
		}
	}

	@Override
	public Tag read(String tagName) throws ServiceException {
		if (tagName != null) {
			try {
				Tag tag = tagDao.read(tagName);
				logger.info("Tag record read: " + tag);
				return tag;
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid Tag instance name.");
		}
	}

	@Override
	public List<Tag> readAll() throws ServiceException {
		try {
			List<Tag> tagList = tagDao.readAll();
			logger.info("list of Tag records read: " + tagList);
			return tagList;
		} catch (DaoException ex) {
			logger.error("Tag service error: ",ex);
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				tagDao.update(tag);
				logger.info("Tag instance updated. ID: " + tag.getId());
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Tag instance is empty");
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void delete(Long tagId) throws ServiceException {
		if (tagId > 0) {
			try {
				tagDao.deleteLinks(tagId);
				tagDao.delete(tagId);
				logger.info("Tag record deleted. ID: " + tagId);
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid Tag instance ID.");
		}
	}

	@Override
	public void deleteTagLinks(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				tagDao.deleteTagLinks(newsId);
				logger.info("associated Tag and Author records deleted.");
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

	@Override
	public List<Tag> searchTagsByNews(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				List<Tag> tagList = tagDao.searchTagsByNews(newsId);
				logger.info("list of Tag records founded: " + tagList);
				return tagList;
			} catch (DaoException ex) {
				logger.error("Tag service error: ",ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

}
