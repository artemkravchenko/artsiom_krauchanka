package com.epam.newsmanagement.service.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.implementation.UserDaoImpl;
import com.epam.newsmanagement.entity.User;

/**
 * {@code UserServiceImpl} class is a service class. Implements
 * {@code UserDetailsService} method for loading user-specific data.
 * 
 * 
 * @author Artsiom_Krauchanka
 * @see org.springframework.security.core.userdetails.UserDetailsService
 */
@Service
public class UserServiceImpl implements UserDetailsService {
	public static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDaoImpl userDao;

	/**
	 * {@code UserDetailsService} method implementation. Loads user-specific
	 * data using DAO layer.
	 * 
	 * @return {@code User} object according to specific name or @{code null} if
	 *         user does not exist.
	 */
	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;

		try {
			user = userDao.loadUserByUsername(username);
			return user;
		} catch (DaoException ex) {
			logger.error("User service error: ", ex);
			throw new UsernameNotFoundException(ex.getMessage());
		}

	}
}
