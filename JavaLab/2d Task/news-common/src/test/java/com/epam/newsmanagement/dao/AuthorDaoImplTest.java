package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * {@code AuthorDaoImplTest} class content list of DB Unit tests, which check
 * right work of Data Access layer ( {@code IAuthorDao} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IAuthorDao
 * @see com.epam.newsmanagement.entity.Author
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/spring-testconfig.xml" })
@DatabaseSetup(value = "/test-dataset.xml")
public class AuthorDaoImplTest {

	@Autowired
	private IAuthorDao authorDao;

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long authorId = null;
		String expectedAuthorName = "Robert";

		Author author = new Author();
		author.setName(expectedAuthorName);

		authorId = authorDao.create(author);
		author = authorDao.read(authorId);

		assertEquals(expectedAuthorName, author.getName());
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long authorId = 1L;
		String expectedAuthorName = "artem";

		Author author = new Author();
		author = authorDao.read(authorId);

		assertEquals(expectedAuthorName, author.getName());
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		int expectedSize = 3;
		String expectedAuthorName = "artem";

		List<Author> authorList = authorDao.readAll();
		assertEquals(expectedSize, authorList.size());
		assertEquals(expectedAuthorName, authorList.get(0).getName());
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long authorId = 1L;
		String expectedAuthorName = "newArtem";

		Author author = new Author();
		author.setId(authorId);
		author.setName(expectedAuthorName);
		author.setExpired(null);

		authorDao.update(author);
		author = authorDao.read(authorId);

		assertEquals(expectedAuthorName, author.getName());

	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long authorId = 1L;
		//authorDao.deleteLinks(authorId);
		authorDao.delete(authorId);

		assertNull(authorDao.read(authorId));
	}

	/**
	 * Data Base Unit test, that verify work setExpired() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void setExpired() throws Exception {
		Long authorId = 1L;
		authorDao.setExpired(authorId);
		assertNotNull(authorDao.read(authorId).getExpired());
	}

	/**
	 * Data Base Unit test, that verify work searchAuthorByNews() method in the
	 * {@code AuthorDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.AuthorDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchAuthorByNews() throws Exception {
		Long newsId = 1L;
		String expectedAuthorName = "artem";
		Author author = authorDao.searchAuthorByNews(newsId);

		assertEquals(expectedAuthorName, author.getName());
	}

}
