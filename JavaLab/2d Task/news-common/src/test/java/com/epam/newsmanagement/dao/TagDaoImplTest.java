package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * {@code TagDaoImplTest} class content list of DB Unit tests, which check right
 * work of Data Access layer ( {@code ITagDao} methods).
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.ITagDao
 * @see com.epam.newsmanagement.entity.Tag
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/spring-testconfig.xml" })
@DatabaseSetup(value = "/test-dataset.xml")
public class TagDaoImplTest {

	@Autowired
	private ITagDao tagDao;

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long tagId = null;
		String expectedTagName = "test tag";

		Tag tag = new Tag();
		tag.setName(expectedTagName);

		tagId = tagDao.create(tag);
		tag = tagDao.read(tagId);

		assertEquals(expectedTagName, tag.getName());
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long tagId = 1L;
		String expectedTagName = "1TAG";
		Tag tag = tagDao.read(tagId);

		assertEquals(expectedTagName, tag.getName());
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		int expectedSize = 3;
		String expectedTagName = "1TAG";

		List<Tag> tagList = tagDao.readAll();
		assertEquals(expectedSize, tagList.size());
		assertEquals(expectedTagName, tagList.get(0).getName());
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.training.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long tagId = 1L;
		String expectedTagName = "new 1TAG";

		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setName(expectedTagName);

		tagDao.update(tag);
		tag = tagDao.read(tagId);

		assertEquals(expectedTagName, tag.getName());

	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long tagId = 1L;
		tagDao.deleteLinks(tagId);
		tagDao.delete(tagId);
		assertNull(tagDao.read(tagId));
	}

	/**
	 * Data Base Unit test, that verify work searchTagByNews() method in the
	 * {@code TagDaoImpl} class.
	 *
	 * @see com.epam.newsmanagement.dao.implementation.TagDaoImpl
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagByNews() throws Exception {
		Long newsId = 1L;
		String expectedTagName = "1TAG";

		List<Tag> tagList = tagDao.searchTagsByNews(newsId);

		assertEquals(expectedTagName, tagList.get(0).getName());
	}
}
