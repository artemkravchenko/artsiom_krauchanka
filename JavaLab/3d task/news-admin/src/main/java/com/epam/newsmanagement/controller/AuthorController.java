package com.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;
import com.epam.newsmanagement.utils.validators.AuthorValidator;

/**
 * {@code AuthorController} is a controller. It contains methods which handles
 * all the requests which refer to the {@code show-authors} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class AuthorController {

	private static final String AUTHOR_LIST = "authorList";
	private static final String AUTHOR = "author";

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private AuthorValidator authorValidator;

	@RequestMapping("/show-authors")
	public String showAuthors(Model model) throws ServiceException {

		List<Author> authorList = authorService.readAll();
		model.addAttribute(AUTHOR_LIST, authorList);

		model.addAttribute(AUTHOR, new Author());
		return PropertyUtils.VIEW_SHOW_AUTHORS;
	}

	@RequestMapping(value = "/edit-author", method = RequestMethod.POST)
	public String updateAuthor(@RequestParam Long authorId, @RequestParam String authorName, Model model)
			throws ServiceException {

		Author author = new Author();
		author.setId(authorId);
		author.setName(authorName);

		if (!authorValidator.validate(author)) {
			authorService.update(author);
		}
		return PropertyUtils.REDIRECT_SHOW_AUTHORS;
	}

	@RequestMapping("/expire-author")
	public String expireAuthor(@RequestParam Long authorId, Model model) throws ServiceException {
		Author author = authorService.read(authorId);
		if (author.getExpired() == null) {
			author.setExpired(new Timestamp(System.currentTimeMillis()));
		} else {
			author.setExpired(null);
		}
		authorService.update(author);
		return PropertyUtils.REDIRECT_SHOW_AUTHORS;
	}

	@RequestMapping(value = "/save-author", method = RequestMethod.POST)
	public String saveAuthor(Model model, @Valid Author author, Errors result) throws ServiceException {
		authorValidator.validate(author, result);

		if (result.hasErrors()) {
			List<Author> authorList = authorService.readAll();
			model.addAttribute(AUTHOR_LIST, authorList);
			return PropertyUtils.VIEW_SHOW_AUTHORS;
		}
		authorService.create(author);
		return PropertyUtils.REDIRECT_SHOW_AUTHORS;
	}

}
