package com.epam.newsmanagement.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.newsmanagement.utils.PropertyUtils;

@ControllerAdvice
public class ExceptionController {
	private Logger logger = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(Exception.class)
	public String handleError(Exception ex) {
		logger.error("Houston we've had a problem: ", ex);
		return PropertyUtils.VIEW_ERROR_500;
	}
}
