package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code NewsListController} is a controller. It contains methods which handles
 * all the requests which refer to the {@code show-news} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class NewsListController {
	private static final String AUTHOR_LIST = "authorList";
	private static final String TAG_LIST = "tagList";
	private static final String NEWS_LIST = "fullNewsList";
	private static final String PAGE_NUMBER = "page";
	private static final String SEARCH_FILTER = "searchFilter";
	private static final String NAV_START = "startButton";
	private static final String NAV_END = "endButton";

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private INewsManagerService newsManagerService;

	@RequestMapping("/show-news")
	public String showNewsList(@RequestParam(defaultValue = "1") int page, Model model, HttpSession session)
			throws ServiceException {

		session.setAttribute(PAGE_NUMBER, page);
		SearchFilter searchFilter = (SearchFilter) session.getAttribute(SEARCH_FILTER);

		setPageInformation(page, model, searchFilter);
		return PropertyUtils.VIEW_SHOW_NEWS;
	}

	@RequestMapping(value = "/delete-news", method = RequestMethod.POST)
	public String deleteNews(@RequestParam(value = "delete") Long[] deleteList, Model model, HttpSession session)
			throws ServiceException {

		if (deleteList != null) {
			SearchFilter searchFilter = (SearchFilter) session.getAttribute(SEARCH_FILTER);
			int pageNumber = (Integer) session.getAttribute(PAGE_NUMBER);

			for (Long id : deleteList) {
				newsManagerService.deleteNewsPost(id);
			}
			setPageInformation(pageNumber, model, searchFilter);
		}
		return PropertyUtils.REDIRECT_SHOW_NEWS;
	}

	@RequestMapping("/reset")
	public String resetSearch(Model model, HttpSession session) throws ServiceException {

		session.removeAttribute(SEARCH_FILTER);
		return PropertyUtils.REDIRECT_SHOW_NEWS;
	}

	@RequestMapping(value = "/search-news")
	public String searchNews(@RequestParam(required = false) Long authorId,
			@RequestParam(required = false) Long[] tagIdList, Model model, HttpSession session) throws ServiceException {

		List<Long> tagList = null;

		if (tagIdList != null) {
			tagList = new ArrayList<Long>();
			for (Long tagId : tagIdList) {
				Tag tag = tagService.read(tagId);
				if (tag != null) {
					tagList.add(tag.getId());
				}
			}
		}
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setAuthorId(authorId);
		searchFilter.setTagIdList(tagList);

		session.setAttribute(SEARCH_FILTER, searchFilter);

		setPageInformation(1, model, searchFilter);
		return PropertyUtils.VIEW_SHOW_NEWS;

	}

	private int getPagesCount(int countOfRecords) {
		return (countOfRecords + (PropertyUtils.NEWS_ON_PAGE - 1)) / PropertyUtils.NEWS_ON_PAGE;
	}

	private void setPageInformation(int pageNumber, Model model, SearchFilter searchFilter) throws ServiceException {

		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();
		List<NewsVO> fullNewsList = newsManagerService.getNewsOnPage(pageNumber, PropertyUtils.NEWS_ON_PAGE,
				searchFilter);

		int pagesCount = getPagesCount(newsService.getCountOfFilterNews(searchFilter));

		int startButton = pageNumber - 3;
		int endButton = pageNumber + 3;
		if (pagesCount >= PropertyUtils.NAV_BUTTONS_ON_PAGE) {
			if (startButton < 1) {
				endButton = endButton + Math.abs(startButton) + 1;
				startButton = 1;
			}

			if (endButton > pagesCount) {
				startButton = startButton - (endButton - pagesCount);
				endButton = pagesCount;
			}
		} else {
			startButton = 1;
			endButton = pagesCount;
		}

		model.addAttribute(SEARCH_FILTER, searchFilter);
		model.addAttribute(NAV_START, startButton);
		model.addAttribute(NAV_END, endButton);
		model.addAttribute(AUTHOR_LIST, authorList);
		model.addAttribute(TAG_LIST, tagList);
		model.addAttribute(NEWS_LIST, fullNewsList);
	}

}
