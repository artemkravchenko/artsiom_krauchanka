package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;
import com.epam.newsmanagement.utils.validators.TagValidator;

/**
 * {@code TagController} is a controller. It contains methods which handles all
 * the requests which refer to the {@code show-tags} page.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Controller
public class TagController {

	private static final String TAG_LIST = "tagList";
	private static final String TAG = "tag";

	@Autowired
	private ITagService tagService;

	@Autowired
	private TagValidator tagValidator;

	@RequestMapping("/show-tags")
	public String showTags(Model model) throws ServiceException {
		List<Tag> tagList = tagService.readAll();
		model.addAttribute(TAG_LIST, tagList);
		model.addAttribute(TAG, new Tag());
		return PropertyUtils.VIEW_SHOW_TAGS;
	}

	@RequestMapping(value = "/edit-tag", method = RequestMethod.POST)
	public String updateTag(@RequestParam Long tagId, @RequestParam String tagName, Model model)
			throws ServiceException {

		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setName(tagName);
		if (!tagValidator.validate(tag)) {
			tagService.update(tag);
		}

		return PropertyUtils.REDIRECT_SHOW_TAGS;
	}

	@RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
	private String deleteTag(@RequestParam Long tagId, Model model) throws ServiceException {

		tagService.delete(tagId);
		return PropertyUtils.REDIRECT_SHOW_TAGS;
	}

	@RequestMapping(value = "/save-tag", method = RequestMethod.POST)
	private String saveTag(Model model, @Valid Tag tag, Errors result) throws ServiceException {
		tagValidator.validate(tag, result);

		if (result.hasErrors()) {
			List<Tag> tagList = tagService.readAll();
			model.addAttribute(TAG_LIST, tagList);
			return PropertyUtils.VIEW_SHOW_TAGS;
		}
		tagService.create(tag);
		return PropertyUtils.REDIRECT_SHOW_TAGS;
	}
}
