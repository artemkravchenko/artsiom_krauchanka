package com.epam.newsmanagement.utils.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code AuthorValidator} is {@code Validator} interface implementation. It
 * Validates @{code Author} instances.
 * 
 * @author Artsiom_Krauchanka
 *
 */
@Component
public class AuthorValidator implements Validator {
	private Logger logger = LoggerFactory.getLogger(AuthorValidator.class);

	private static final int AUTHOR_NAME_LENGTH = 30;
	private static final String AUTHOR_NAME = "name";
	private static final String AUTHOR_NAME_ERRORCODE = "messages.author.error";

	@Autowired
	private IAuthorService authorService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Author.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Author author = (Author) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, AUTHOR_NAME,
				AUTHOR_NAME_ERRORCODE);
		try {
			if (author.getName().length() > AUTHOR_NAME_LENGTH
					|| authorService.read(author.getName()) != null) {
				errors.rejectValue(AUTHOR_NAME, AUTHOR_NAME_ERRORCODE);
			}
		} catch (ServiceException ex) {
			logger.error("author validation error.", ex);
			errors.reject(AUTHOR_NAME_ERRORCODE);
		}
	}

	public boolean validate(Object target) throws ServiceException {
		Author author = (Author) target;
		String authorName = author.getName().trim();
		return authorName.isEmpty()
				|| (authorName.length() > AUTHOR_NAME_LENGTH)
				|| authorService.read(authorName) != null;
	}
}
