<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>



<form:form id="save-news-form" action="save-news" cssClass="newspage-content" modelAttribute="newsVO" method="post">	

	<div class="newspage-content-error-message">
		<span id="title-error-message" class="error-message">
			<spring:message code="messages.title.error"/>
		</span>
		<form:errors path="news.title" />
	</div>
	
	<div class="newspage-title">
		<span> 
			<spring:message code="messages.label.title" />
		</span> 
		<form:input path="news.title" id="titleId" maxlength="50" /> 
	</div>
	
	<div class="newspage-content-error-message">
		<span id="brief-error-message" class="error-message">
			<spring:message code="messages.brief.error"/>
		</span>
		<form:errors path="news.shortText" />
	</div>
	
	<div class="newspage-brief">
		<span>
			<spring:message code="messages.label.brief" />
		</span> 
		<form:textarea path="news.shortText" id="briefId" maxlength="200"/>
	</div>
	
	<div class="newspage-content-error-message">
		<span id="content-error-message" class="error-message">
			<spring:message code="messages.content.error"/>
		</span>
		<form:errors path="news.fullText" />
	</div>
	
	<div class="newspage-text">
		<span> 
			<spring:message code="messages.label.content" />
		</span>
		<form:textarea path="news.fullText" id="contentId" maxlength="2000"/>
	</div>
	
	<div class="newspage-content-error-message">
		<span id="aurhorselect-error-message" class="error-message">
			<spring:message code="messages.authorselect.error"/>
		</span>
		<form:errors path="author.name" />
	</div>
	
	<div class="newspage-select">
		<form:select path="author.name" id="authorSelect" cssClass="single-select">
			<form:option value='' disabled="disabled" cssStyle="display: none;">
				<spring:message code="messages.filter.author.label" />
			</form:option>
			
			<c:forEach items="${authorList}" var="author">
				<c:if test="${author.expired eq nil }">
					<form:option value="${author.name}">${author.name}</form:option>
				</c:if>
			</c:forEach>
		</form:select>
		
		<select class="multi-select" multiple name="tagNameList"
			data-placeholder="<spring:message code="messages.filter.tag.label" />">

			<c:forEach items="${tagList}" var="tag">
				<option>${tag.name}</option>
			</c:forEach>
		</select>
	</div>

	<div class="newspage-submit">
		<input type="submit" class="button"
			value="<spring:message code="messages.tag.button.save" />">
	</div>
	
	<script>
		"use strict";
		$(".single-select").chosen({
			disable_search_threshold : 50
		});
		$(".multi-select").chosen();
	</script>
</form:form>


<script>
	$("#save-news-form").submit(isNewsValid);
</script> 
