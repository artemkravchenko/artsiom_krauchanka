<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="content-editlist">
	<div class="content-scrollList">
		<c:forEach items="${authorList}" var="author">
			<div class="content-editform">
				
				<form method="post" action="edit-author">
					<span>
						<spring:message code="messages.author.label"/>
					</span>
					<c:choose >
						<c:when test="${author.expired ne null}">
							<input id="text${author.id}" type="text" name="authorName" value="${author.name}" disabled="disabled" style="background-color: #FFA9A9;">
						</c:when>
						<c:otherwise>
							<input id="text${author.id}" type="text" name="authorName" value="${author.name}" disabled="disabled">
						</c:otherwise>
					</c:choose>	 
			    	 
					<input  class="button" id="update${author.id}" style="display:none;" type="submit" 
					 	value="<spring:message code="messages.author.button.update"/> "> 
					<input type="hidden" value="${author.id}" name="authorId"> 
				</form> 
					
				<button  class="button showButton" id="hide${author.id}" onClick="showAuthorButtons(${author.id})">
					<spring:message code="messages.button.edit"/>
				</button>
					
				<form action="expire-author">
					<input  class="button showButton" id="expire${author.id}" style="display:none;" type="submit" 
						value="<spring:message code="messages.author.button.expire"/> ">
					<input type="hidden" value="${author.id}" name="authorId"> 
				</form>
				
				<button  class="button showButton" id="cancel${author.id}" style="display:none;" onclick="hideAuthorButtons(${author.id})">
					<spring:message code="messages.button.cancel"/>
				</button>
			</div>
		</c:forEach>
	</div>
	
	<form:form id="author-form" action="save-author" method="post" modelAttribute="author">
		<span>
			<spring:message code="messages.author.addlabel"/> 
		</span>
		
		<form:input id="author-form-authorname" path="name" maxlength="30" /> <br/>
		<form:errors path="name" cssClass="error"/>	<br/>
		<input  class="button" type="submit" value="<spring:message code="messages.author.button.save"/> ">
	</form:form>
	
	 <span id="author-error-message" class="error-message">
		<spring:message code="messages.author.error"/>
	</span>
	
	<script>
		$("#author-form").submit(isAuthorNameValid);
	</script> 
</div>
