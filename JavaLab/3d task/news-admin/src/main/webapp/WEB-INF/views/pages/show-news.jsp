<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="content">
	<div class="content-search">
	<div class="content-search-forms">
		<form id="search-filter-form"  class="search-form" action="search-news">

			<select
			id="authorSelect" class="single-select" name="authorId" >
			
				<option value='' disabled selected style="display: none;">
					<spring:message code="messages.filter.author.label" />
				</option>
					
		 		<c:forEach items="${authorList}" var="author">
					<option value="${author.id}">${author.name}</option>
				</c:forEach> 
			</select> 
			
			<select id="tagSelect" class="multi-select" 
				multiple name="tagIdList" data-placeholder="<spring:message code="messages.filter.tag.label" />">
				
				<c:forEach items="${tagList}" var="tag">
					<option value="${tag.id}" >${tag.name}</option>
				</c:forEach>
			</select>
			
			<script>
				$(document).ready(setAuthorSelect('${searchFilter.authorId}'));
				$(document).ready(setTagSelect('${searchFilter.tagIdList}'));
				
				"use strict";
				$(".single-select").chosen({
					disable_search_threshold : 50
				});
				$(".multi-select").chosen();
			</script>
			
			<input id="filterButton"  type="submit" class="button" 
				value="<spring:message code="messages.content.button.filter"/>">
				
		</form>
		
		<script>
			$("#search-filter-form").submit(isFilterOptionsChecked);
		</script>
	
		<form id="reset-form" action="reset">
			<input id="resetButton" type="submit" class="button" 
				value="<spring:message code="messages.content.button.reset"/>">
		</form>
	</div>
	
	<div class="content-search-error-message">
		
		<span class="error-message" id="search-message">
			<spring:message code="messages.filter.error"/>
		</span>
	</div>
	</div>
	
	<div class="content-delete">
		<form id="delete-news" action="delete-news" method="post">
							
			<c:forEach items="${fullNewsList}" var="fullNews">
				<div class="content-newspost">
					<div class="newspost-title">
						
						<a href="news-page?id=${fullNews.news.id}" >
							<c:out value="${fullNews.news.title}"/>
						</a>
														
						<span>

							 <script>

								getDate('${fullNews.news.modificationDate}');
							</script>
						</span>
						
						<span>
							(<spring:message code="messages.content.author.label"/> 
							<a href="search-news?authorId=${fullNews.author.id}">
								<c:out value="${fullNews.author.name}"/>
							</a>
							)
						</span>
					</div>
								
					<div class="newspost-brief">
						<p>
							<c:out value="${fullNews.news.shortText}"/>
						</p>
					</div>
								
					<div class="newspost-info">
						<c:forEach items="${fullNews.tagList}" var="tag">
							<a href="search-news?tagIdList=${tag.id}">
								<c:out value="${tag.name}"/>
							</a>
						</c:forEach>
													
						<div class="newspost-edit">
							<a href="/news-admin/edit-news?id=${fullNews.news.id}">
								<spring:message code="messages.content.button.edit"/>
							</a>
							<input type="checkbox" name="delete" value="${fullNews.news.id}">	
						</div>
									
						<span>
							<spring:message code="messages.content.comments" /> (${fn:length(fullNews.commentList)})
						</span>
				   	</div>
				   	<div style="clear:both;"></div>
				</div>
			</c:forEach>
				
			<c:choose>
				<c:when test="${empty fullNewsList}">
					<div class="no-news-message">
						<span>
							<spring:message code="messages.content.nonewsmessage"/>
						</span>
					</div>	
				</c:when>
				
				<c:otherwise>
					<input type="submit" class="button"
						value="<spring:message code="messages.content.button.delete" />">
				</c:otherwise>
			</c:choose>
									
			<span class="error-message" id="delete-message">
				<spring:message code="messages.content.button.delete.message"/>
			</span>
		</form>
			
		<script>
			$("#delete-news").submit(isDeleteItemsSelected);
		</script>
	</div>
	
	<div class="content-pages">
		<form action="show-news">
			<c:forEach var="i" begin="${startButton}" end="${endButton}">
				<c:choose>
					<c:when test="${sessionScope.page eq i}">
						<input type="submit" class="button" style="background: #A7A7A7;" value="${i}" name="page" />
					</c:when>
					
					<c:otherwise>
						<input type="submit" class="button" value="${i}" name="page" />
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</form>
	</div>
	
</div>

