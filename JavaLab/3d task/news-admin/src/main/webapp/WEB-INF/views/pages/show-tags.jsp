<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="content-editlist">
	<div class="content-scrollList">
		<c:forEach items="${tagList}" var="tag">
			<div class = "content-editform">
				<form method="post" action="edit-tag">
					<span>
						<spring:message code="messages.tag.label"/>
					</span>    
					
					<input id="text${tag.id}" type="text" name="tagName" value="${tag.name}" disabled="disabled">
					<input id="update${tag.id}" class="button" style="display:none;" type="submit" 
					 	value="<spring:message code="messages.tag.button.update"/>"> 
					<input type="hidden" value="${tag.id}" name="tagId"> 
				</form>
					
				<button  class="button showButton" id="hide${tag.id}" onClick="showTagButtons(${tag.id})">
					<spring:message code="messages.button.edit"/>
				</button>
					
				<form method="post" action="delete-tag">
					<input class="button showButton" id="delete${tag.id}" style="display:none;" type="submit" value="<spring:message code="messages.tag.button.delete"/>"> 
					<input type="hidden" value="${tag.id}" name="tagId">
				</form>
			
				<button  class="button showButton" id="cancel${tag.id}" style="display:none;" onclick="hideTagButtons(${tag.id})">
					<spring:message code="messages.button.cancel"/>
				</button>
			</div>
		</c:forEach>
	</div>
	
	<form:form  id="tag-form" action="save-tag" method="post" modelAttribute="tag">
		<span>
			<spring:message code="messages.tag.addlabel"/> 
		</span>
		
		<form:input id="tag-form-tagname" path="name" maxlength="30" /> <br/>
		<form:errors path="name" cssClass="error"/>	<br/>
		<input  class="button" type="submit" value="<spring:message code="messages.tag.button.save"/> ">
	</form:form>
	
	<span id="tag-error-message" class="error-message">
		<spring:message code="messages.tag.error"/>
	</span>
	
	<script>
		$("#tag-form").submit(isTagNameValid);
	</script> 
</div>
