<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<header>
	<div class="header-topic">
		<h1>
			News portal - <spring:message code="messages.header.label"/>
		</h1>		
	</div>
	
	<div class="header-info">
		<div class="header-info-logout">
			<p><spring:message code="messages.header.greetings"/>
				 <security:authentication property="principal.username" />!	
			</p>
		
			<form action="logout">
				<input class="button" type="submit" 
				value="<spring:message code="messages.header.button.logout"/>" />
			</form>
		</div>
		
		<div class="header-info-localeform">
			    <a href="?lang=en">
			    	<spring:message code="messages.lang.en"/>
			    </a> 
    			|
    			<a href="?lang=ru">
    				<spring:message code="messages.lang.ru"/>
    			</a>
		</div>
	</div>
</header>
