/**
 * Method setAuthorSelect make specific option in author select selected. This
 * option is selected according to the author ID.
 */
function setAuthorSelect(authorId) {
	var authorOptions = document.getElementById('authorSelect').options;
	for (var i = 0; i < authorOptions.length; i++) {
		if (authorOptions[i].value == authorId) {
			authorOptions[i].selected = true;
		}
	}
}

/**
 * Method setTagSelect make specific options in tag select selected. This
 * options are selected according to the tag IDs.
 */
function setTagSelect(str) {
	var tagOptions = document.getElementById('tagSelect').options;
	var tagIdString = str.substring(1, str.length - 1);
	var regExp = /\s*,\s*/;
	var tagIdList = tagIdString.split(regExp);

	for (var i = 0; i < tagOptions.length; i++) {
		if (tagIdList.indexOf(tagOptions[i].value) != -1) {
			tagOptions[i].selected = true;
		}
	}
}

/**
 * Method isFilterOptionsChecked works when 'filter' button is clicked. It
 * checks if author and tag selects are not empty. If so, 'filter' button become
 * disabled.
 */
function isFilterOptionsChecked() {
	var authorOption = $("#authorSelect :selected").val();
	var tagOption = $("#tagSelect :selected").val();
	if (authorOption || tagOption) {
		return true;
	} else {
		$("#search-message:hidden").show();
		return false;
	}
}

/**
 * Check for selected news to delete.
 * 
 * @returns {Boolean}
 */
function isDeleteItemsSelected() {
	var isCheck = $("#delete-news input:checkbox:checked").val();
	if (!isCheck) {
		$("#delete-message:hidden").show();
		return false;
	}
}

/**
 * Method showAuthorButtons works when 'edit' button is clicked in the authors
 * page. It makes additional buttons visible.
 */
function showAuthorButtons(elementId) {
	$("#text" + elementId).removeAttr("disabled");
	$("#update" + elementId + ":hidden").show();
	$("#expire" + elementId + ":hidden").show();
	$("#cancel" + elementId + ":hidden").show();
	$("#hide" + elementId).hide();
}

/**
 * Method hideAuthorButtons works when 'cancel' button is clicked in the authors
 * page. It makes additional buttons enabled.
 */
function hideAuthorButtons(elementId) {
	$("#text" + elementId).prop("disabled", true);
	$("#update" + elementId).hide();
	$("#expire" + elementId).hide();
	$("#cancel" + elementId).hide();
	$("#hide" + elementId + ":hidden").show();
}

/**
 * Method showTagButtons works when 'edit' button is clicked in the tags page.
 * It makes additional buttons visible.
 */
function showTagButtons(elementId) {
	$("#text" + elementId).removeAttr("disabled");
	$("#update" + elementId + ":hidden").show();
	$("#delete" + elementId + ":hidden").show();
	$("#cancel" + elementId + ":hidden").show();
	$("#hide" + elementId).hide();
}

/**
 * Method hideTagButtons works when 'cancel' button is clicked in the tags page.
 * It makes additional buttons enabled.
 */
function hideTagButtons(elementId) {
	$("#text" + elementId).prop("disabled", true);
	$("#update" + elementId).hide();
	$("#delete" + elementId).hide();
	$("#cancel" + elementId).hide();
	$("#hide" + elementId + ":hidden").show();
}

/**
 * Check for author name validity.
 * 
 * @returns {Boolean}
 */
function isAuthorNameValid() {
	var authorname = $("#author-form-authorname").val();
	if (authorname.length == 0 || authorname.length > 30) {
		$("#author-error-message:hidden").show();
		return false;
	}
	return true;
}
/**
 * Check for tag name validity.
 * 
 * @returns {Boolean}
 */
function isTagNameValid() {
	var tagname = $("#tag-form-tagname").val();
	if (tagname.length == 0 || tagname.length > 30) {
		$("#tag-error-message:hidden").show();
		return false;
	}
	return true;
}
/**
 * Check for comment text validity.
 * 
 * @returns {Boolean}
 */
function isCommentNotEmpty() {
	var comment = $("#comment-text").val();
	if (comment.length == 0 || comment.length > 100) {
		$("#comment-message:hidden").show();
		return false;
	}
	return true;
}
/**
 * Check for news post validity.
 * 
 * @returns {Boolean}
 */
function isNewsValid() {
	var title = $("#titleId").val();
	var brief = $("#briefId").val();
	var content = $("#contentId").val();
	var author = $("#authorSelect").val();
	var isErrors = false;

	$("#title-error-message").hide();
	$("#brief-error-message").hide();
	$("#content-error-message").hide();
	$("#aurhorselect-error-message").hide();

	if (title.length == 0 || title.length > 50) {
		$("#title-error-message:hidden").show();
		isErrors = true;
	}

	if (brief.length == 0 || brief.length > 200) {
		$("#brief-error-message:hidden").show();
		isErrors = true;
	}

	if (content.length == 0 || content.length > 2000) {
		$("#content-error-message:hidden").show();
		isErrors = true;
	}

	if (author.length == 0) {
		$("#aurhorselect-error-message:hidden").show();
		isErrors = true;
	}

	if (isErrors) {
		return false;
	}
	return true;

}

/**
 * 
 * 
 * @returns {Number}
 */
function getDate(stringDate) {
	stringDate = stringDate.split(".")[0];
	stringDate = stringDate.replace(/-/g, "/");

	var formattedDate = new Date(Date.parse(stringDate));
	var offset = -new Date().getTimezoneOffset() / 60;

	formattedDate.setHours(formattedDate.getHours() + offset, formattedDate
			.getMinutes(), formattedDate.getSeconds(), formattedDate
			.getMilliseconds());

	document.write(formattedDate.toLocaleString());
}

/**
 * Check for comment text validity.
 * 
 * @returns {Boolean}
 */
function isLoginNotEmpty() {
	var login = $("#login").val();
	var password = $("#password").val();
	if (login.length == 0 || password.length == 0) {
		$("#login-error-message:hidden").show();
		return false;
	}
	return true;
}

/**
 * Function navigationPagination implements pagination for navigation buttons.
 * it shows only those buttons which are in the specific range.
 * 
 * @param currentPage -
 *            current pressed button.
 */
function navigationPagination(currentPage) {
	currentPage = currentPage - 1;
	var items = $(".content-pages input[type=submit]");
	var ITEM_LENGTH = items.length;
	var ITEMS_PER_PAGE = 7;

	if (ITEM_LENGTH >= ITEMS_PER_PAGE) {
		var showFrom = currentPage - 3;
		var showTo = currentPage + 4;

		if (showFrom < 0) {
			showTo = showTo + Math.abs(showFrom);
			showFrom = 0;
		}

		if (showTo > ITEM_LENGTH) {
			showFrom = showFrom - (showTo - ITEM_LENGTH);
			showTo = ITEM_LENGTH;
		}
		items.hide().slice(showFrom, showTo).show();

	} else {
		items.hide().slice(0, ITEMS_PER_PAGE).show();
	}
}
