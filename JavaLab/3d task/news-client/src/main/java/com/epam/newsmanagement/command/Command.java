package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.newsmanagement.service.ServiceException;

/**
 * The interface {@code Command} is a part of a Command pattern implementation.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public interface Command {
	public static Logger logger = LoggerFactory.getLogger(Command.class);

	/**
	 * Returns string representation of view layer location.
	 * 
	 * @param request
	 * @param response
	 */
	public String service(HttpServletRequest request, HttpServletResponse response) throws ServiceException;

}
