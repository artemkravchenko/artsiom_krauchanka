package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * {@code CommandFactory} class is a factory pattern implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.command.Command
 */
public class CommandFactory {
	private static ApplicationContext context = new ClassPathXmlApplicationContext("controller-context.xml");
	private static final String PARAM_COMMAND = "action";

	/**
	 * Returns defined bean from application context according to the command
	 * name from request.
	 * 
	 * @param request
	 * @return {@code Command} instance.
	 */
	public static Command getCommand(HttpServletRequest request) {
		Command command = null;
		CommandEnum commandEnum = null;
		try {
			commandEnum = CommandEnum.valueOf(request.getParameter(PARAM_COMMAND));
		} catch (IllegalArgumentException ex) {
			commandEnum = CommandEnum.UNKNOWN;
		}

		switch (commandEnum) {
		case SHOW_NEWS:
			command = context.getBean(ShowNewsCommand.class);
			break;
		case NEWS_PAGE:
			command = context.getBean(NewsPageCommand.class);
			break;
		case POST_COMMENT:
			command = context.getBean(PostCommentCommand.class);
			break;
		case FILTER_NEWS:
			command = context.getBean(FilterNewsCommand.class);
			break;
		case RESET_FILTER:
			command = context.getBean(ResetFilterCommand.class);
			break;
		case SET_LOCALE:
			command = context.getBean(SetLocaleCommand.class);
			break;
		case UNKNOWN:
			command = context.getBean(UnknownCommand.class);
			break;
		}
		return command;
	}
}
