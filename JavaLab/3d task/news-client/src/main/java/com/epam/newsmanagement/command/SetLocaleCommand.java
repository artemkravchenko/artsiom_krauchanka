package com.epam.newsmanagement.command;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code Command} interface implementation. Contains logic of setting locale.
 * {@code service} method works when request has SET_LOCALE parameter.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class SetLocaleCommand implements Command {

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private INewsManagerService newsManagerService;

	private static final String REQUEST_PAGENUMBER = "page";
	private static final String REQUEST_SEARCHFILTER = "searchFilter";
	private static final String REQUEST_LOCALE = "locale";
	private static final String REQUEST_PAGESCOUNT = "pagesCount";
	private static final String REQUEST_AUTHOR_LIST = "authorList";
	private static final String REQUEST_TAG_LIST = "tagList";
	private static final String REQUEST_NEWSVO_LIST = "fullNewsList";

	public String service(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
		try {
			int pageNumber = 1;
			String localeName = request.getParameter(REQUEST_LOCALE);
			Locale locale = new Locale(localeName);
			request.getSession().setAttribute(REQUEST_LOCALE, locale);

			setPageInformation(request, null, pageNumber);
			request.getSession().removeAttribute(REQUEST_SEARCHFILTER);

			return PropertyUtils.JSP_SHOW_NEWS;
		} catch (ServiceException ex) {
			logger.error("Exception occured: " + ex.getMessage());
			return PropertyUtils.JSP_ERROR_500;
		}

	}

	private int getPagesCount(int countOfRecords) {
		return (countOfRecords + (PropertyUtils.NEWS_ON_PAGE - 1)) / PropertyUtils.NEWS_ON_PAGE;
	}

	private void setPageInformation(HttpServletRequest request, SearchFilter searchFilter, int pageNumber)
			throws ServiceException {

		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();
		List<NewsVO> fullNewsList = newsManagerService.getNewsOnPage(pageNumber, PropertyUtils.NEWS_ON_PAGE,
				searchFilter);

		int pagesCount = getPagesCount(newsService.getCountOfFilterNews(searchFilter));

		request.getSession().setAttribute(REQUEST_PAGENUMBER, pageNumber);
		request.setAttribute(REQUEST_SEARCHFILTER, searchFilter);
		request.setAttribute(REQUEST_PAGESCOUNT, pagesCount);
		request.setAttribute(REQUEST_AUTHOR_LIST, authorList);
		request.setAttribute(REQUEST_TAG_LIST, tagList);
		request.setAttribute(REQUEST_NEWSVO_LIST, fullNewsList);
	}

}
