package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.utils.PropertyUtils;

/**
 * {@code Command} interface implementation. {@code service} method works when
 * request has unknown parameter.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class UnknownCommand implements Command {

	private static final String REQUEST_ACTION = "action";

	@Override
	public String service(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

		logger.error("Unknown command: " + request.getParameter(REQUEST_ACTION));
		return PropertyUtils.JSP_ERROR_404;
	}
}
