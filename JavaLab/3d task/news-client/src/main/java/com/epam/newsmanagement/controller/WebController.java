package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.CommandFactory;
import com.epam.newsmanagement.service.ServiceException;

/**
 * Servlet implementation class {@code WebController}
 */
@WebServlet(name = "WebController", urlPatterns = { "/do" })
public class WebController extends HttpServlet {

	private static final long serialVersionUID = -7595854567264217769L;
	private static final String FORWARD = "forward";
	protected static final Logger logger = LoggerFactory.getLogger(WebController.class);

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws java.io.IOException
	 *
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		try {
			Command command = CommandFactory.getCommand(request);
			String forward = command.service(request, response);
			request.setAttribute(FORWARD, forward);
		} catch (ServiceException ex) {
			logger.error(ex.getMessage());
		}
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

}
