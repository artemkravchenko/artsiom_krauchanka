package com.epam.newsmanagement.utils;

/**
 * {@code PropertyUtils} class is util class that contains String representation
 * of some project properties.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class PropertyUtils {
	public static final int NEWS_ON_PAGE = 3;
	public static final int NAV_BUTTONS_ON_PAGE = 7;

	public static final String JSP_REDIRECT_NEWS_PAGE = "redirect:do?action=NEWS_PAGE&newsId=";
	public static final String JSP_NEWS_PAGE = "/WEB-INF/views/pages/news-page.jsp";
	public static final String JSP_SHOW_NEWS = "/WEB-INF/views/pages/show-news.jsp";
	public static final String JSP_ERROR_404 = "/WEB-INF/views/pages/error404.jsp";
	public static final String JSP_ERROR_500 = "/WEB-INF/views/pages/error500.jsp";
}
