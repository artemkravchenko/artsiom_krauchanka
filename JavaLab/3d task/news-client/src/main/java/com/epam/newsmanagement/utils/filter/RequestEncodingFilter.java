package com.epam.newsmanagement.utils.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 
 * {@code RequestEncodingFilter} class is a {@code Filter} interface
 * implementation. It sets request encoding to "UTF-8".
 * 
 * @author Artsiom_Krauchanka
 * @see javax.servlet.Filter
 * @see javax.servlet.FilterChain
 * @see javax.servlet.FilterConfig
 */
public class RequestEncodingFilter implements Filter {
	private static final String UTF8_ENCODING = "UTF-8";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		request.setCharacterEncoding(UTF8_ENCODING);
		chain.doFilter(request, response);
	}
}
