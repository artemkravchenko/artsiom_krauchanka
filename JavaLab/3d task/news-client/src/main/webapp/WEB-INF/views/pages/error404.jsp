<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="resources/css/clientstyle.css" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,700,300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
	<title>
		page not found
	</title>
	
	<fmt:setLocale value="${sessionScope.locale}" />
	<fmt:setBundle basename="Resources" />
</head>

<body>
	<div class="error-page">
		<h1 style="font-size:250px;">404</h1>
		<h3>
			<fmt:message key="messages.error" />
		</h3>
		<a href="do?action=SHOW_NEWS">
			<fmt:message key="messages.error.link" />
		</a>
	</div>
</body>
</html>