<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resources"/>		

<div class="header">
	<div class="header-topic">
		<h1>
			<fmt:message key="messages.title"/>
		</h1>
	</div>
	
	<div class="header-info">
		<div class="localization">
   			
    		<c:set var="parameters" value="${requestScope['javax.servlet.forward.query_string']}"/>
   			
   			<c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], '&locale=RU')}">
      			<c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], '&locale=RU', '')}"/>
    		</c:if>
   			
   			<c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], '&locale=EN')}">
      			<c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], '&locale=EN', '')}"/>
    		</c:if>
    
    		
        	<a id="en" href="do?${parameters}&locale=EN"><fmt:message key="messages.lang.en"/></a>
        	<a id="ru" href="do?${parameters}&locale=RU"><fmt:message key="messages.lang.ru"/></a>
     	</div>
	</div>
</div>