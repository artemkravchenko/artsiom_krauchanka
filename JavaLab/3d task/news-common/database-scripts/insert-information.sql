/* Insert data to the AUTHOR table  */
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (1,'Ford',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (2,'Andrew',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (3,'Garry',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (4,'Hudson',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (5,'Leon',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (6,'Frank',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (7,'James',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (8,'Willis',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (9,'Carry',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (10,'Martin',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (11,'Cooper',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (12,'Armstrong',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (13,'Arnold',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (14,'Richard',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (15,'Arthur',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (16,'Eddy',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (17,'Michael',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (18,'Theodor',null);
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (19,'Peter',to_timestamp('17-NOV-15 09.44.24.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (20,'Ray',null);


/*  Insert data to the NEWS table  */
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (111,'123123','123123','123123',to_timestamp('15-JAN-16 05.55.15.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('15-JAN-16 06.07.14.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (40,'''Carbonite'' space imager revealed','UK small satellite manufacturer SSTL releases details of a new spacecraft it launched in July that takes 1m resolution video of the Earth''s surface.','												UK small satellite manufacturer SSTL has released details of the spacecraft it launched in July, but about which it gave few details at the time.
Codenamed Carbonite, the 80kg platform filled some unused mass on the rocket that put up the company''s new high-resolution imaging constellation.
It turns out the additional passenger was a demonstrator for a new type of quick-build, ultra-low-cost satellite.
Carbonite uses an off-the-shelf camera and telescope to take videos of Earth.
It acquires still pictures as well. Both modes show features on the ground down to a size of 1.5m.
However, if flown in a 500km-high orbit, this would be a 1m ground-resolution.
Carbonite''s mission was revealed at last week''s World Satellite Business Week conference in Paris, organised by Euroconsult.
The intention is to compete the platform in the emerging Earth-observation market for daily, fast-turn-around imagery.
This is a market being targeted currently by a number of Silicon Valley operations, including Skybox-Google and Planet Labs, and requires the use of multiple satellites in orbit.
			
			
			',to_timestamp('22-SEP-15 05.34.30.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('28-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (41,'''Universal urination duration'' wins Ig Nobel prize','A study showing that nearly all mammals take the same amount of time to urinate has been awarded one of the 2015 Ig Nobel prizes at Harvard University.','A study showing that nearly all mammals take the same amount of time to urinate has been awarded one of the 2015 Ig Nobel prizes at Harvard University.
These spoof Nobels for "improbable research" are in their 25th year.
The team behind the urination research, from Georgia Tech, won the physics Ig.
Using high-speed video analysis, they modelled the fluid dynamics involved in urination and discovered that all mammals weighing more than 3kg empty their bladders over about 21 seconds.
Their subjects included rats, goats, cows and elephants - and although the findings reveal a remarkably consistent "scaling law" in bigger beasts, they also emphasise that small animals do things quite differently.
Rats can urinate in a fraction of a second, for example. This might make rodents a poor choice for studying urinary health problems.
"We don''t have a proper animal model for urinary system research," said the study''s lead author Patricia Yang, a PhD student in mechanical engineering.',to_timestamp('22-SEP-15 05.33.33.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (42,'Apple''s App store infected in China.','Apple has said it is taking steps to remove malicious code added to a number of apps commonly used on iPhones and iPads in China.','Apple has said it is taking steps to remove malicious code added to a number of apps commonly used on iPhones and iPads in China.
It is thought to be the first large-scale attack on Apple''s App Store.
The hackers created a counterfeit version of Apple''s software for building iOS apps, which they persuaded developers to download.
Apps compiled using the tool allow the attackers to steal data about users and send it to servers they control.
Cybersecurity firm Palo Alto Networks - which has analysed the malware dubbed XcodeGhost - said the perpetrators would also be able to send fake alerts to infected devices to trick their owners into revealing information.
It added they could also read and alter information in compromised devices'' clipboards, which would potentially allow them to see logins copied to and from password management tools.',to_timestamp('22-SEP-15 05.35.26.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (43,'Pakistan delays hanging paraplegic man','The execution of a paraplegic prisoner in Pakistan is delayed, his lawyer says, because he could not be hanged in compliance with prison guidelines.','The planned hanging of a paraplegic prisoner in Pakistan has been delayed, his lawyer has said.
Abdul Basit could not be hanged in compliance with the jail manual because he is in a wheelchair, a magistrate said when ordering the postponement.
Pakistan''s prison guidelines require that a prisoner stand on the gallows.
Rights groups say hanging Basit would constitute cruel and degrading treatment, and that there is a risk of the hanging going wrong.
Abdul Basit, 43, is paralysed from the waist down and uses a wheelchair after becoming ill in prison.
Pakistan reintroduced the death penalty in December 2014 and has hanged 239 people since.
At the time, the government said it was a measure to combat terrorism after the Taliban massacred more than 150 people, most of them children, in a Peshawar school.',to_timestamp('22-SEP-15 05.37.17.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (44,'Gunmen kidnap tourists in Philippines','Two Canadians, a Norwegian and a local woman have been kidnapped from a tourist resort in the southern Philippines, the military has said!','Two Canadians, a Norwegian and a local woman have been kidnapped from a tourist resort in the southern Philippines, the military has said.
An army spokesman said they were taken by gunmen late on Monday from the Holiday Oceanview resort on Samal Island, near Davao City on Mindanao.
The attackers, who have not been identified by police, left by boat, Capt Alberto Caber told reporters.
He said it appeared the four were targeted rather than taken at random.
Since the 1990s the southern Philippines has seen sporadic incidents of kidnapping by Muslim militant groups, who hold hostages for ransom.
Foreigners named
Philippine authorities have named the Canadian abductees as John Ridsel and Robert Hall.
The Norwegian, Kjartan Sekkingstad, was said to be the manager of the resort.
			',to_timestamp('22-SEP-15 06.53.24.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('16-NOV-15 10.28.23.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (45,'Volkswagen US boss says ''we screwed up''','The boss of Volkswagen''s US business admits the company was dishonest in using software to rig emissions tests.','The boss of Volkswagen''s US business has admitted the firm was dishonest in using software to rig emissions tests.
Michael Horn said the firm was dishonest with US regulators, adding: "We have totally screwed up."
Last Friday, the regulators said VW diesel cars had much higher emissions than tests had suggested.
French Finance Minister Michel Sapin has called for an EU inquiry, but a UK car industry spokesman said there was "no evidence" of cheating.
Mike Hawes, who is chief executive of the UK''s Society of Motor Manufacturers and Traders, said the EU operated a "fundamentally different system" from the US, with tests performed in strict conditions and witnessed by a government-appointed independent approval agency.
"There is no evidence that manufacturers cheat the cycle," he said. "Vehicles are removed from the production line randomly and must be standard production models, certified by the relevant authority - the UK body being the Vehicle Certification Agency, which is responsible to the Department for Transport."
However, he also described current testing methods as "outdated" and said the car industry wanted an updated emissions test, "more representative of on-road conditions".',to_timestamp('22-SEP-15 06.55.58.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (34,'Day wins in Chicago to take number one slot','Jason Day replaces Rory McIlroy as world number one after winning the BMW Championship by six shots in Chicago.','
                
                                    
           Jason Day replaced Rory McIlroy as world number one after winning the BMW Championship by six shots in Chicago.
The Australian, 27, finished with a birdie as he carded a two-under 69 to triumph on 22 under par.
Daniel Berger of the United States was second after a two-under 69, one ahead of compatriot Scott Piercy (70).
Northern Ireland''s McIlroy finished with a 70 to tie for fourth on 14 under with Americans JB Holmes and Rickie Fowler, eight shots adrift of Day.
Day admitted to nerves ahead of his bid to go to the top of the world rankings, saying: "To be honest I had terrible sleep over the last few nights.         ',to_timestamp('21-SEP-15 09.25.32.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('21-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (35,'EU divided on migrants as talks due','Foreign ministers from eastern European countries gather for talks on migrants amid stark divisions in the EU over the continuing influx.','Foreign ministers from four eastern European countries are preparing to meet to discuss the migrant influx amid continuing stark divisions.
The meeting, in Prague, comes as thousands continue to cross the EU''s southern borders, and begins a week of intense diplomatic activity.
Poland, Hungary, the Czech Republic and Slovakia remain strongly opposed to accepting obligatory quotas.
Germany and France want migrants shared out more evenly across the EU.
Thousands more migrants entered Austria over the weekend and more are expected to arrive via Hungary on Monday.
Croatia''s authorities tweeted that 29,000 had entered its territory by early Monday morning.


',to_timestamp('21-SEP-15 12.27.52.000000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('18-NOV-15 11.57.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (39,'CBI warning over renewables subsidies','The government’s wholesale cuts to renewable energy subsidies are sending a worrying sign to investors, says business group the CBI.','The government’s wholesale cuts to renewable energy subsidies are sending a worrying sign to investors, says employers'' group, the CBI.The head of the group, John Cridland, said firms must be given confidence that ministers really mean to tackle climate change.The government cut subsidies in the summer because the ?7.6bn budget had been exceeded.Ministers say they are committed to protecting the climate.They said they would announce replacement renewables policies soon.Critics say new policies can’t come quickly enough. Mr Cridland said: ',to_timestamp('22-SEP-15 04.52.57.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (37,'Burkina Faso army reaches capital','Burkina Faso''s army reaches the capital Ouagadougou to seek the surrender of the presidential guard who staged a coup last Thursday','Burkina Faso''s army has reached the capital, Ouagadougou, to seek the surrender of the elite presidential guard who staged a coup last Thursday.
Negotiations between army chiefs and the presidential guard are under way, security sources said.
Both the president and the prime minister have been released.
Coup leader Gen Gilbert Diendere says he will step aside once regional leaders endorse a plan including an amnesty for the coup plotters.
Prime Minister Isaac Zida was seized by the presidential guard in last week''s coup.
France''s ambassador to Burkina Faso, Gilles Thibault, has also tweeted that interim President Michel Kafando, who was arrested last Wednesday, has been released from house arrest and is now at the ambassador''s residence.
Gen Diendere seems to be backed into a corner by international and national pressure, and on Monday thousands gathered to celebrate what they believed was his defeat, says the BBC''s Maud Jullien in Ouagadougou.',to_timestamp('22-SEP-15 04.47.56.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into ARTSIOM_KRAUCHANKA.NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (38,'Scientists make ''lab-grown'' kidney','Scientists say they are a step closer to growing fully functioning replacement kidneys, after promising results in animals.','Scientists say they are a step closer to growing fully functioning replacement kidneys, after promising results in animals.
When transplanted into pigs and rats, the kidneys worked, passing urine just like natural ones.
Getting the urine out has been a problem for earlier prototypes, causing them to balloon under the pressure.
The Japanese team got round this by growing extra plumbing for the kidney to stop the backlog, PNAS reports.
Although still years off human trials, the research helps guide the way towards the end goal of making organs for people, say experts.
In the UK, more than 6,000 people are waiting for a kidney - but because of a shortage of donors, fewer than 3,000 transplants are carried out each year.
More than 350 people die a year, almost one a day, waiting for a transplant.
Lab-grown kidneys using human stem cells could solve this problem.',to_timestamp('22-SEP-15 04.50.41.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('22-SEP-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));

/* Insert data in NEWS_AUTHOR table */
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (34,7);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (35,5);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (37,16);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (38,5);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (39,7);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (40,14);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (41,10);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (42,9);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (43,4);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (44,5);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (45,8);
Insert into ARTSIOM_KRAUCHANKA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (111,3);


/*Insert data in COMMENTS table */
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (52,'Wow, its a great news!',to_timestamp('22-SEP-15 05.42.58.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),38);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (292,'you',to_timestamp('25-JAN-16 11.19.25.563000000 AM','DD-MON-RR HH.MI.SSXFF AM'),111);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (274,'good news
',to_timestamp('23-NOV-15 07.29.18.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),35);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (55,'Very interesting article!
',to_timestamp('22-SEP-15 06.09.28.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),40);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (293,':(',to_timestamp('27-JAN-16 07.27.06.821000000 AM','DD-MON-RR HH.MI.SSXFF AM'),44);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (58,'wow, its a good news!',to_timestamp('28-SEP-15 06.57.56.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),34);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (59,'wow, its a great news!
',to_timestamp('29-SEP-15 03.55.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),41);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (60,'wow, its a great news!					',to_timestamp('29-SEP-15 05.24.22.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),40);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (63,'wow
',to_timestamp('05-OCT-15 04.01.09.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),44);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (66,'horrorable thing
',to_timestamp('09-OCT-15 06.46.48.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),44);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (283,'another test',to_timestamp('21-JAN-16 11.25.39.038000000 AM','DD-MON-RR HH.MI.SSXFF AM'),111);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (284,'hey',to_timestamp('21-JAN-16 01.42.56.632000000 PM','DD-MON-RR HH.MI.SSXFF AM'),35);
Insert into ARTSIOM_KRAUCHANKA.COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (291,'test hibernate comment!',to_timestamp('25-JAN-16 11.11.34.994000000 AM','DD-MON-RR HH.MI.SSXFF AM'),null);

/* Insert data in TAG table */
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (52,'Europe');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (53,'Technology');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (54,'Art');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (55,'Business');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (56,'Entertainment');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (57,'Health');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (58,'World');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (59,'USA');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (60,'Asia');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (61,'Middle East');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (62,'Sport');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (63,'Football');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (64,'Golf');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (65,'Volleyball');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (66,'Rugby');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (67,'Science');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (68,'UK');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (69,'Africa');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (70,'Tennis');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (71,'Environment');
Insert into ARTSIOM_KRAUCHANKA.TAG (TAG_ID,TAG_NAME) values (87,'test');


/*  Insert data in NEWS_TAG table  */
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (34,62);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (34,64);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (35,52);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (37,58);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (37,69);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (38,57);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (38,67);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (39,67);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (39,71);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (40,53);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (40,67);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (40,71);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (41,53);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (41,67);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (42,53);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (43,58);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (43,60);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (44,60);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (45,55);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (111,53);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (111,54);
Insert into ARTSIOM_KRAUCHANKA.NEWS_TAG (NEWS_ID,TAG_ID) values (111,69);




Insert into ARTSIOM_KRAUCHANKA.USERS (USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD) values (1,'artem kravchenko','smoke','fdfa02ecf86feac3801254da57c1c9ba');
Insert into ARTSIOM_KRAUCHANKA.USERS (USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD) values (2,'test user','user','fdfa02ecf86feac3801254da57c1c9ba');




Insert into ARTSIOM_KRAUCHANKA.ROLES (USER_ID,ROLE_NAME) values (1,'ROLE_ADMIN');
Insert into ARTSIOM_KRAUCHANKA.ROLES (USER_ID,ROLE_NAME) values (2,'ROLE_USER');


