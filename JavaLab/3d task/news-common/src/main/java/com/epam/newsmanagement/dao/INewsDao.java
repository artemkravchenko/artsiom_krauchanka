package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;

/**
 * {@code INewsDao} interface extends {@code IGenericDao} interface and provides
 * additional methods for {@code News} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanegement.dao.IGenericDao
 *
 */
public interface INewsDao extends IGenericDao<News, Long> {
	/**
	 * Return next {@code News} instance ID according search filter.
	 * 
	 * @param newsId
	 *            current {@code News} instance ID.
	 * @param searchFilter
	 *            news filter.
	 * @return {@code News} instance ID.
	 * @throws DaoException
	 */
	public Long next(Long newsId, SearchFilter searchFilter) throws DaoException;

	/**
	 * return previous {@code News} instance ID according search filter.
	 * 
	 * @param newsId
	 *            current {@code News} instance ID.
	 * @param searchFilter
	 *            news filter.
	 * @return {@code News} instance ID.
	 * @throws DaoException
	 */
	public Long previous(Long newsId, SearchFilter searchFilter) throws DaoException;

	/**
	 * Counts {@code News } instances in the DB according by search filter.
	 * 
	 * @return count of {@code News} filter instances.
	 * @throws DaoException
	 */
	public int getCountOfFilterNews(SearchFilter searchFilter) throws DaoException;

	/**
	 * Read all records according searchFilter if exists from DB in the range
	 * [beginIndex, endIndex] order by modification date and count of comments.
	 * 
	 * @param beginIndex
	 * @param endIndex
	 * @param searchFilter
	 *            {@code SearchFilter} object.
	 * @return
	 * @throws DaoException
	 */
	public List<News> readAll(int beginIndex, int endIndex, SearchFilter searchFilter) throws DaoException;

	/**
	 * Search {@code News} instances according given {@code Tag} instance.
	 *
	 * @param tagId
	 *            {@code Tag} instance ID.
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> searchNewsByTag(Long tagId) throws DaoException;

	/**
	 * Search {@code News} instances according given {@code Author} instance.
	 *
	 * @param authorId
	 *            {@code Author} instance ID.
	 * @return list of {@code News} instances.
	 * @throws DaoException
	 */
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException;

	/**
	 * Associate {@code News} and {@code Author} instance in the DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @param authorId
	 *            {@code Author} instance ID.
	 * @throws DaoException
	 */
	public void associateNewsAuthor(Long newsId, Long authorId) throws DaoException;

	/**
	 * Associate {@code News} and {@code Tag} instance in the DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @param tagId
	 *            {@code Tag} instance ID.
	 *
	 * @throws DaoException
	 */
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException;

	/**
	 * Associate {@code News} instance and list of {@code Tag} instances in the
	 * DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @param tagList
	 *            list of {@code Tag} instances.
	 *
	 * @throws DaoException
	 */
	public void associateNewsTagList(Long newsId, List<Tag> tagList) throws DaoException;

}
