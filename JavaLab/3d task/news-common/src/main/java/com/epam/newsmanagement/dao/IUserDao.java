package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;

/**
 * {@code IUserDao} interface describes methods for {@code User} object
 * processing.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public interface IUserDao {
	/**
	 * Loads user-specific data
	 * 
	 * @param name
	 *            user name.
	 * @return {@code User} object according to specific name or @{code null} if
	 *         user does not exist.
	 * @throws DaoException
	 */
	public User loadUserByUsername(String name) throws DaoException;
}
