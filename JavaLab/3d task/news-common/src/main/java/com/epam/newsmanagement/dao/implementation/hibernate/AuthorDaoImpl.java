package com.epam.newsmanagement.dao.implementation.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utils.HibernateUtils;

/**
 * {@code AuthorDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code IAuthorDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IAuthorDao
 * @see com.epam.newsmanagement.entity.Author
 *
 */
public class AuthorDaoImpl implements IAuthorDao {

	private static final String PARAM_AUTHOR_NAME = "name";
	private static final String PARAM_NEWS_ID = "newsId";

	private static final String HQL_READ_AUTHOR_BY_NEWS = "SELECT author FROM Author author JOIN author.newsSet news WHERE news.id = :newsId";

	@Autowired
	private HibernateUtils hibernateUtils;

	@Override
	public Long create(Author entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Long key = (Long) session.save(entity);
			session.getTransaction().commit();
			return key;

		} catch (HibernateException ex) {
			throw new DaoException("Error while creating new Author instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public Author read(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Author author = (Author) session.get(Author.class, entityId);
			return author;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Author instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readAll() throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			List<Author> authorList = session.createCriteria(Author.class).list();
			return authorList;
		} catch (HibernateException ex) {
			throw new DaoException("error while reading all Author instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void update(Author entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			session.update(entity);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while updating Author instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Author author = (Author) session.get(Author.class, entityId);
			session.delete(author);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Comment instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public Author read(String authorName) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Criteria criteria = session.createCriteria(Author.class);
			criteria.add(Restrictions.eq(PARAM_AUTHOR_NAME, authorName));
			return (Author) criteria.uniqueResult();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Author instances by name. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public Author searchAuthorByNews(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Query query = session.createQuery(HQL_READ_AUTHOR_BY_NEWS);
			query.setLong(PARAM_NEWS_ID, newsId);
			Author author = (Author) query.uniqueResult();
			return author;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Author instances by News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteAuthorLinks(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Query query = session.createQuery(HQL_READ_AUTHOR_BY_NEWS);
			query.setLong(PARAM_NEWS_ID, newsId);
			List<Author> authorList = query.list();
			News news = (News) session.get(News.class, newsId);
			for (Author author : authorList) {
				author.getNewsSet().remove(news);
			}
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Author links with News. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void setExpired(Long authorId) throws DaoException {
		throw new UnsupportedOperationException("Not implemented yet.");
	}
}
