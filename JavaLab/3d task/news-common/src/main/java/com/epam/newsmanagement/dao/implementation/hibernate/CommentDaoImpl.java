package com.epam.newsmanagement.dao.implementation.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utils.HibernateUtils;

/**
 * {@code CommentDaoImpl} class is a part of Data Access layer. Implementation
 * of {@code ICommentDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.ICommentDao
 * @see com.epam.newsmanagement.entity.Comment
 */

public class CommentDaoImpl implements ICommentDao {

	private static final String PARAM_NEWS_ID = "newsId";
	private static final String HQL_READ_COMMENTS_BY_NEWS = "SELECT comment FROM Comment comment WHERE comment.news.id = :newsId ORDER BY creationDate DESC";

	@Autowired
	private HibernateUtils hibernateUtils;

	@Override
	public Long create(Comment entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Long key = (Long) session.save(entity);
			session.getTransaction().commit();
			return key;

		} catch (HibernateException ex) {
			throw new DaoException("error while creating new Comment instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public Comment read(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Comment comment = (Comment) session.get(Comment.class, entityId);
			return comment;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Comment instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readAll() throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			List<Comment> commentList = session.createCriteria(Comment.class).list();
			return commentList;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading all Comment instances. See Details: ", ex);
		} finally {
			session = hibernateUtils.getSession();
		}
	}

	@Override
	public void update(Comment entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			session.update(entity);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while updating Comment instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Comment comment = (Comment) session.get(Comment.class, entityId);
			session.delete(comment);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Comment instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Query query = session.createQuery(HQL_READ_COMMENTS_BY_NEWS);
			query.setLong(PARAM_NEWS_ID, newsId);
			return query.list();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Comment instances by news. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void deleteCommentsByNews(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, newsId);
			news.getCommentSet().clear();
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Comment instances by news. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}
}
