package com.epam.newsmanagement.dao.implementation.hibernate;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.HibernateUtils;

/**
 * {@code NewsDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code INewsDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.INewsDao
 * @see com.epam.newsmanagement.entity.News
 */
public class NewsDaoImpl implements INewsDao {

	private static final String PARAM_AUTHOR_IDS = "authorIds";
	private static final String PARAM_TAG_IDS = "tagIDs";
	private static final String PARAM_TAG_ID = "tagId";
	private static final String PARAM_AUTHOR_ID = "authorId";

	private static final String HQL_READ_NEWS_BY_TAG = "SELECT news FROM News news JOIN news.tagSet tag WHERE tag.id = :tagId";
	private static final String HQL_READ_NEWS_BY_AUTHOR = "SELECT news FROM News news JOIN news.authorSet author WHERE author.id = :authorId";

	private static final String COUNTFILTER_BASE_SELECT = " SELECT COUNT(*) FROM News news ";
	private static final String FILTER_BASE_SELECT = " SELECT news FROM News news ";
	private static final String FILTER_JOIN_AUTHOR = " JOIN news.authorSet author ";
	private static final String FILTER_JOIN_TAG = " JOIN news.tagSet tag ";
	private static final String FILTER_JOIN_COMMENT = " LEFT OUTER JOIN news.commentSet comment ";
	private static final String FILTER_WHERE_CLAUSE = " WHERE ";
	private static final String FILTER_AND_CLAUSE = " AND ";
	private static final String FILTER_GROUP_BY = " GROUP BY news.id, news.title, news.shortText, news.fullText, news.creationDate, news.modificationDate ";
	private static final String FILTER_ORDER_BY = " ORDER BY news.modificationDate DESC, COUNT(comment) ";
	private static final String FILTER_PARAM_AUTHOR_ID = " author.id IN :authorIds ";
	private static final String FILTER_PARAM_TAG_ID = "  tag.id IN :tagIDs  ";

	@Autowired
	private HibernateUtils hibernateUtils;

	@Override
	public Long create(News entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Long key = (Long) session.save(entity);
			session.getTransaction().commit();
			return key;

		} catch (HibernateException ex) {
			throw new DaoException("error while creating new News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public News read(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			News news = (News) session.get(News.class, entityId);
			return news;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll() throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Criteria criteria = session.createCriteria(News.class);
			List<News> newsList = criteria.list();
			return newsList;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading all News instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void update(News entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			session.update(entity);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while updating News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, entityId);
			session.delete(news);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long next(Long newsId, SearchFilter searchFilter) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();

			String queryString = buildSearchQuery(searchFilter);
			Query query = session.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				query.setParameterList(PARAM_AUTHOR_IDS, new Long[] { searchFilter.getAuthorId() });
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameterList(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}

			Iterator<News> it = query.list().iterator();
			Long next = 0l;
			while (it.hasNext()) {
				News news = (News) it.next();
				if (news.getId().equals(newsId)) {
					next = (it.hasNext()) ? (Long) it.next().getId() : 0l;
					break;
				}
			}
			return next;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading next News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long previous(Long newsId, SearchFilter searchFilter) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();

			String queryString = buildSearchQuery(searchFilter);
			Query query = session.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				query.setParameterList(PARAM_AUTHOR_IDS, new Long[] { searchFilter.getAuthorId() });
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameterList(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}

			ListIterator<News> it = query.list().listIterator(query.list().size());
			Long prev = 0l;

			while (it.hasPrevious()) {
				News news = (News) it.previous();
				if (news.getId().equals(newsId)) {
					prev = (it.hasPrevious()) ? (Long) it.previous().getId() : 0l;
					break;
				}
			}
			return prev;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading previous News instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public int getCountOfFilterNews(SearchFilter searchFilter) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();

			String queryString = buildCountSearchQuery(searchFilter);
			Query query = session.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				query.setParameterList(PARAM_AUTHOR_IDS, new Long[] { searchFilter.getAuthorId() });
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameterList(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}
			Long result = (Long) query.uniqueResult();
			return result.intValue();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading News instances count. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll(int beginIndex, int endIndex, SearchFilter searchFilter) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			String queryString = buildSearchQuery(searchFilter);
			Query query = session.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				query.setParameterList(PARAM_AUTHOR_IDS, new Long[] { searchFilter.getAuthorId() });
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameterList(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}
			query.setFirstResult(beginIndex - 1).setMaxResults(endIndex - beginIndex + 1);
			return query.list();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading News instances in range. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNewsByTag(Long tagId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Query query = session.createQuery(HQL_READ_NEWS_BY_TAG);
			query.setLong(PARAM_TAG_ID, tagId);
			List<News> newsList = (List<News>) query.list();
			return newsList;

		} catch (HibernateException ex) {
			throw new DaoException("error while searching News instances by Tag. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Query query = session.createQuery(HQL_READ_NEWS_BY_AUTHOR);
			query.setLong(PARAM_AUTHOR_ID, authorId);
			List<News> newsList = (List<News>) query.list();
			return newsList;

		} catch (HibernateException ex) {
			throw new DaoException("error while searching News instances by Author. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void associateNewsAuthor(Long newsId, Long authorId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, newsId);
			Author author = (Author) session.get(Author.class, authorId);
			news.getAuthorSet().add(author);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while assosiating News and Author instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, newsId);
			Tag tag = (Tag) session.get(Tag.class, tagId);
			news.getTagSet().add(tag);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while assosiating News and Tag instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void associateNewsTagList(Long newsId, List<Tag> tagList) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, newsId);
			for (Tag tag : tagList) {
				news.getTagSet().add(tag);
			}
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while assosiating News and Tag instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	private String buildSearchQuery(SearchFilter filter) {
		StringBuilder builder = new StringBuilder(FILTER_BASE_SELECT);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = filter == null;

		if (!isFilter && filter.getAuthorId() != null) {
			builder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && filter.getTagIdList() != null && !filter.getTagIdList().isEmpty()) {
			builder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		builder.append(FILTER_JOIN_COMMENT);

		if (checkAuthor || checkTagList) {
			builder.append(FILTER_WHERE_CLAUSE);
		}

		if (checkAuthor) {
			builder.append(FILTER_PARAM_AUTHOR_ID);

		}

		if (checkAuthor && checkTagList) {
			builder.append(FILTER_AND_CLAUSE);

		}

		if (checkTagList) {
			builder.append(FILTER_PARAM_TAG_ID);
		}

		builder.append(FILTER_GROUP_BY);
		builder.append(FILTER_ORDER_BY);

		return builder.toString();
	}

	private String buildCountSearchQuery(SearchFilter filter) {
		StringBuilder builder = new StringBuilder(COUNTFILTER_BASE_SELECT);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = filter == null;

		if (!isFilter && filter.getAuthorId() != null) {
			builder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && filter.getTagIdList() != null && !filter.getTagIdList().isEmpty()) {
			builder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		if (checkAuthor || checkTagList) {
			builder.append(FILTER_WHERE_CLAUSE);
		}

		if (checkAuthor) {
			builder.append(FILTER_PARAM_AUTHOR_ID);

		}

		if (checkAuthor && checkTagList) {
			builder.append(FILTER_AND_CLAUSE);

		}

		if (checkTagList) {
			builder.append(FILTER_PARAM_TAG_ID);
		}

		return builder.toString();
	}
}
