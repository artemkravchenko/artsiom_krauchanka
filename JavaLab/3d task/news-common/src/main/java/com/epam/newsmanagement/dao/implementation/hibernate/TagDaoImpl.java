package com.epam.newsmanagement.dao.implementation.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.HibernateUtils;

/**
 * {@code TagDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code ITagDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.ITagDao
 * @see com.epam.newsmanagement.entity.Tag
 */

public class TagDaoImpl implements ITagDao {

	private static final String PARAM_TAG_NAME = "name";
	private static final String PARAM_NEWS_ID = "newsId";

	private static final String HQL_READ_TAG_BY_NEWS = "SELECT tag FROM Tag tag JOIN tag.newsSet news WHERE news.id = :newsId";

	@Autowired
	private HibernateUtils hibernateUtils;

	@Override
	public Long create(Tag entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Long key = (Long) session.save(entity);
			session.getTransaction().commit();
			return key;

		} catch (HibernateException ex) {
			throw new DaoException("error while creating new Tag instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public Tag read(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Tag tag = (Tag) session.get(Tag.class, entityId);
			return tag;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Tag instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readAll() throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Criteria criteria = session.createCriteria(Tag.class);
			List<Tag> tagList = criteria.list();
			return tagList;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading all Tag instances. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void update(Tag entity) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			session.update(entity);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while updating Tag instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Tag tag = (Tag) session.get(Tag.class, entityId);
			session.delete(tag);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Tag instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}

	}

	@Override
	public Tag read(String tagName) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Criteria criteria = session.createCriteria(Tag.class);
			criteria.add(Restrictions.eq(PARAM_TAG_NAME, tagName));
			return (Tag) criteria.uniqueResult();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Tag instances by name. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Query query = session.createQuery(HQL_READ_TAG_BY_NEWS);
			query.setLong(PARAM_NEWS_ID, newsId);
			List<Tag> tagList = (List<Tag>) query.list();
			return tagList;

		} catch (HibernateException ex) {
			throw new DaoException("error while reading Tag instances by news instance. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@Override
	public void deleteLinks(Long tagId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Tag tag = (Tag) session.get(Tag.class, tagId);
			tag.getNewsSet().clear();
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Tag links with News. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteTagLinks(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			session.beginTransaction();
			Query query = session.createQuery(HQL_READ_TAG_BY_NEWS);
			query.setLong(PARAM_NEWS_ID, newsId);
			List<Tag> tagList = query.list();
			News news = (News) session.get(News.class, newsId);
			for (Tag tag : tagList) {
				tag.getNewsSet().remove(news);
			}
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			throw new DaoException("error while deleting Tag links with News. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}
}
