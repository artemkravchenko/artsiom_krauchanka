package com.epam.newsmanagement.dao.implementation.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IUserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.utils.HibernateUtils;

/**
 * {@code IUserDao} interface implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IUserDao
 */

public class UserDaoImpl implements IUserDao {

	private static final String PARAM_USER_LOGIN = "login";

	@Autowired
	private HibernateUtils hibernateUtils;

	@Override
	public User loadUserByUsername(String name) throws DaoException {
		Session session = null;
		try {
			session = hibernateUtils.getSession();
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq(PARAM_USER_LOGIN, name));
			return (User) criteria.uniqueResult();

		} catch (HibernateException ex) {
			throw new DaoException("error while reading User instance by login. See Details: ", ex);
		} finally {
			hibernateUtils.closeSession(session);
		}
	}

}
