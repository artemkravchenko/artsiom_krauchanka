package com.epam.newsmanagement.dao.implementation.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.utils.DatabaseUtils;

/**
 * {@code AuthorDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code IAuthorDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IAuthorDao
 * @see com.epam.newsmanagement.entity.Author
 *
 */
public class AuthorDaoImpl implements IAuthorDao {

	private static final String TIME_FORMAT = "GMT";
	/* SQL queries */
	private static final String SQL_CREATE = "INSERT INTO AUTHOR (AUTHOR_ID, " + "AUTHOR_NAME) VALUES (AUTHOR_ID_SEQ.NEXTVAL, ?)";
	private static final String SQL_READ = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_READ_BY_NAME = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_NAME = ?";
	private static final String SQL_READALL = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR ORDER BY AUTHOR_ID";
	private static final String SQL_UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? " + "WHERE AUTHOR_ID = ?";
	private static final String SQL_SET_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_AUTHOR_LINK = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private static final String SQL_SEARCH_BY_NEWS = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED FROM AUTHOR JOIN NEWS_AUTHOR ON "
			+ "AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID = ?";

	/* DB fields */
	private static final String AUTHOR_ID = "AUTHOR_ID";
	private static final String AUTHOR_NAME = "AUTHOR_NAME";
	private static final String AUTHOR_EXPIRED = "EXPIRED";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(Author author) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Long authorId = null;
		String generatedColumns[] = { AUTHOR_ID };

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE, generatedColumns);

			preparedStatement.setString(1, author.getName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			}
			return authorId;
		} catch (SQLException ex) {
			throw new DaoException("Error while creating author instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public Author read(Long authorId) throws DaoException {
		Author author = null;
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong(AUTHOR_ID));
				author.setName(resultSet.getString(AUTHOR_NAME));
				author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));
			}

			return author;
		} catch (SQLException ex) {
			throw new DaoException("Error while reading author instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public Author read(String authorName) throws DaoException {
		Author author = null;
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_BY_NAME);
			preparedStatement.setString(1, authorName);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong(AUTHOR_ID));
				author.setName(resultSet.getString(AUTHOR_NAME));
				author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));
			}

			return author;
		} catch (SQLException ex) {
			throw new DaoException("Error while reading author instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public List<Author> readAll() throws DaoException {
		List<Author> authorList = new ArrayList<>();
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READALL);

			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(AUTHOR_ID));
				author.setName(resultSet.getString(AUTHOR_NAME));
				author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));

				authorList.add(author);
			}
			return authorList;
		} catch (SQLException ex) {
			throw new DaoException("Error while reading all author instances. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, author.getName());

			if (author.getExpired() != null) {
				preparedStatement.setTimestamp(2, new Timestamp(author.getExpired().getTime()), Calendar.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));
			} else {
				preparedStatement.setNull(2, Types.NULL);
			}

			preparedStatement.setLong(3, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Error while updating author instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		throw new UnsupportedOperationException("unsupported operation yet.");
	}

	@Override
	public void deleteAuthorLinks(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR_LINK);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException("Error while deleting author links. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public void setExpired(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED);

			preparedStatement.setTimestamp(1, new Timestamp(System.currentTimeMillis()), Calendar.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));

			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Error while updating author instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public Author searchAuthorByNews(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Author author = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS);

			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				author = new Author();

				author.setId(resultSet.getLong(AUTHOR_ID));
				author.setName(resultSet.getString(AUTHOR_NAME));
				author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));
			}

			return author;
		} catch (SQLException ex) {
			throw new DaoException("Error while searching author instance by news. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement, connection);
		}
	}

}
