package com.epam.newsmanagement.dao.implementation.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.utils.DatabaseUtils;

/**
 * {@code CommentDaoImpl} class is a part of Data Access layer. Implementation
 * of {@code ICommentDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.ICommentDao
 * @see com.epam.newsmanagement.entity.Comment
 */
public class CommentDaoImpl implements ICommentDao {

	private static final String TIME_FORMAT = "GMT";
	/* SQL queries */
	private static final String SQL_CREATE = "INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID) "
			+ "VALUES (COMMENT_ID_SEQ.NEXTVAL, ?,?,?)";
	private static final String SQL_READ = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_READALL = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS ORDER BY COMMENT_ID";
	private static final String SQL_UPDATE = "UPDATE COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ?, "
			+ "NEWS_ID = ?  WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE_BY_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private static final String SQL_SEARCH_BY_NEWS = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID "
			+ "FROM COMMENTS WHERE NEWS_ID = ? ORDER BY (CREATION_DATE) DESC";

	/* DB fields */
	private static final String COMMENT_ID = "COMMENT_ID";
	private static final String COMMENT_TEXT = "COMMENT_TEXT";
	private static final String CREATION_DATE = "CREATION_DATE";
	private static final String NEWS_ID = "NEWS_ID";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(Comment comment) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Long commentId = null;
		String generatedColumns[] = { COMMENT_ID };

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);

			preparedStatement.setString(1, comment.getText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));

			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				commentId = resultSet.getLong(1);
			}
			return commentId;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while creating comment instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public Comment read(Long commentId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Comment comment = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				comment = new Comment();
				comment.setId(resultSet.getLong(COMMENT_ID));
				comment.setText(resultSet.getString(COMMENT_TEXT));
				comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
				comment.setNewsId(resultSet.getLong(NEWS_ID));
			}
			return comment;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading comment instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<Comment> readAll() throws DaoException {
		List<Comment> commentList = new ArrayList<>();
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READALL);

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(COMMENT_ID));
				comment.setText(resultSet.getString(COMMENT_TEXT));
				comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));

				comment.setNewsId(resultSet.getLong(NEWS_ID));
				commentList.add(comment);
			}

			return commentList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading all comment instances. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(Comment comment) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);

			preparedStatement.setString(1, comment.getText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));

			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.setLong(4, comment.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while updating comment instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while deleting comment instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteCommentsByNews(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while deleting comment instances by news. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		List<Comment> commentList = new ArrayList<>();
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(COMMENT_ID));
				comment.setText(resultSet.getString(COMMENT_TEXT));
				comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));

				comment.setNewsId(resultSet.getLong(NEWS_ID));
				commentList.add(comment);
			}
			return commentList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while searching comment instances by news. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

}
