package com.epam.newsmanagement.dao.implementation.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.DatabaseUtils;

/**
 * {@code NewsDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code INewsDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.INewsDao
 * @see com.epam.newsmanagement.entity.News
 */
public class NewsDaoImpl implements INewsDao {

	private static final String TIME_FORMAT = "GMT";
	/* SQL queries */
	private static final String SQL_CREATE = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.NEXTVAL,?,?,?,?,?)";

	private static final String SQL_READ = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
			+ "FROM NEWS WHERE NEWS_ID = ?";

	private static final String SQL_UPDATE = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, "
			+ "FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ?  WHERE NEWS_ID = ?";

	private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NEWS_ID = ?";

	private static final String SQL_SEARCH_BY_AUTHOR = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON "
			+ "NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ?";

	private static final String SQL_SEARCH_BY_TAG = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON "
			+ "NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID = ?";

	private static final String SQL_READALL_SORTED_BY_COMMENTS_AND_DATE = "SELECT  NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT,"
			+ " NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS "
			+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, "
			+ "NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID), MODIFICATION_DATE DESC";

	private static final String SQL_BIND_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	private static final String SQL_BIND_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";
	private static final String SQL_GET_RECORD_COUNT = "SELECT COUNT(*) FROM NEWS";

	/* next and previous news ID query parts */
	private static final String FILTER_NEXT_ID_BASE = " SELECT NEXT_NEWS FROM ( SELECT NEWS_ID, LEAD (NEWS_ID,1) OVER (ORDER BY ROWNUM) AS NEXT_NEWS FROM ( ";
	private static final String FILTER_PREV_ID_BASE = " SELECT PREV_NEWS FROM ( SELECT NEWS_ID, LAG (NEWS_ID,1) OVER (ORDER BY ROWNUM) AS PREV_NEWS FROM ( ";
	private static final String FILTER_ID_WHERE_CLAUSE = " ) ) WHERE NEWS_ID = ?";
	private static final String FILTER_ID_INNER_SELECT = " SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS  ";

	/* count filter query */
	private static final String COUNTFILTER_QUERY_BASE = "SELECT COUNT(*) FROM ( SELECT DISTINCT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS ";

	/* filter query parts */
	private static final String FILTER_QUERY_BASE = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM "
			+ "( SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, ROWNUM rn FROM "
			+ "( SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS ";

	private static final String FILTER_JOIN_AUTHOR = " LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID";
	private static final String FILTER_JOIN_TAG = " LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID ";
	private static final String FILTER_JOIN_COMMENTS = " LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";
	private static final String FILTER_GROUP_BY = " GROUP BY (NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) ";
	private static final String FILTER_WHERE = " WHERE ";
	private static final String FILTER_AUTHOR_ID_CLAUSE = " AUTHOR_ID = ";
	private static final String FILTER_RN_CLAUSE_LESS = " rn <= ";
	private static final String FILTER_RN_CLAUSE_MORE = " rn >= ";
	private static final String FILTER_AND = " AND ";
	private static final String FILTER_OR = " OR ";
	private static final String FILTER_TAG_ID_CLAUSE = " TAG_ID = ";
	private static final String FILTER_ORDER_BY = " ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.COMMENT_ID) DESC ";

	/* DB fields */
	private static final String NEWS_ID = "NEWS_ID";
	private static final String NEWS_TITLE = "TITLE";
	private static final String NEWS_SHORTTEXT = "SHORT_TEXT";
	private static final String NEWS_FULLTEXT = "FULL_TEXT";
	private static final String NEWS_CREATIONDATE = "CREATION_DATE";
	private static final String NEWS_MODIFICATIONDATE = "MODIFICATION_DATE";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public Long create(News news) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Long newsId = null;
		String generatedColumns[] = { NEWS_ID };

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE,
					generatedColumns);

			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());

			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));

			preparedStatement.setTimestamp(5, new Timestamp(news
					.getModificationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			}

			return newsId;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while creating news instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public News read(Long newsId) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		News news = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong(NEWS_ID));
				news.setTitle(resultSet.getString(NEWS_TITLE));
				news.setShortText(resultSet.getString(NEWS_SHORTTEXT));
				news.setFullText(resultSet.getString(NEWS_FULLTEXT));

				news.setCreationDate(resultSet.getTimestamp(NEWS_CREATIONDATE));

				news.setModificationDate(resultSet
						.getTimestamp(NEWS_MODIFICATIONDATE));
			}

			return news;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading news instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<News> readAll() throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		Statement statement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement
					.executeQuery(SQL_READALL_SORTED_BY_COMMENTS_AND_DATE);

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(NEWS_ID));
				news.setTitle(resultSet.getString(NEWS_TITLE));
				news.setShortText(resultSet.getString(NEWS_SHORTTEXT));
				news.setFullText(resultSet.getString(NEWS_FULLTEXT));
				news.setCreationDate(resultSet.getTimestamp(NEWS_CREATIONDATE));

				news.setModificationDate(resultSet
						.getTimestamp(NEWS_MODIFICATIONDATE));

				newsList.add(news);
			}
			return newsList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading all news instances. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public List<News> readAll(int beginIndex, int endIndex,
			SearchFilter searchFilter) throws DaoException {
		ResultSet resultSet = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();
		String searchFilterQuery = buildSearchQuery(beginIndex, endIndex,
				searchFilter);

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(searchFilterQuery);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(NEWS_ID));
				news.setTitle(resultSet.getString(NEWS_TITLE));
				news.setShortText(resultSet.getString(NEWS_SHORTTEXT));
				news.setFullText(resultSet.getString(NEWS_FULLTEXT));
				news.setCreationDate(resultSet.getTimestamp(NEWS_CREATIONDATE));

				news.setModificationDate(resultSet
						.getTimestamp(NEWS_MODIFICATIONDATE));
				newsList.add(news);
			}

			return newsList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading all news instances. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	public int getCountOfFilterNews(SearchFilter searchFilter)
			throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int recordCount = 0;
		String query = buildCountQuery(searchFilter);

		try {
			connection = databaseUtils.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			if (resultSet.next()) {
				recordCount = resultSet.getInt(1);
			}

			return recordCount;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while reading news instances number. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(resultSet, statement, connection);
		}
	}

	@Override
	public void update(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));

			preparedStatement.setTimestamp(5, new Timestamp(news
					.getModificationDate().getTime()), Calendar
					.getInstance(TimeZone.getTimeZone(TIME_FORMAT)));
			preparedStatement.setLong(6, news.getId());
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while updating news instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while deleting news instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<News> searchNewsByTag(Long tagId) throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_TAG);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(NEWS_ID));
				news.setTitle(resultSet.getString(NEWS_TITLE));
				news.setShortText(resultSet.getString(NEWS_SHORTTEXT));
				news.setFullText(resultSet.getString(NEWS_FULLTEXT));
				news.setCreationDate(resultSet.getTimestamp(NEWS_CREATIONDATE));

				news.setModificationDate(resultSet
						.getTimestamp(NEWS_MODIFICATIONDATE));
				newsList.add(news);
			}

			return newsList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while searching news instances by tag. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		List<News> newsList = new ArrayList<>();

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_AUTHOR);

			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(NEWS_ID));
				news.setTitle(resultSet.getString(NEWS_TITLE));
				news.setShortText(resultSet.getString(NEWS_SHORTTEXT));
				news.setFullText(resultSet.getString(NEWS_FULLTEXT));
				news.setCreationDate(resultSet.getTimestamp(NEWS_CREATIONDATE));

				news.setModificationDate(resultSet
						.getTimestamp(NEWS_MODIFICATIONDATE));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while searching news instances by author. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public void associateNewsAuthor(Long newsId, Long authorId)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_BIND_NEWS_AUTHOR);

			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while binding news and author instances. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void associateNewsTagList(Long newsId, List<Tag> tagList)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(SQL_BIND_NEWS_TAG);

			for (Tag tag : tagList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tag.getId());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while binding news and tag instances. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_BIND_NEWS_TAG);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(
					"Error while binding news and tag instances. See details: ",
					ex);
		} finally {
			databaseUtils.closeResources(preparedStatement, connection);
		}

	}

	@Override
	public Long next(Long newsId, SearchFilter searchFilter)
			throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Long nextId = null;

		try {
			connection = databaseUtils.getConnection();
			String query = buildNextNavigationQuery(newsId, searchFilter);
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				nextId = resultSet.getLong(1);
			}
			return nextId;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while counting next news ID. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	@Override
	public Long previous(Long newsId, SearchFilter searchFilter)
			throws DaoException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Long prevId = null;
		try {
			connection = databaseUtils.getConnection();
			String query = buildPrevNavigationQuery(newsId, searchFilter);

			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				prevId = resultSet.getLong(1);
			}

			return prevId;
		} catch (SQLException ex) {
			throw new DaoException(
					"Error while counting previous news ID. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	private String buildNextNavigationQuery(Long newsId,
			SearchFilter searchFilter) {
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = searchFilter == null;

		StringBuilder queryBuilder = new StringBuilder(FILTER_NEXT_ID_BASE);
		queryBuilder.append(FILTER_ID_INNER_SELECT);

		if (!isFilter && searchFilter.getAuthorId() != null) {
			queryBuilder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && searchFilter.getTagIdList() != null
				&& !searchFilter.getTagIdList().isEmpty()) {
			queryBuilder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		queryBuilder.append(FILTER_JOIN_COMMENTS);
		if (checkAuthor || checkTagList) {
			queryBuilder.append(FILTER_WHERE);
		}

		if (checkAuthor) {
			queryBuilder.append(FILTER_AUTHOR_ID_CLAUSE);
			queryBuilder.append(searchFilter.getAuthorId());
		}

		if (checkAuthor && checkTagList) {
			queryBuilder.append(FILTER_AND);
		}

		if (checkTagList) {

			for (Long tagId : searchFilter.getTagIdList()) {
				queryBuilder.append(FILTER_TAG_ID_CLAUSE);
				queryBuilder.append(tagId);
				queryBuilder.append(FILTER_OR);
			}
			queryBuilder.delete(queryBuilder.length() - 3,
					queryBuilder.length());
		}
		queryBuilder.append(FILTER_GROUP_BY);
		queryBuilder.append(FILTER_ORDER_BY);
		queryBuilder.append(FILTER_ID_WHERE_CLAUSE);

		return queryBuilder.toString();
	}

	private String buildPrevNavigationQuery(Long newsId,
			SearchFilter searchFilter) {
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = searchFilter == null;

		StringBuilder queryBuilder = new StringBuilder(FILTER_PREV_ID_BASE);
		queryBuilder.append(FILTER_ID_INNER_SELECT);

		if (!isFilter && searchFilter.getAuthorId() != null) {
			queryBuilder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && searchFilter.getTagIdList() != null
				&& !searchFilter.getTagIdList().isEmpty()) {
			queryBuilder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		queryBuilder.append(FILTER_JOIN_COMMENTS);
		if (checkAuthor || checkTagList) {
			queryBuilder.append(FILTER_WHERE);
		}

		if (checkAuthor) {
			queryBuilder.append(FILTER_AUTHOR_ID_CLAUSE);
			queryBuilder.append(searchFilter.getAuthorId());
		}

		if (checkAuthor && checkTagList) {
			queryBuilder.append(FILTER_AND);
		}

		if (checkTagList) {

			for (Long tagId : searchFilter.getTagIdList()) {
				queryBuilder.append(FILTER_TAG_ID_CLAUSE);
				queryBuilder.append(tagId);
				queryBuilder.append(FILTER_OR);
			}
			queryBuilder.delete(queryBuilder.length() - 3,
					queryBuilder.length());
		}
		queryBuilder.append(FILTER_GROUP_BY);
		queryBuilder.append(FILTER_ORDER_BY);
		queryBuilder.append(FILTER_ID_WHERE_CLAUSE);

		return queryBuilder.toString();
	}

	private String buildCountQuery(SearchFilter searchFilter) {
		if (searchFilter == null) {
			return SQL_GET_RECORD_COUNT;
		}

		StringBuilder queryBuilder = new StringBuilder(COUNTFILTER_QUERY_BASE);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		if (searchFilter.getAuthorId() != null) {
			queryBuilder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (searchFilter.getTagIdList() != null
				&& !searchFilter.getTagIdList().isEmpty()) {
			queryBuilder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}
		queryBuilder.append(FILTER_JOIN_COMMENTS);

		if (checkAuthor || checkTagList) {
			queryBuilder.append(FILTER_WHERE);
		}

		if (checkAuthor) {
			queryBuilder.append(FILTER_AUTHOR_ID_CLAUSE);
			queryBuilder.append(searchFilter.getAuthorId());
		}

		if (checkAuthor && checkTagList) {
			queryBuilder.append(FILTER_AND);
		}

		if (checkTagList) {
			queryBuilder.append(" ( ");
			for (Long tagId : searchFilter.getTagIdList()) {
				queryBuilder.append(FILTER_TAG_ID_CLAUSE);
				queryBuilder.append(tagId);
				queryBuilder.append(FILTER_OR);
			}
			queryBuilder.delete(queryBuilder.length() - 3,
					queryBuilder.length());
			queryBuilder.append(" ) ");

		}
		queryBuilder.append(FILTER_GROUP_BY);
		queryBuilder.append(FILTER_ORDER_BY);
		queryBuilder.append(" ) ");
		return queryBuilder.toString();
	}

	private String buildSearchQuery(int startIndex, int endIndex,
			SearchFilter searchFilter) {
		StringBuilder queryBuilder = new StringBuilder(FILTER_QUERY_BASE);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = searchFilter == null;

		if (!isFilter && searchFilter.getAuthorId() != null) {
			queryBuilder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && searchFilter.getTagIdList() != null
				&& !searchFilter.getTagIdList().isEmpty()) {
			queryBuilder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}
		queryBuilder.append(FILTER_JOIN_COMMENTS);

		if (checkAuthor || checkTagList) {
			queryBuilder.append(FILTER_WHERE);
		}

		if (checkAuthor) {
			queryBuilder.append(FILTER_AUTHOR_ID_CLAUSE);
			queryBuilder.append(searchFilter.getAuthorId());
		}

		if (checkAuthor && checkTagList) {
			queryBuilder.append(FILTER_AND);

		}

		if (checkTagList) {
			queryBuilder.append(" ( ");
			for (Long tagId : searchFilter.getTagIdList()) {
				queryBuilder.append(FILTER_TAG_ID_CLAUSE);
				queryBuilder.append(tagId);
				queryBuilder.append(FILTER_OR);
			}
			queryBuilder.delete(queryBuilder.length() - 3,
					queryBuilder.length());

			queryBuilder.append(" ) ");

		}

		queryBuilder.append(FILTER_GROUP_BY);
		queryBuilder.append(FILTER_ORDER_BY);
		queryBuilder.append(" ) ");

		queryBuilder.append(" ) ");
		queryBuilder.append(FILTER_WHERE);
		queryBuilder.append(FILTER_RN_CLAUSE_MORE);
		queryBuilder.append(startIndex);
		queryBuilder.append(FILTER_AND);
		queryBuilder.append(FILTER_RN_CLAUSE_LESS);
		queryBuilder.append(endIndex);

		return queryBuilder.toString();
	}

}
