package com.epam.newsmanagement.dao.implementation.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IUserDao;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.utils.DatabaseUtils;

/**
 * {@code IUserDao} interface implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IUserDao
 */
public class UserDaoImpl implements IUserDao {
	/* SQL queries */
	private static final String SQL_READ = "SELECT USERS.USER_ID, USER_LOGIN, USER_NAME, USER_PASSWORD, "
			+ "ROLE_NAME FROM USERS JOIN ROLES ON ROLES.USER_ID = USERS.USER_ID WHERE USER_LOGIN = ?";

	/* DB fields names */
	private static final String USER_ID = "USER_ID";
	private static final String USER_LOGIN = "USER_LOGIN";
	private static final String USER_NAME = "USER_NAME";
	private static final String USER_PASSWORD = "USER_PASSWORD";
	private static final String USER_ROLE = "ROLE_NAME";

	@Autowired
	private DatabaseUtils databaseUtils;

	@Override
	public User loadUserByUsername(String name) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		User user = null;

		try {
			connection = databaseUtils.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ);
			preparedStatement.setString(1, name);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getLong(USER_ID));
				user.setLogin(resultSet.getString(USER_LOGIN));
				user.setUsername(resultSet.getString(USER_NAME));
				user.setPassword(resultSet.getString(USER_PASSWORD));

				Role role = new Role();
				role.setName(resultSet.getString(USER_ROLE));
				Set<Role> roles = new HashSet<Role>();
				roles.add(role);
				user.setAuthorities(roles);
			}
			return user;
		} catch (SQLException ex) {
			throw new DaoException("Error while reading user instance. See details: ", ex);
		} finally {
			databaseUtils.closeResources(resultSet, preparedStatement, connection);
		}
	}

}
