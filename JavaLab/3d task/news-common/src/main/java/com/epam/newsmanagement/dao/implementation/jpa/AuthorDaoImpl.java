package com.epam.newsmanagement.dao.implementation.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utils.JpaManagerUtils;

/**
 * {@code AuthorDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code IAuthorDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IAuthorDao
 * @see com.epam.newsmanagement.entity.Author
 *
 */
public class AuthorDaoImpl implements IAuthorDao {

	private static final String PARAM_AUTHOR_NAME = "authorName";
	private static final String PARAM_NEWS_ID = "newsId";

	private static final String JPQL_READ_ALL = "SELECT author FROM Author author";
	private static final String JPQL_READ_AUTHOR_BY_NAME = "SELECT author FROM Author author WHERE author.name = :authorName";
	private static final String JPQL_READ_AUTHOR_BY_NEWS = "SELECT author FROM Author author JOIN author.newsSet news WHERE news.id = :newsId";

	@Autowired
	private JpaManagerUtils managerUtils;

	@Override
	public Long create(Author entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.flush();
			manager.getTransaction().commit();
			return entity.getId();
		} catch (Exception ex) {
			throw new DaoException("error while creating Author instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public Author read(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			return manager.find(Author.class, entityId);
		} catch (Exception ex) {
			throw new DaoException("error while reading Author instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readAll() throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_ALL);
			List<Author> authorList = query.getResultList();
			return authorList;
		} catch (Exception ex) {
			throw new DaoException("error while reading all Author instances. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void update(Author entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Author author = manager.find(Author.class, entity.getId());
			author.setName(entity.getName());
			author.setExpired(entity.getExpired());
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while updating Author instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Author author = manager.find(Author.class, entityId);
			manager.remove(author);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Author instance. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public Author read(String authorName) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_AUTHOR_BY_NAME);
			query.setParameter(PARAM_AUTHOR_NAME, authorName);
			Author author = (Author) query.getSingleResult();
			return author;
		} catch (Exception ex) {
			throw new DaoException("error while reading Author instance by name. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public Author searchAuthorByNews(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_AUTHOR_BY_NEWS);
			query.setParameter(PARAM_NEWS_ID, newsId);
			Author author = (Author) query.getSingleResult();
			return author;
		} catch (Exception ex) {
			throw new DaoException("error while reading Author instance by News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteAuthorLinks(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createQuery(JPQL_READ_AUTHOR_BY_NEWS);
			query.setParameter(PARAM_NEWS_ID, newsId);

			List<Author> authorList = query.getResultList();
			News news = manager.find(News.class, newsId);
			for (Author author : authorList) {
				author.getNewsSet().remove(news);
			}
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Author links with News. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public void setExpired(Long authorId) throws DaoException {
		throw new UnsupportedOperationException("Not implemented yet.");
	}

}
