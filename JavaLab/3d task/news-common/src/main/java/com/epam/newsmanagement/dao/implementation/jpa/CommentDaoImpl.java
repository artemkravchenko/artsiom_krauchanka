package com.epam.newsmanagement.dao.implementation.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utils.JpaManagerUtils;

/**
 * {@code CommentDaoImpl} class is a part of Data Access layer. Implementation
 * of {@code ICommentDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.ICommentDao
 * @see com.epam.newsmanagement.entity.Comment
 */
public class CommentDaoImpl implements ICommentDao {

	private static final String PARAM_NEWS_ID = "newsId";
	private static final String JPQL_READ_COMMENTS_BY_NEWS = "SELECT comment FROM Comment comment WHERE comment.news.id = :newsId ORDER BY comment.creationDate DESC";
	private static final String JPQL_READ_COMMENT = "SELECT comment FROM Comment comment";

	@Autowired
	private JpaManagerUtils managerUtils;

	@Override
	public Long create(Comment entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.flush();
			manager.getTransaction().commit();
			return entity.getId();
		} catch (Exception ex) {
			throw new DaoException("error while creating Comment instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public Comment read(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			return manager.find(Comment.class, entityId);
		} catch (Exception ex) {
			throw new DaoException("error while reading Comment instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readAll() throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_COMMENT);
			List<Comment> commentList = query.getResultList();
			return commentList;
		} catch (Exception ex) {
			throw new DaoException("error while reading all Comment instances. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void update(Comment entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Comment comment = manager.find(Comment.class, entity.getId());
			comment.setText(entity.getText());
			comment.setCreationDate(entity.getCreationDate());
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while updating Comment instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Comment comment = manager.find(Comment.class, entityId);
			manager.remove(comment);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Comment instance. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_COMMENTS_BY_NEWS);
			query.setParameter(PARAM_NEWS_ID, newsId);
			List<Comment> commentList = query.getResultList();
			return commentList;
		} catch (Exception ex) {
			throw new DaoException("error while reading Comment instances by news. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public void deleteCommentsByNews(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, newsId);
			news.getCommentSet().clear();
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Comment instances by news. See Details: ", ex);
		} finally {
			manager.close();
		}
	}
}
