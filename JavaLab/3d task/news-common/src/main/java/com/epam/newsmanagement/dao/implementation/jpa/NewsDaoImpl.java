package com.epam.newsmanagement.dao.implementation.jpa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.JpaManagerUtils;

/**
 * {@code NewsDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code INewsDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.INewsDao
 * @see com.epam.newsmanagement.entity.News
 */
public class NewsDaoImpl implements INewsDao {

	private static final String PARAM_AUTHOR_IDS = "authorIds";
	private static final String PARAM_TAG_IDS = "tagIDs";
	private static final String PARAM_AUTHOR_ID = "authorId";
	private static final String PARAM_TAG_ID = "tagId";

	private static final String JPQL_READ_ALL = "SELECT news FROM News news";
	private static final String JPQL_READ_NEWS_BY_TAG = "SELECT news FROM News news JOIN news.tagSet tag WHERE tag.id = :tagId";
	private static final String JPQL_READ_NEWS_BY_AUTHOR = "SELECT news FROM News news JOIN news.authorSet author WHERE author.id = :authorId";

	private static final String COUNTFILTER_BASE_SELECT = " SELECT COUNT(news) AS totalCount FROM News news ";
	private static final String FILTER_BASE_SELECT = " SELECT news FROM News news ";
	private static final String FILTER_JOIN_AUTHOR = " JOIN news.authorSet author ";
	private static final String FILTER_JOIN_TAG = " JOIN news.tagSet tag ";
	private static final String FILTER_JOIN_COMMENT = " LEFT OUTER JOIN news.commentSet comment ";
	private static final String FILTER_WHERE_CLAUSE = " WHERE ";
	private static final String FILTER_AND_CLAUSE = " AND ";
	private static final String FILTER_GROUP_BY = " GROUP BY news.id, news.title, news.shortText, news.fullText, news.creationDate, news.modificationDate ";
	private static final String FILTER_ORDER_BY = " ORDER BY news.modificationDate DESC, COUNT(comment) ";
	private static final String FILTER_PARAM_AUTHOR_ID = " author.id IN :authorIds ";
	private static final String FILTER_PARAM_TAG_ID = "  tag.id IN :tagIDs  ";

	@Autowired
	private JpaManagerUtils managerUtils;

	@Override
	public Long create(News entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.flush();
			manager.getTransaction().commit();
			return entity.getId();
		} catch (Exception ex) {
			throw new DaoException("error while creating News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public News read(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			News news = manager.find(News.class, entityId);
			return news;
		} catch (Exception ex) {
			throw new DaoException("error while reading News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll() throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_ALL);
			List<News> newsList = query.getResultList();
			return newsList;
		} catch (Exception ex) {
			throw new DaoException("error while reading all News instances. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void update(News entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, entity.getId());
			news.setTitle(entity.getTitle());
			news.setShortText(entity.getShortText());
			news.setFullText(entity.getFullText());
			news.setCreationDate(entity.getCreationDate());
			news.setModificationDate(entity.getModificationDate());
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while updating News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, entityId);
			manager.remove(news);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting News instance. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long next(Long newsId, SearchFilter searchFilter) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			String queryString = buildSearchQuery(searchFilter);
			Query query = manager.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				List<Long> authorIdList = new ArrayList<Long>();
				authorIdList.add(searchFilter.getAuthorId());
				query.setParameter(PARAM_AUTHOR_IDS, authorIdList);
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameter(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}

			Iterator<News> it = query.getResultList().iterator();
			Long next = null;
			while (it.hasNext()) {
				News news = (News) it.next();
				if (news.getId().equals(newsId)) {
					next = (it.hasNext()) ? (Long) it.next().getId() : null;
					break;
				}
			}
			return next;

		} catch (Exception ex) {
			throw new DaoException("error while reading next News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long previous(Long newsId, SearchFilter searchFilter) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			String queryString = buildSearchQuery(searchFilter);
			Query query = manager.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				List<Long> authorIdList = new ArrayList<Long>();
				authorIdList.add(searchFilter.getAuthorId());
				query.setParameter(PARAM_AUTHOR_IDS, authorIdList);
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameter(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}

			List<News> newsList = query.getResultList();
			ListIterator<News> it = newsList.listIterator(newsList.size());
			Long prev = null;

			while (it.hasPrevious()) {
				News news = (News) it.previous();
				if (news.getId().equals(newsId)) {
					prev = (it.hasPrevious()) ? (Long) it.previous().getId() : null;
					break;
				}
			}
			return prev;

		} catch (Exception ex) {
			throw new DaoException("error while reading previous News instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public int getCountOfFilterNews(SearchFilter searchFilter) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			String queryString = buildCountSearchQuery(searchFilter);
			Query query = manager.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				List<Long> authorIdList = new ArrayList<Long>();
				authorIdList.add(searchFilter.getAuthorId());
				query.setParameter(PARAM_AUTHOR_IDS, authorIdList);
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameter(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}
			Long result = (Long) query.getSingleResult();
			return result.intValue();

		} catch (Exception ex) {
			throw new DaoException("error while reading News instances count. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll(int beginIndex, int endIndex, SearchFilter searchFilter) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			String queryString = buildSearchQuery(searchFilter);
			Query query = manager.createQuery(queryString);

			if (searchFilter != null && searchFilter.getAuthorId() != null) {
				List<Long> authorIdList = new ArrayList<Long>();
				authorIdList.add(searchFilter.getAuthorId());
				query.setParameter(PARAM_AUTHOR_IDS, authorIdList);
			}

			if (searchFilter != null && searchFilter.getTagIdList() != null && !searchFilter.getTagIdList().isEmpty()) {
				query.setParameter(PARAM_TAG_IDS, searchFilter.getTagIdList());
			}
			query.setFirstResult(beginIndex - 1).setMaxResults(endIndex - beginIndex + 1);
			return query.getResultList();

		} catch (Exception ex) {
			throw new DaoException("error while reading News instances in range. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNewsByTag(Long tagId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_NEWS_BY_TAG);
			query.setParameter(PARAM_TAG_ID, tagId);
			List<News> newsList = query.getResultList();
			return newsList;
		} catch (Exception ex) {
			throw new DaoException("error while searching News instances by Tag. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNewsByAuthor(Long authorId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_NEWS_BY_AUTHOR);
			query.setParameter(PARAM_AUTHOR_ID, authorId);
			List<News> newsList = query.getResultList();
			return newsList;
		} catch (Exception ex) {
			throw new DaoException("error while searching News instances by Author. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void associateNewsAuthor(Long newsId, Long authorId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, newsId);
			Author author = (Author) manager.find(Author.class, authorId);
			news.getAuthorSet().add(author);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while assosiating News and Author instances. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public void associateNewsTag(Long newsId, Long tagId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, newsId);
			Tag tag = (Tag) manager.find(Tag.class, tagId);
			news.getTagSet().add(tag);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while assosiating News and Tag instances. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public void associateNewsTagList(Long newsId, List<Tag> tagList) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			News news = manager.find(News.class, newsId);

			for (Tag tag : tagList) {
				news.getTagSet().add(tag);
			}
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while assosiating News and Tag instances. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	private String buildSearchQuery(SearchFilter filter) {
		StringBuilder builder = new StringBuilder(FILTER_BASE_SELECT);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = filter == null;

		if (!isFilter && filter.getAuthorId() != null) {
			builder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && filter.getTagIdList() != null && !filter.getTagIdList().isEmpty()) {
			builder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		builder.append(FILTER_JOIN_COMMENT);

		if (checkAuthor || checkTagList) {
			builder.append(FILTER_WHERE_CLAUSE);
		}

		if (checkAuthor) {
			builder.append(FILTER_PARAM_AUTHOR_ID);

		}

		if (checkAuthor && checkTagList) {
			builder.append(FILTER_AND_CLAUSE);

		}

		if (checkTagList) {
			builder.append(FILTER_PARAM_TAG_ID);
		}

		builder.append(FILTER_GROUP_BY);
		builder.append(FILTER_ORDER_BY);

		return builder.toString();
	}

	private String buildCountSearchQuery(SearchFilter filter) {
		StringBuilder builder = new StringBuilder(COUNTFILTER_BASE_SELECT);
		boolean checkTagList = false;
		boolean checkAuthor = false;
		boolean isFilter = filter == null;

		if (!isFilter && filter.getAuthorId() != null) {
			builder.append(FILTER_JOIN_AUTHOR);
			checkAuthor = true;
		}

		if (!isFilter && filter.getTagIdList() != null && !filter.getTagIdList().isEmpty()) {
			builder.append(FILTER_JOIN_TAG);
			checkTagList = true;
		}

		if (checkAuthor || checkTagList) {
			builder.append(FILTER_WHERE_CLAUSE);
		}

		if (checkAuthor) {
			builder.append(FILTER_PARAM_AUTHOR_ID);

		}

		if (checkAuthor && checkTagList) {
			builder.append(FILTER_AND_CLAUSE);

		}

		if (checkTagList) {
			builder.append(FILTER_PARAM_TAG_ID);
		}

		return builder.toString();
	}
}
