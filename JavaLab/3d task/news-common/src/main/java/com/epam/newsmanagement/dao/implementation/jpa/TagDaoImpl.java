package com.epam.newsmanagement.dao.implementation.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.JpaManagerUtils;

/**
 * {@code TagDaoImpl} class is a part of Data Access layer. Implementation of
 * {@code ITagDao} interface.
 *
 * @author Artsiom_Krauchanka
 * @version 1.0
 * @see com.epam.newsmanagement.dao.ITagDao
 * @see com.epam.newsmanagement.entity.Tag
 */
public class TagDaoImpl implements ITagDao {

	private static final String PARAM_TAG_NAME = "tagName";
	private static final String PARAM_NEWS_ID = "newsId";

	private static final String JPQL_READ_TAG = "SELECT tag FROM Tag tag";
	private static final String JPQL_READ_TAG_BY_NAME = "SELECT tag FROM Tag tag where tag.name = :tagName";
	private static final String JPQL_READ_TAG_BY_NEWS = "SELECT tag FROM Tag tag JOIN tag.newsSet news where news.id = :newsId";

	@Autowired
	private JpaManagerUtils managerUtils;

	@Override
	public Long create(Tag entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.flush();
			manager.getTransaction().commit();
			return entity.getId();
		} catch (Exception ex) {
			throw new DaoException("error while creating Tag instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public Tag read(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			return manager.find(Tag.class, entityId);
		} catch (Exception ex) {
			throw new DaoException("error while reading Tag instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readAll() throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_TAG);
			List<Tag> newsList = query.getResultList();
			return newsList;
		} catch (Exception ex) {
			throw new DaoException("error while reading all Tag instances. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void update(Tag entity) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Tag tag = manager.find(Tag.class, entity.getId());
			tag.setName(entity.getName());
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while updating Tag instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void delete(Long entityId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Tag tag = manager.find(Tag.class, entityId);
			manager.remove(tag);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Tag instance. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@Override
	public Tag read(String tagName) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_TAG_BY_NAME);
			query.setParameter(PARAM_TAG_NAME, tagName);
			Tag tag = (Tag) query.getSingleResult();
			return tag;
		} catch (Exception ex) {
			throw new DaoException("error while reading Tag instance by name. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_TAG_BY_NEWS);
			query.setParameter(PARAM_NEWS_ID, newsId);
			List<Tag> tagList = query.getResultList();
			return tagList;
		} catch (Exception ex) {
			throw new DaoException("error while reading Tag instance by news. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

	@Override
	public void deleteLinks(Long tagId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Tag tag = (Tag) manager.find(Tag.class, tagId);
			tag.getNewsSet().clear();
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Tag links with News. See Details: ", ex);
		} finally {
			manager.close();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteTagLinks(Long newsId) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createQuery(JPQL_READ_TAG_BY_NEWS);
			query.setParameter(PARAM_NEWS_ID, newsId);
			List<Tag> tagList = query.getResultList();
			News news = (News) manager.find(News.class, newsId);

			for (Tag tag : tagList) {
				tag.getNewsSet().remove(news);
			}
			manager.getTransaction().commit();
		} catch (Exception ex) {
			throw new DaoException("error while deleting Tag links with News. See Details: ", ex);
		} finally {
			manager.close();
		}
	}

}
