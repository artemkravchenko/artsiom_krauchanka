package com.epam.newsmanagement.dao.implementation.jpa;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IUserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.utils.JpaManagerUtils;

/**
 * {@code IUserDao} interface implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.dao.IUserDao
 */
public class UserDaoImpl implements IUserDao {

	private static final String PARAM_USER_LOGIN = "login";
	private static final String JPQL_READ_USER_BY_LOGIN = "SELECT user FROM User user WHERE user.login = :login";

	@Autowired
	private JpaManagerUtils managerUtils;

	@Override
	public User loadUserByUsername(String name) throws DaoException {
		EntityManager manager = managerUtils.getManager();
		try {
			Query query = manager.createQuery(JPQL_READ_USER_BY_LOGIN);
			query.setParameter(PARAM_USER_LOGIN, name);
			User user = (User) query.getSingleResult();
			return user;
		} catch (Exception ex) {
			throw new DaoException("error while reading User instance. See Details: ", ex);
		} finally {
			manager.close();
		}
	}
}
