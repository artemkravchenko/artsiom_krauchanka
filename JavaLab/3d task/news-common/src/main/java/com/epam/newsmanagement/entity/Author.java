package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * {@code Author} class is POJO class. It contains fields according to the
 * {@code AUTHOR} table in the database.
 * 
 * @author Artsiom_Krauchanka
 * 
 */
@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {

	private static final long serialVersionUID = 1869139131182905726L;

	@Id
	@SequenceGenerator(name = "authorSeq", sequenceName = "AUTHOR_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authorSeq")
	@Column(name = "AUTHOR_ID", length = 20, nullable = false)
	private Long id;

	@Column(name = "AUTHOR_NAME", length = 30, nullable = false)
	private String name;

	@Column(name = "EXPIRED", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date expired;

	@ManyToMany(targetEntity = News.class, cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = { @JoinColumn(name = "AUTHOR_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	private Set<News> newsSet;

	public Author() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	public Set<News> getNewsSet() {
		return newsSet;
	}

	public void setNewsSet(Set<News> newsSet) {
		this.newsSet = newsSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash + ((expired == null) ? 0 : expired.hashCode());
		hash = prime * hash + ((id == null) ? 0 : id.hashCode());
		hash = prime * hash + ((name == null) ? 0 : name.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Author other = (Author) obj;

		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired + "]";
	}

}