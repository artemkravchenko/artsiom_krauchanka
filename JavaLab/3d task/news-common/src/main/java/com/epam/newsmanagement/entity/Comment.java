package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * {@code Comment} class is POJO class. It contains fields according to the
 * {@code Comment} table in the database.
 *
 * @author Artsiom_Krauchanka
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = 473699854147992329L;

	@Id
	@SequenceGenerator(name = "commentSeq", sequenceName = "COMMENT_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commentSeq")
	@Column(name = "COMMENT_ID", length = 20, nullable = false)
	private Long id;

	@Column(name = "COMMENT_TEXT", length = 100, nullable = false)
	private String text;

	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@ManyToOne(targetEntity = News.class)
	@JoinColumn(name = "NEWS_ID")
	private News news;

	@Transient
	private Long newsId;

	public Comment() {

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash + ((creationDate == null) ? 0 : creationDate.hashCode());
		hash = prime * hash + ((id == null) ? 0 : id.hashCode());
		hash = prime * hash + ((news == null) ? 0 : news.hashCode());
		hash = prime * hash + ((text == null) ? 0 : text.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Comment other = (Comment) obj;

		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;

		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentText=" + text + ", creationDate=" + creationDate + "]";
	}

}
