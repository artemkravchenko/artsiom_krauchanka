package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * {@code News} class is POJO class. It contains fields according to the
 * {@code News} table in the database.
 *
 * @author Artsiom_Krauchanka
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = 8648344003364039122L;

	@Id
	@SequenceGenerator(name = "newsSeq", sequenceName = "NEWS_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "newsSeq")
	@Column(name = "NEWS_ID", length = 20, nullable = false)
	private Long id;

	@Column(name = "TITLE", length = 50, nullable = false)
	private String title;

	@Column(name = "SHORT_TEXT", length = 200, nullable = false)
	private String shortText;

	@Column(name = "FULL_TEXT", length = 2000, nullable = false)
	private String fullText;

	@Column(name = "CREATION_DATE", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "MODIFICATION_DATE", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@ManyToMany(mappedBy = "newsSet", fetch = FetchType.EAGER)
	private Set<Author> authorSet;

	@ManyToMany(mappedBy = "newsSet", fetch = FetchType.EAGER)
	private Set<Tag> tagSet;

	@OneToMany(mappedBy = "news", fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	private Set<Comment> commentSet;

	public News() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Author> getAuthorSet() {
		return authorSet;
	}

	public void setAuthorSet(Set<Author> authorSet) {
		this.authorSet = authorSet;
	}

	public Set<Comment> getCommentSet() {
		return commentSet;
	}

	public void setCommentSet(Set<Comment> commentSet) {
		this.commentSet = commentSet;
	}

	public Set<Tag> getTagSet() {
		return tagSet;
	}

	public void setTagSet(Set<Tag> tagSet) {
		this.tagSet = tagSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		News other = (News) obj;

		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;

		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;

		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;

		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate
				+ ", authorSet=" + authorSet + ", commentSet=" + commentSet + ", tagSet=" + tagSet + "]";
	}

}