package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * {@code NewsVO} class represents a fixed set of data related with {@code News}
 * object.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class NewsVO implements Serializable {

	private static final long serialVersionUID = -318572220353175082L;
	private News news;
	private Author author;
	private List<Comment> commentList;
	private List<Tag> tagList;

	public NewsVO() {

	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		NewsVO other = (NewsVO) obj;
		
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "NewsPost [news=" + news + ", author=" + author
				+ ", commentList=" + commentList + ", tagList=" + tagList + "]";
	}

}
