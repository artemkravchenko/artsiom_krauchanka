package com.epam.newsmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

/**
 * {@code Role} class is POJO class. It contains fields according to the
 * {@code Role} table in the database and represents {@code User} role.
 * 
 * @author Artsiom_Krauchanka
 * @see org.springframework.security.core.GrantedAuthority
 */
@Entity
@Table(name = "ROLES")
public class Role implements GrantedAuthority {

	private static final long serialVersionUID = -1026143473373216149L;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;

	@Id
	@Column(name = "ROLE_NAME", length = 30, nullable = false)
	private String name;

	public Role() {

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getAuthority() {
		return this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Role other = (Role) obj;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Role [name=" + name + "]";
	}

}
