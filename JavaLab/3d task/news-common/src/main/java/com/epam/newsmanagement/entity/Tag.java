package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * {@code Tag} class is POJO class. It contains fields according to the
 * {@code Tag} table in the database.
 *
 * @author Artsiom_Krauchanka
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {

	private static final long serialVersionUID = 3645439362730218722L;

	@Id
	@SequenceGenerator(name = "tagSeq", sequenceName = "TAG_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tagSeq")
	@Column(name = "TAG_ID", length = 20, nullable = false)
	private Long id;

	@Column(name = "TAG_NAME", length = 30, nullable = false)
	private String name;

	@ManyToMany(targetEntity = News.class, cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "TAG_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	private Set<News> newsSet;

	public Tag() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<News> getNewsSet() {
		return newsSet;
	}

	public void setNewsSet(Set<News> newsSet) {
		this.newsSet = newsSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Tag other = (Tag) obj;

		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", tagName=" + name + "]";
	}

}
