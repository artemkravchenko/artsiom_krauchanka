package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;
import java.util.List;

/**
 * {@code ICommentService} interface describes the behavior of a particular
 * service layer which working with {@code Comment} instance using Data Access
 * layer implementations of interface {@code ICommentDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.newsmanagement.service.IGenericService
 * @see com.epam.newsmanagement.dao.ICommentDao
 * @see com.epam.newsmanagement.entity.Comment
 */
public interface ICommentService extends IGenericService<Comment, Long> {

	/**
	 * Search {@code Comment} instances in the DB using Data access layer.
	 *
	 * @param newsId
	 *            {@code News} instance ID
	 * @return list of {@code Comment} instances.
	 * @throws ServiceException
	 */
	public List<Comment> searchCommentsByNews(Long newsId) throws ServiceException;

	/**
	 * Delete all {@code Comment} instances associated by given {@code News}
	 * instance.
	 * 
	 * @param newsId
	 *            {@code News} instance ID
	 * @throws ServiceException
	 */
	public void deleteCommentsByNews(Long newsId) throws ServiceException;
}
