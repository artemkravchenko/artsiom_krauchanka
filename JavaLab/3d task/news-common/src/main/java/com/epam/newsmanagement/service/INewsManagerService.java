package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;

/**
 * {@code INewsManagerService} interface describes transactional operations for
 * {@code NewsVO} instance working.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.service.ServiceException
 */
public interface INewsManagerService {
	/**
	 * Construct {@code NewsVO} object.
	 * 
	 * @param newsId
	 *            {@code News} instance ID.
	 * @return {@code NewsVO} instance.
	 * @throws ServiceException
	 */
	public NewsVO readNewsPost(Long newsId) throws ServiceException;

	/**
	 * Save {@code NewsVO} instance to the DB.
	 * 
	 * @param newsPost
	 *            {@code NewsVO} instance
	 * @throws ServiceException
	 */
	public void saveNewsPost(NewsVO newsPost) throws ServiceException;

	/**
	 * Update {@code NewsVO} instance in the DB.
	 * 
	 * @param newsPost
	 *            {@code NewsVO} instance
	 * @throws ServiceException
	 */
	public void updateNewsPost(NewsVO newsPost) throws ServiceException;

	/**
	 * Delete {@code NewsVO} instance from the DB.
	 * 
	 * @param newsId
	 *            {@code News} instance ID
	 * @throws ServiceException
	 */
	public void deleteNewsPost(Long newsId) throws ServiceException;

	/**
	 * Read {@code NewsVO} instances on the current page.
	 * 
	 * @param pageNumber
	 *            number of current page.
	 * @param newsOnPage
	 *            count of news on the page.
	 * @param searchFilter
	 *            {@code SearchFilter} object
	 * @return list of {@code NewsVO} instances
	 * @throws ServiceException
	 */
	public List<NewsVO> getNewsOnPage(int pageNumber, int newsOnPage, SearchFilter searchFilter)
			throws ServiceException;

}
