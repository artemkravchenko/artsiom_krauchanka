package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Tag;

import java.util.List;

/**
 * {@code ITagService} interface describes the behavior of a particular service
 * layer which working with {@code Tag} instance using Data Access layer
 * implementations of interface {@code ITagDao}.
 * 
 * @author Artsiom_Krauchanka
 * 
 * @see com.epam.newsmanagement.service.IGenericService
 * @see com.epam.newsmanagement.dao.ITagDao
 * @see com.epam.newsmanagement.entity.Tag
 */
public interface ITagService extends IGenericService<Tag, Long> {

	/**
	 * Read {@code Tag} instance by tag name.
	 * 
	 * @param tagName
	 * @return {@code Tag} instance.
	 * @throws ServiceException
	 */
	public Tag read(String tagName) throws ServiceException;

	/**
	 * Search {@code Tag} instances according given {@code News} instance.
	 *
	 * @param newsId
	 *            {@code News} instance ID
	 * @return list of {@Tag} instances.
	 * @throws ServiceException
	 */
	public List<Tag> searchTagsByNews(Long newsId) throws ServiceException;

	/**
	 * Delete records in the {@code NEWS_TAG} table according news ID.
	 * 
	 * @param newsId
	 *            {@code News} instance ID
	 * @throws ServiceException
	 */
	public void deleteTagLinks(Long newsId) throws ServiceException;
}
