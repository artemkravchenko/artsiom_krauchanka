package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code AuthorServiceImpl} class is an element of Service layer. Its works
 * with {@code Author} DB instance. {@code IAuthorService} interface
 * implementation.
 *
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.service.IAuthorService
 * @see com.epam.newsmanagement.entity.Author
 */
public class AuthorServiceImpl implements IAuthorService {

	@Autowired
	private IAuthorDao authorDao;

	@Override
	public Long create(Author author) throws ServiceException {
		if (author != null) {
			try {
				Long authorId = authorDao.create(author);
				logger.info("Author instance created. ID: " + authorId);
				return authorId;

			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Author instance is empty");
		}
	}

	@Override
	public Author read(Long entityId) throws ServiceException {
		if (entityId > 0) {
			try {
				Author author = authorDao.read(entityId);
				logger.info("Author record read: " + author);
				return author;
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid author instance ID.");
		}
	}

	@Override
	public Author read(String authorName) throws ServiceException {
		if (authorName != null) {
			try {
				Author author = authorDao.read(authorName);
				logger.info("Author record read: " + author);
				return author;
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid author instance ID.");
		}
	}

	@Override
	public List<Author> readAll() throws ServiceException {
		try {
			List<Author> authorList = authorDao.readAll();
			logger.info("List of Author records read: " + authorList);
			return authorList;
		} catch (DaoException ex) {
			logger.error("Author service error: ", ex);
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		if (author != null) {
			try {
				authorDao.update(author);
				logger.info("Author instance updated. ID: " + author.getId());
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("author instance is empty");
		}
	}

	@Override
	public void setExpired(Long authorId) throws ServiceException {
		if (authorId > 0) {
			try {
				authorDao.setExpired(authorId);
				logger.info("Author instance updated. ID: " + authorId);
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid author instance ID.");
		}
	}

	@Override
	public void delete(Long authorId) throws ServiceException {
		if (authorId > 0) {
			try {
				authorDao.delete(authorId);
				logger.info("Author instance deleted. ID: " + authorId);
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid author instance ID.");
		}
	}

	@Override
	public void deleteAuthorLinks(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				authorDao.deleteAuthorLinks(newsId);
				logger.info("associated News and Author records deleted.");
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid News instance ID.");
		}
	}

	@Override
	public Author searchAuthorByNews(Long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				Author author = authorDao.searchAuthorByNews(newsId);
				logger.info("Author instance founded: " + author);
				return author;
			} catch (DaoException ex) {
				logger.error("Author service error: ", ex);
				throw new ServiceException(ex);
			}
		} else {
			throw new ServiceException("Invalid news instance ID.");
		}
	}

}
