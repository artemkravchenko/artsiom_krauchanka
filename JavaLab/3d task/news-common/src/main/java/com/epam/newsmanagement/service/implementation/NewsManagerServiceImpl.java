package com.epam.newsmanagement.service.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchFilter;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ServiceException;

/**
 * {@code NewsManagerServiceImpl} class is an element of Service layer. Its
 * works with {@code NewsVO} instance.{@code INewsManagerService} interface
 * implementation.
 * 
 * @author Artsiom_Krauchanka
 * @see com.epam.newsmanagement.entity.NewsVO
 * @see com.epam.newsmanagement.service.INewsManagerService
 */

public class NewsManagerServiceImpl implements INewsManagerService {

	@Autowired
	private INewsService newsService;

	@Override
	public NewsVO readNewsPost(Long newsId) throws ServiceException {
		NewsVO newsVO = null;
		News news = newsService.read(newsId);
		if (news != null) {
			newsVO = createNewsVO(news);
		}
		return newsVO;
	}

//	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveNewsPost(NewsVO newsPost) throws ServiceException {
		News news = getNewsObject(newsPost);
		newsService.create(news);
	}

//	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateNewsPost(NewsVO newsPost) throws ServiceException {
		News news = getNewsObject(newsPost);
		newsService.update(news);
	}

	@Override
	//@Transactional(rollbackFor = Exception.class)
	public void deleteNewsPost(Long newsId) throws ServiceException {
		newsService.delete(newsId);
	}

	@Override
	public List<NewsVO> getNewsOnPage(int pageNumber, int newsOnPage, SearchFilter searchFilter) throws ServiceException {
		List<News> newsList = newsService.readAll(pageNumber, newsOnPage, searchFilter);
		List<NewsVO> fullNewsList = createNewsVOList(newsList);
		return fullNewsList;
	}

	private NewsVO createNewsVO(News news) {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(news);
		newsVO.setTagList(new ArrayList<>(news.getTagSet()));
		newsVO.setCommentList(new ArrayList<>(news.getCommentSet()));
		newsVO.setAuthor(news.getAuthorSet().iterator().next());
		return newsVO;
	}

	private List<NewsVO> createNewsVOList(List<News> newsList) {
		Author author = null;
		List<Tag> tagList = null;
		List<Comment> commentList = null;
		List<NewsVO> fullNewsList = new ArrayList<>();

		for (News news : newsList) {
			author = news.getAuthorSet().iterator().next();
			tagList = new ArrayList<>(news.getTagSet());
			commentList = new ArrayList<>(news.getCommentSet());

			NewsVO newsVO = new NewsVO();
			newsVO.setNews(news);
			newsVO.setAuthor(author);
			newsVO.setCommentList(commentList);
			newsVO.setTagList(tagList);

			fullNewsList.add(newsVO);
		}
		return fullNewsList;
	}

	private News getNewsObject(NewsVO newsVO) {
		News news = newsVO.getNews();
		news.setTagSet(new HashSet<>(newsVO.getTagList()));
		news.setAuthorSet(new HashSet<>(Arrays.asList(newsVO.getAuthor())));
		return news;
	}

}
