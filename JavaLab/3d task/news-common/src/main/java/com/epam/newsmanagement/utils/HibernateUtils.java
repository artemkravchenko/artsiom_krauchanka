package com.epam.newsmanagement.utils;

import java.util.TimeZone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * 
 * {@code HibernateUtils} class is utilite class for working with database using
 * Hibernate. Contains methods for creating hibernate connection with DB and for
 * closing connection.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class HibernateUtils {

	private final SessionFactory sessionFactory;
	private static final String TIME_FORMAT = "UTC";

	public HibernateUtils(SessionFactory sessionFactory) {
		TimeZone.setDefault(TimeZone.getTimeZone(TIME_FORMAT));
		this.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		return sessionFactory.openSession();
	}

	public void closeSession(Session session) {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}
}
