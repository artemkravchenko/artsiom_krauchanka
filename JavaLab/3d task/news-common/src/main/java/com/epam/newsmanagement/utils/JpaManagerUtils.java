package com.epam.newsmanagement.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * 
 * {@code JpaManagerUtils} class is utilite class for working with database
 * using JPA. Contains methods for creating JPA connection with DB.
 * 
 * @author Artsiom_Krauchanka
 *
 */
public class JpaManagerUtils {
	private final EntityManagerFactory factory;

	public JpaManagerUtils(EntityManagerFactory factory) {
		this.factory = factory;
	}

	public EntityManager getManager() {
		return factory.createEntityManager();
	}
}
